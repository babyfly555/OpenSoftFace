using log4net;
using System;

namespace LogUtil
{
	public class LogHelper
	{
		private static readonly ILog loginfo = LogManager.GetLogger("loginfo");

		private static readonly ILog logerror = LogManager.GetLogger("logerror");

		public static void LogInfo(Type type, string msg)
		{
            try
            {
                if (LogHelper.loginfo.IsInfoEnabled)
                {
                    LogHelper.loginfo.Info(type.ToString(), new Exception(msg));
                }
            }
            catch (Exception err) { }
		}

		public static void LogInfo(Type type, Exception ex)
		{
            try
            {
			    if (LogHelper.loginfo.IsInfoEnabled)
			    {
				    LogHelper.loginfo.Info(type.ToString(), ex);
			    }
            }
            catch (Exception err) { }
		}

		public static void LogError(Type type, Exception se)
		{
            try
            {
			    if (LogHelper.logerror.IsErrorEnabled)
			    {
				    LogHelper.logerror.Error(type.ToString(), se);
			    }
            }
            catch (Exception err) { }
		}
	}
}
