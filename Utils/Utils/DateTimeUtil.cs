using LogUtil;
using System;
using System.Collections.Generic;
using System.Globalization;

namespace Utils
{
	public class DateTimeUtil
	{
		private static string[] weekdays = new string[]
		{
			"日",
			"一",
			"二",
			"三",
			"四",
			"五",
			"六"
		};

		public static string CalculationWorkingHours(string inTime, string outTime)
		{
			string result = "0";
			try
			{
				if (DateTimeUtil.Compare(outTime, inTime) < 0)
				{
					return "异常";
				}
				DateTime value = DateTime.Parse(inTime);
				result = (DateTime.Parse(outTime).Subtract(value).TotalSeconds * 1.0 / 3600.0).ToString("0.###");
			}
			catch (Exception se)
			{
				LogHelper.LogError(typeof(DateTimeUtil), se);
			}
			return result;
		}

		public static int Compare(string time1, string time2)
		{
			try
			{
				DateTime arg_0E_0 = DateTime.Parse(time1);
				DateTime t = DateTime.Parse(time2);
				return DateTime.Compare(arg_0E_0, t);
			}
			catch
			{
			}
			return 0;
		}

		public static string Parse(long timeStamp)
		{
			return TimeZone.CurrentTimeZone.ToLocalTime(new DateTime(1970, 1, 1)).AddMilliseconds((double)timeStamp).ToString("yyyy-MM-dd HH:mm:ss");
		}

		public static DateTime ToDateTime(string timeStamp)
		{
			long num = Convert.ToInt64(timeStamp) * 10000000L;
			DateTime dateTime = new DateTime(1970, 1, 1, 8, 0, 0);
			long ticks = dateTime.Ticks + num;
			return new DateTime(ticks);
		}

		public static long Parse(string timeStamp)
		{
			DateTime d = new DateTime(1970, 1, 1, 8, 0, 0);
			return Convert.ToInt64((DateTime.Parse(timeStamp) - d).TotalMilliseconds);
		}

		public static long Dist(string time1, string time2)
		{
			DateTime arg_0E_0 = DateTime.Parse(time1);
			DateTime d = DateTime.Parse(time2);
			return (long)(arg_0E_0 - d).TotalSeconds;
		}

		public static string getDateTimeNow()
		{
			return DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
		}

		public static List<string> getDayList(DateTime startDT, DateTime endDT, string preStr = "", string dateStyle = "yyyy/MM/dd")
		{
			List<string> list = new List<string>();
			try
			{
				if (startDT.Year.Equals(endDT.Year) && startDT.Month.Equals(endDT.Month) && startDT.Day.Equals(endDT.Day))
				{
					list.Add(string.Format("{0}{1}", preStr, startDT.ToString(dateStyle, DateTimeFormatInfo.InvariantInfo)));
				}
				while (!startDT.Year.Equals(endDT.Year) || !startDT.Month.Equals(endDT.Month) || !startDT.Day.Equals(endDT.Day))
				{
					list.Add(string.Format("{0}{1}", preStr, startDT.ToString(dateStyle, DateTimeFormatInfo.InvariantInfo)));
					startDT = startDT.AddDays(1.0);
					if (startDT.Year.Equals(endDT.Year) && startDT.Month.Equals(endDT.Month) && startDT.Day.Equals(endDT.Day))
					{
						list.Add(string.Format("{0}{1}", preStr, startDT.ToString(dateStyle, DateTimeFormatInfo.InvariantInfo)));
						break;
					}
				}
			}
			catch (Exception se)
			{
				LogHelper.LogError(typeof(DateTimeUtil), se);
			}
			return list;
		}

		public static Dictionary<string, List<string>> getDayDict(DateTime startDT, DateTime endDT, ref Dictionary<string, List<DateTime>> dateTimeDict, string dateStyle = "yyyy/MM/dd")
		{
			Dictionary<string, List<string>> dictionary = new Dictionary<string, List<string>>();
			try
			{
				List<string> list = new List<string>();
				List<DateTime> list2 = new List<DateTime>();
				string text = string.Format("{0}年{1}月", startDT.Year, startDT.Month);
				if (startDT.Year.Equals(endDT.Year) && startDT.Month.Equals(endDT.Month) && startDT.Day.Equals(endDT.Day))
				{
					list.Add(string.Format(startDT.ToString(dateStyle, DateTimeFormatInfo.InvariantInfo), new object[0]) + "," + DateTimeUtil.Week(startDT));
					dictionary.Add(text, list);
					list2.Add(startDT);
					dateTimeDict.Add(text, list2);
				}
				while (!startDT.Year.Equals(endDT.Year) || !startDT.Month.Equals(endDT.Month) || !startDT.Day.Equals(endDT.Day))
				{
					string text2 = string.Format("{0}年{1}月", startDT.Year, startDT.Month);
					if (!text2.Equals(text))
					{
						dictionary.Add(text, list);
						list = new List<string>();
						dateTimeDict.Add(text, list2);
						list2 = new List<DateTime>();
						text = text2;
					}
					list.Add(startDT.ToString(dateStyle, DateTimeFormatInfo.InvariantInfo) + "," + DateTimeUtil.Week(startDT));
					list2.Add(startDT);
					startDT = startDT.AddDays(1.0);
					if (startDT.Year.Equals(endDT.Year) && startDT.Month.Equals(endDT.Month) && startDT.Day.Equals(endDT.Day))
					{
						text2 = string.Format("{0}年{1}月", startDT.Year, startDT.Month);
						if (!text2.Equals(text))
						{
							dictionary.Add(text, list);
							list = new List<string>();
							dateTimeDict.Add(text, list2);
							list2 = new List<DateTime>();
							text = text2;
						}
						list.Add(startDT.ToString(dateStyle, DateTimeFormatInfo.InvariantInfo) + "," + DateTimeUtil.Week(startDT));
						dictionary.Add(text, list);
						list2.Add(startDT);
						dateTimeDict.Add(text, list2);
						break;
					}
				}
			}
			catch (Exception se)
			{
				LogHelper.LogError(typeof(DateTimeUtil), se);
			}
			return dictionary;
		}

		public static string Week(DateTime datetime)
		{
			string result = "周一";
			try
			{
				int num = Convert.ToInt32(datetime.DayOfWeek);
				result = DateTimeUtil.weekdays[num];
			}
			catch
			{
			}
			return result;
		}

		public static DateTime getMonthFirstDay(DateTime datetime)
		{
			try
			{
				datetime = datetime.AddDays((double)(1 - datetime.Day));
			}
			catch
			{
			}
			return datetime;
		}

		public static DateTime getMonthLastDay(DateTime datetime)
		{
			try
			{
				datetime = datetime.AddDays((double)(1 - datetime.Day)).Date.AddMonths(1).AddSeconds(-1.0);
			}
			catch
			{
			}
			return datetime;
		}
	}
}
