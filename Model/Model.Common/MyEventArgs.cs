using System;

namespace Model.Common
{
	public class MyEventArgs : EventArgs
	{
		private int m_value;

		private string m_strId;

		private EventArgs m_e;

		public string StrId
		{
			get
			{
				return this.m_strId;
			}
			set
			{
				this.m_strId = value;
			}
		}

		public int Value
		{
			get
			{
				return this.m_value;
			}
			set
			{
				this.m_value = value;
			}
		}

		public EventArgs E
		{
			get
			{
				return this.m_e;
			}
			set
			{
				this.m_e = value;
			}
		}

		public MyEventArgs()
		{
		}

		public MyEventArgs(string str)
		{
			this.m_strId = str;
		}

		public MyEventArgs(int value)
		{
			this.m_value = value;
		}

		public MyEventArgs(string str, int value, EventArgs e)
		{
			this.m_strId = str;
			this.m_value = value;
			this.m_e = e;
		}
	}
}
