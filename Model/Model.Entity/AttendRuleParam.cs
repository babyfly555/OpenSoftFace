using System;

namespace Model.Entity
{
	public class AttendRuleParam
	{
		public string work_hours_limit
		{
			get;
			set;
		}

		public string attend_rule_mode
		{
			get;
			set;
		}

		public string attend_start_time
		{
			get;
			set;
		}

		public string attend_end_time
		{
			get;
			set;
		}

		public string effective_time_start
		{
			get;
			set;
		}

		public string effective_time_end
		{
			get;
			set;
		}

		public string rest_day
		{
			get;
			set;
		}

		public int effective_mode
		{
			get;
			set;
		}
	}
}
