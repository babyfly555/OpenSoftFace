using System;

namespace CommonConstant
{
	public class FaceInfoHasFace
	{
		public const int HAS_FACE = 0;

		public const int DO_NOT_HAS_FACE = 1;
	}
}
