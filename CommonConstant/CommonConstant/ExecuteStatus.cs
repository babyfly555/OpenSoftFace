using System;

namespace CommonConstant
{
	public class ExecuteStatus
	{
		public const int CLIENT_WAIT = 0;

		public const string CLIENT_WAIT_TEXT = "等待执行";

		public const int CLIENT_NO_RESPONSE = 1;

		public const string CLIENT_NO_RESPONSE_TEXT = "客户端无反应";

		public const int CLIENT_RETURN_FAILED = 2;

		public const string CLIENT_RETURN_FAILED_TEXT = "设置失败";

		public const string EXCEPTION_TEXT = "异常";
	}
}
