using System;

namespace CommonConstant
{
	public class AttendRuleMode
	{
		public const string FIXED_ATTENDANCE_RULE = "0";

		public const string FLEXIBLE_ATTENDANCE_RULE = "1";
	}
}
