using System;

namespace CommonConstant
{
	public class FaceDistanceConstant
	{
		public const string DISTANCE_ONE_VALUE = "8";

		public const string DISTANCE_TWO_VALUE = "10";

		public const string DISTANCE_THREE_VALUE = "16";
	}
}
