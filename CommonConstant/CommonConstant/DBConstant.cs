using System;

namespace CommonConstant
{
	public class DBConstant
	{
		public static string DB_DIR = AppConfig.CONFIG_DIR;

		public const string DB_NAME = "generalattendance.db";

		public const string CHECK_RECORD_DB_NAME = "CheckRecord.db";

		public const string MAIN_INFO_TABLE = "MainInfo";

		public const string FEATURE_INFO_TABLE = "FeatureInfo";

		public const string ATTENDANCE_RECORD_TABLE = "AttendRecord";

		public const string ATTENDANCE_RULE_TABLE = "AttendRule";
	}
}
