using System;

namespace CommonConstant
{
	public class AttendStatus
	{
		public const int NORMAL_RECORD = 2;

		public const int ABSENCE_RECORD = 1;

		public const int NONE_RECORD = 0;

		public const string NORMAL_RECORD_TEXT = "正常";

		public const string ABSENCE_RECORD_TEXT = "缺勤";

		public const string NONE_RECORD_TEXT = "--";
	}
}
