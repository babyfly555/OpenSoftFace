using CommonConstant;
using LogUtil;
using Model.Entity;
using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Text;
using Utils;

namespace DAL.Service.Impl
{
	public class AttendRuleServiceImpl : IAttendRuleService
	{
		private string connectionString;

		public AttendRuleServiceImpl()
		{
			try
			{
				this.connectionString = new SQLiteConnectionStringBuilder
				{
					DataSource = DBConstant.DB_DIR + "generalattendance.db"
				}.ToString();
			}
			catch (Exception ex)
			{
				LogHelper.LogError(base.GetType(), ex);
			}
		}

		public int Count(string[] where = null)
		{
			using (SQLiteConnection conn = new SQLiteConnection(this.connectionString))
			{
				try
				{
					conn.SetPassword(ComputerUtil.GetDBPasswordStr());
					conn.Open();
					StringBuilder sqlBuilder = new StringBuilder();
					sqlBuilder.Append(string.Format(" select distinct count(*) as num from {0}  ", "AttendRule"));
					if (where != null && where.Length != 0)
					{
						sqlBuilder.Append(" where ");
						for (int i = 0; i < where.Length; i++)
						{
							if (i == 0)
							{
								sqlBuilder.Append(string.Format(" {0} ", where[i]));
							}
							else
							{
								sqlBuilder.Append(string.Format(" and {0} ", where[i]));
							}
						}
					}
					Console.WriteLine("sql:{0}", sqlBuilder.ToString());
					using (SQLiteCommand command = conn.CreateCommand())
					{
						command.CommandText = sqlBuilder.ToString();
						SQLiteDataReader reader = command.ExecuteReader();
						int num = 0;
						if (reader.Read())
						{
							num = int.Parse(reader["num"].ToString());
						}
						reader.Close();
						command.Dispose();
						conn.Close();
						return num;
					}
				}
				catch (Exception e)
				{
					LogHelper.LogError(base.GetType(), e);
					Console.WriteLine(e);
				}
				finally
				{
					DBHelper.closeConn(conn);
				}
			}
			return 0;
		}

		public int Delete()
		{
			using (SQLiteConnection conn = new SQLiteConnection(this.connectionString))
			{
				conn.SetPassword(ComputerUtil.GetDBPasswordStr());
				conn.Open();
				try
				{
					string sql = string.Format("delete from {0} where end_date <>'' and end_date is not null and datetime(end_date) < datetime('{1}')", "AttendRule", DateTime.Now.ToString("yyyy-MM-dd"));
					Console.WriteLine("sql:{0}", sql);
					object dBLocker = DBHelper.DBLocker;
					lock (dBLocker)
					{
						using (SQLiteCommand command = conn.CreateCommand())
						{
							command.CommandText = sql;
							int arg_7F_0 = command.ExecuteNonQuery();
							command.Dispose();
							conn.Close();
							return arg_7F_0;
						}
					}
				}
				catch (Exception e)
				{
					LogHelper.LogError(base.GetType(), e);
					Console.WriteLine(e);
				}
				finally
				{
					DBHelper.closeConn(conn);
				}
			}
			return 0;
		}

		public int DeleteOtherRecordById(int ID)
		{
			using (SQLiteConnection conn = new SQLiteConnection(this.connectionString))
			{
				conn.SetPassword(ComputerUtil.GetDBPasswordStr());
				conn.Open();
				try
				{
					string sql = string.Format("delete from {0} where  id != {1}", "AttendRule", ID);
					Console.WriteLine("sql:{0}", sql);
					object dBLocker = DBHelper.DBLocker;
					lock (dBLocker)
					{
						using (SQLiteCommand command = conn.CreateCommand())
						{
							command.CommandText = sql;
							int arg_72_0 = command.ExecuteNonQuery();
							command.Dispose();
							conn.Close();
							return arg_72_0;
						}
					}
				}
				catch (Exception e)
				{
					LogHelper.LogError(base.GetType(), e);
					Console.WriteLine(e);
				}
				finally
				{
					DBHelper.closeConn(conn);
				}
			}
			return 0;
		}

		public int Delete(int ruleId)
		{
			using (SQLiteConnection conn = new SQLiteConnection(this.connectionString))
			{
				conn.SetPassword(ComputerUtil.GetDBPasswordStr());
				conn.Open();
				try
				{
					string sql = string.Format("delete from {0} where id={1}", "AttendRule", ruleId);
					Console.WriteLine("sql:{0}", sql);
					object dBLocker = DBHelper.DBLocker;
					lock (dBLocker)
					{
						using (SQLiteCommand command = conn.CreateCommand())
						{
							command.CommandText = sql;
							int arg_72_0 = command.ExecuteNonQuery();
							command.Dispose();
							conn.Close();
							return arg_72_0;
						}
					}
				}
				catch (Exception e)
				{
					LogHelper.LogError(base.GetType(), e);
					Console.WriteLine(e);
				}
				finally
				{
					DBHelper.closeConn(conn);
				}
			}
			return 0;
		}

		public AttendRule Get(int attendID)
		{
			AttendRule result;
			using (SQLiteConnection conn = new SQLiteConnection(this.connectionString))
			{
				try
				{
					conn.SetPassword(ComputerUtil.GetDBPasswordStr());
					conn.Open();
					string sql = string.Format("select * from {0} where id={1} order by id desc", "AttendRule", attendID);
					Console.WriteLine("sql:{0}", sql);
					using (SQLiteCommand command = conn.CreateCommand())
					{
						command.CommandText = sql;
						SQLiteDataReader reader = command.ExecuteReader();
						AttendRule attendRule = null;
						if (reader.Read())
						{
							attendRule = new AttendRule();
							attendRule.ID = StringUtil.CheckSqlIntValue(reader["id"].ToString());
							attendRule.Json = reader["json"].ToString();
							attendRule.StartDate = reader["start_date"].ToString();
							attendRule.EndDate = reader["end_date"].ToString();
						}
						reader.Close();
						command.Dispose();
						conn.Close();
						result = attendRule;
						return result;
					}
				}
				catch (Exception e)
				{
					LogHelper.LogError(base.GetType(), e);
					Console.WriteLine(e);
				}
				finally
				{
					DBHelper.closeConn(conn);
				}
				result = null;
			}
			return result;
		}

		public AttendRule Get(bool isNew)
		{
			AttendRule result;
			using (SQLiteConnection conn = new SQLiteConnection(this.connectionString))
			{
				try
				{
					conn.SetPassword(ComputerUtil.GetDBPasswordStr());
					conn.Open();
					string sql = string.Format("select * from {0} where ({1}) order by id desc limit 1", "AttendRule", isNew ? "end_date='' or end_date is null" : "end_date is not null and end_date <> ''");
					Console.WriteLine("sql:{0}", sql);
					using (SQLiteCommand command = conn.CreateCommand())
					{
						command.CommandText = sql;
						SQLiteDataReader reader = command.ExecuteReader();
						AttendRule attendRule = null;
						if (reader.Read())
						{
							attendRule = new AttendRule();
							attendRule.ID = StringUtil.CheckSqlIntValue(reader["id"].ToString());
							attendRule.Json = reader["json"].ToString();
							attendRule.StartDate = reader["start_date"].ToString();
							attendRule.EndDate = reader["end_date"].ToString();
						}
						reader.Close();
						command.Dispose();
						conn.Close();
						result = attendRule;
						return result;
					}
				}
				catch (Exception e)
				{
					LogHelper.LogError(base.GetType(), e);
					Console.WriteLine(e);
				}
				finally
				{
					DBHelper.closeConn(conn);
				}
				result = null;
			}
			return result;
		}

		public int Insert(AttendRule attendRule)
		{
			using (SQLiteConnection conn = new SQLiteConnection(this.connectionString))
			{
				conn.SetPassword(ComputerUtil.GetDBPasswordStr());
				conn.Open();
				try
				{
					string sql = string.Format("insert into {0} values(null,'{1}','{2}','{3}')", new object[]
					{
						"AttendRule",
						attendRule.Json,
						attendRule.StartDate,
						attendRule.EndDate
					});
					Console.WriteLine("sql:{0}", sql);
					object dBLocker = DBHelper.DBLocker;
					lock (dBLocker)
					{
						using (SQLiteCommand command = conn.CreateCommand())
						{
							command.CommandText = sql;
							int arg_90_0 = command.ExecuteNonQuery();
							command.Dispose();
							conn.Close();
							return arg_90_0;
						}
					}
				}
				catch (Exception e)
				{
					LogHelper.LogError(base.GetType(), e);
					Console.WriteLine(e);
				}
				finally
				{
					DBHelper.closeConn(conn);
				}
			}
			return 0;
		}

		public List<AttendRule> QueryForList(string[] where = null, string groupBy = null, string having = null, string orderBy = null, int offset = -1, int size = -1)
		{
			List<AttendRule> list = new List<AttendRule>();
			using (SQLiteConnection conn = new SQLiteConnection(this.connectionString))
			{
				try
				{
					conn.SetPassword(ComputerUtil.GetDBPasswordStr());
					conn.Open();
					StringBuilder sqlBuilder = new StringBuilder();
					sqlBuilder.Append(string.Format(" select * from {0}", "AttendRule"));
					if (where != null && where.Length != 0)
					{
						sqlBuilder.Append(string.Format(" where ", new object[0]));
						for (int i = 0; i < where.Length; i++)
						{
							if (i == 0)
							{
								sqlBuilder.Append(string.Format(" {0} ", where[i]));
							}
							else
							{
								sqlBuilder.Append(string.Format(" and {0} ", where[i]));
							}
						}
					}
					if (groupBy != null && groupBy != string.Empty)
					{
						sqlBuilder.Append(string.Format(" group by {0} ", groupBy));
					}
					if (having != null && having != string.Empty)
					{
						sqlBuilder.Append(string.Format(" having {0} ", having));
					}
					if (orderBy != null && orderBy != string.Empty)
					{
						sqlBuilder.Append(string.Format(" order by {0} ", orderBy));
					}
					if (offset >= 0 && size >= 0)
					{
						sqlBuilder.Append(string.Format(" limit {0},{1} ", offset, size));
					}
					Console.WriteLine("sql:{0}", sqlBuilder.ToString());
					using (SQLiteCommand command = conn.CreateCommand())
					{
						command.CommandText = sqlBuilder.ToString();
						SQLiteDataReader reader = command.ExecuteReader();
						while (reader.Read())
						{
							list.Add(new AttendRule
							{
								ID = StringUtil.CheckSqlIntValue(reader["id"].ToString()),
								Json = reader["json"].ToString(),
								StartDate = reader["start_date"].ToString(),
								EndDate = reader["end_date"].ToString()
							});
						}
						reader.Close();
						command.Dispose();
						conn.Close();
					}
				}
				catch (Exception e)
				{
					LogHelper.LogError(base.GetType(), e);
					Console.WriteLine(e);
				}
				finally
				{
					DBHelper.closeConn(conn);
				}
			}
			return list;
		}

		public int Update(AttendRule attendRule)
		{
			int result;
			using (SQLiteConnection conn = new SQLiteConnection(this.connectionString))
			{
				conn.SetPassword(ComputerUtil.GetDBPasswordStr());
				conn.Open();
				try
				{
					string sql = string.Format("update {0} set json='{1}',start_date='{2}',end_date='{3}' where id={4}", new object[]
					{
						"AttendRule",
						attendRule.Json,
						attendRule.StartDate,
						attendRule.EndDate,
						attendRule.ID
					});
					Console.WriteLine("sql:{0}", sql);
					object dBLocker = DBHelper.DBLocker;
					lock (dBLocker)
					{
						using (SQLiteCommand command = conn.CreateCommand())
						{
							command.CommandText = sql;
							int arg_9E_0 = command.ExecuteNonQuery();
							command.Dispose();
							conn.Close();
							result = arg_9E_0;
							return result;
						}
					}
				}
				catch (Exception e)
				{
					LogHelper.LogError(base.GetType(), e);
					Console.WriteLine(e);
				}
				finally
				{
					DBHelper.closeConn(conn);
				}
				result = 0;
			}
			return result;
		}

		public int MaxId()
		{
			using (SQLiteConnection conn = new SQLiteConnection(this.connectionString))
			{
				try
				{
					conn.SetPassword(ComputerUtil.GetDBPasswordStr());
					conn.Open();
					StringBuilder sqlBuilder = new StringBuilder();
					sqlBuilder.Append(string.Format(" select max(id) as num from {0}  ", "AttendRule"));
					using (SQLiteCommand command = conn.CreateCommand())
					{
						command.CommandText = sqlBuilder.ToString();
						using (SQLiteDataReader reader = command.ExecuteReader())
						{
							int num = 0;
							if (reader.Read())
							{
								num = int.Parse(reader["num"].ToString()) + 1;
							}
							reader.Close();
							command.Dispose();
							conn.Close();
							return num;
						}
					}
				}
				catch (Exception e)
				{
					LogHelper.LogError(base.GetType(), e);
					Console.WriteLine(e);
				}
				finally
				{
					DBHelper.closeConn(conn);
				}
			}
			return 1;
		}
	}
}
