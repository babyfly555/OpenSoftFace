using CommonConstant;
using LogUtil;
using Model.Entity;
using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Text;
using Utils;

namespace DAL.Service.Impl
{
	public class MainInfoServiceImpl : IMainInfoService
	{
		private string connectionString;

		public MainInfoServiceImpl()
		{
			try
			{
				this.connectionString = new SQLiteConnectionStringBuilder
				{
					DataSource = DBConstant.DB_DIR + "generalattendance.db"
				}.ToString();
			}
			catch (Exception ex)
			{
				LogHelper.LogError(base.GetType(), ex);
			}
		}

		public int Count(string[] where = null)
		{
			using (SQLiteConnection conn = new SQLiteConnection(this.connectionString))
			{
				try
				{
					conn.SetPassword(ComputerUtil.GetDBPasswordStr());
					conn.Open();
					StringBuilder sqlBuilder = new StringBuilder();
					sqlBuilder.Append(string.Format(" select distinct count(*) as num from {0}  ", "MainInfo"));
					if (where != null && where.Length != 0)
					{
						sqlBuilder.Append(" where ");
						for (int i = 0; i < where.Length; i++)
						{
							if (i == 0)
							{
								sqlBuilder.Append(string.Format(" {0} ", where[i]));
							}
							else
							{
								sqlBuilder.Append(string.Format(" and {0} ", where[i]));
							}
						}
					}
					Console.WriteLine("sql:{0}", sqlBuilder.ToString());
					using (SQLiteCommand command = conn.CreateCommand())
					{
						command.CommandText = sqlBuilder.ToString();
						SQLiteDataReader reader = command.ExecuteReader();
						int num = 0;
						if (reader.Read())
						{
							num = int.Parse(reader["num"].ToString());
						}
						reader.Close();
						command.Dispose();
						conn.Close();
						return num;
					}
				}
				catch (Exception e)
				{
					LogHelper.LogError(base.GetType(), e);
					Console.WriteLine(e);
				}
				finally
				{
					DBHelper.closeConn(conn);
				}
			}
			return 0;
		}

		public int Delete(int personInfoID)
		{
			using (SQLiteConnection conn = new SQLiteConnection(this.connectionString))
			{
				conn.SetPassword(ComputerUtil.GetDBPasswordStr());
				conn.Open();
				try
				{
					string sql = string.Format("delete from {0} where id={1}", "MainInfo", personInfoID);
					Console.WriteLine("sql:{0}", sql);
					object dBLocker = DBHelper.DBLocker;
					lock (dBLocker)
					{
						using (SQLiteCommand command = conn.CreateCommand())
						{
							command.CommandText = sql;
							int arg_72_0 = command.ExecuteNonQuery();
							command.Dispose();
							conn.Close();
							return arg_72_0;
						}
					}
				}
				catch (Exception e)
				{
					LogHelper.LogError(base.GetType(), e);
					Console.WriteLine(e);
				}
				finally
				{
					DBHelper.closeConn(conn);
				}
			}
			return 0;
		}

		public MainInfo Get(int personInfoID)
		{
			MainInfo result;
			using (SQLiteConnection conn = new SQLiteConnection(this.connectionString))
			{
				try
				{
					conn.SetPassword(ComputerUtil.GetDBPasswordStr());
					conn.Open();
					string sql = string.Format("select * from {0} where id={1} order by id desc", "MainInfo", personInfoID);
					Console.WriteLine("sql:{0}", sql);
					using (SQLiteCommand command = conn.CreateCommand())
					{
						command.CommandText = sql;
						SQLiteDataReader reader = command.ExecuteReader();
						MainInfo personInfo = null;
						if (reader.Read())
						{
							personInfo = new MainInfo();
							personInfo.ID = StringUtil.CheckSqlIntValue(reader["id"].ToString());
							personInfo.PersonName = reader["person_name"].ToString();
							personInfo.ShowImageName = reader["show_image_name"].ToString();
							personInfo.PLeft = StringUtil.CheckSqlIntValue(reader["p_left"].ToString());
							personInfo.PTop = StringUtil.CheckSqlIntValue(reader["p_top"].ToString());
							personInfo.PRight = StringUtil.CheckSqlIntValue(reader["p_right"].ToString());
							personInfo.PBottom = StringUtil.CheckSqlIntValue(reader["p_bottom"].ToString());
							personInfo.POrient = StringUtil.CheckSqlIntValue(reader["p_orient"].ToString());
							personInfo.PersonSerial = reader["person_serial"].ToString();
							personInfo.PValid = StringUtil.CheckSqlIntValue(reader["p_valid"].ToString());
							personInfo.AddTime = reader["add_time"].ToString();
							personInfo.UpdateTime = reader["update_time"].ToString();
							personInfo.Dept = reader["dept"].ToString();
						}
						reader.Close();
						command.Dispose();
						conn.Close();
						result = personInfo;
						return result;
					}
				}
				catch (Exception e)
				{
					LogHelper.LogError(base.GetType(), e);
					Console.WriteLine(e);
				}
				finally
				{
					DBHelper.closeConn(conn);
				}
				result = null;
			}
			return result;
		}

		public MainInfo Get(string workNumber)
		{
			MainInfo result;
			using (SQLiteConnection conn = new SQLiteConnection(this.connectionString))
			{
				try
				{
					conn.SetPassword(ComputerUtil.GetDBPasswordStr());
					conn.Open();
					string sql = string.Format("select * from {0} where person_serial='{1}' and p_valid != -1 order by p_valid desc", "MainInfo", workNumber);
					Console.WriteLine("sql:{0}", sql);
					using (SQLiteCommand command = conn.CreateCommand())
					{
						command.CommandText = sql;
						using (SQLiteDataReader reader = command.ExecuteReader())
						{
							MainInfo personInfo = null;
							if (reader.Read())
							{
								personInfo = new MainInfo();
								personInfo.ID = StringUtil.CheckSqlIntValue(reader["id"].ToString());
								personInfo.PersonName = reader["person_name"].ToString();
								personInfo.ShowImageName = reader["show_image_name"].ToString();
								personInfo.PLeft = StringUtil.CheckSqlIntValue(reader["p_left"].ToString());
								personInfo.PTop = StringUtil.CheckSqlIntValue(reader["p_top"].ToString());
								personInfo.PRight = StringUtil.CheckSqlIntValue(reader["p_right"].ToString());
								personInfo.PBottom = StringUtil.CheckSqlIntValue(reader["p_bottom"].ToString());
								personInfo.POrient = StringUtil.CheckSqlIntValue(reader["p_orient"].ToString());
								personInfo.PersonSerial = reader["person_serial"].ToString();
								personInfo.PValid = StringUtil.CheckSqlIntValue(reader["p_valid"].ToString());
								personInfo.AddTime = reader["add_time"].ToString();
								personInfo.UpdateTime = reader["update_time"].ToString();
								personInfo.Dept = reader["dept"].ToString();
							}
							reader.Close();
							command.Dispose();
							conn.Close();
							result = personInfo;
							return result;
						}
					}
				}
				catch (Exception e)
				{
					LogHelper.LogError(base.GetType(), e);
					Console.WriteLine(e);
				}
				finally
				{
					DBHelper.closeConn(conn);
				}
				result = null;
			}
			return result;
		}

		public int Insert(MainInfo personInfo)
		{
			using (SQLiteConnection conn = new SQLiteConnection(this.connectionString))
			{
				conn.SetPassword(ComputerUtil.GetDBPasswordStr());
				conn.Open();
				try
				{
					personInfo.AddTime = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
					personInfo.UpdateTime = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
					string sql = string.Format("insert into {0} values(null,'{1}','{2}',{3},{4},{5},{6},{7},'{8}',{9},'{10}','{11}','{12}')", new object[]
					{
						"MainInfo",
						personInfo.PersonName,
						personInfo.ShowImageName,
						personInfo.PLeft,
						personInfo.PTop,
						personInfo.PRight,
						personInfo.PBottom,
						personInfo.POrient,
						personInfo.PersonSerial,
						personInfo.PValid,
						personInfo.AddTime,
						personInfo.UpdateTime,
						personInfo.Dept
					});
					Console.WriteLine("sql:{0}", sql);
					object dBLocker = DBHelper.DBLocker;
					lock (dBLocker)
					{
						using (SQLiteCommand command = conn.CreateCommand())
						{
							command.CommandText = sql;
							int arg_135_0 = command.ExecuteNonQuery();
							command.Dispose();
							conn.Close();
							return arg_135_0;
						}
					}
				}
				catch (Exception e)
				{
					LogHelper.LogError(base.GetType(), e);
					Console.WriteLine(e);
				}
				finally
				{
					DBHelper.closeConn(conn);
				}
			}
			return 0;
		}

		public List<MainInfo> QueryForList(string[] where = null, string groupBy = null, string having = null, string orderBy = null, int offset = -1, int size = -1)
		{
			List<MainInfo> list = new List<MainInfo>();
			using (SQLiteConnection conn = new SQLiteConnection(this.connectionString))
			{
				try
				{
					conn.SetPassword(ComputerUtil.GetDBPasswordStr());
					conn.Open();
					StringBuilder sqlBuilder = new StringBuilder();
					sqlBuilder.Append(string.Format(" select * from {0}", "MainInfo"));
					if (where != null && where.Length != 0)
					{
						sqlBuilder.Append(string.Format(" where ", new object[0]));
						for (int i = 0; i < where.Length; i++)
						{
							if (i == 0)
							{
								sqlBuilder.Append(string.Format(" {0} ", where[i]));
							}
							else
							{
								sqlBuilder.Append(string.Format(" and {0} ", where[i]));
							}
						}
					}
					if (groupBy != null && groupBy != string.Empty)
					{
						sqlBuilder.Append(string.Format(" group by {0} ", groupBy));
					}
					if (having != null && having != string.Empty)
					{
						sqlBuilder.Append(string.Format(" having {0} ", having));
					}
					if (orderBy != null && orderBy != string.Empty)
					{
						sqlBuilder.Append(string.Format(" order by {0} ", orderBy));
					}
					if (offset >= 0 && size >= 0)
					{
						sqlBuilder.Append(string.Format(" limit {0},{1} ", offset, size));
					}
					Console.WriteLine("sql:{0}", sqlBuilder.ToString());
					using (SQLiteCommand command = conn.CreateCommand())
					{
						command.CommandText = sqlBuilder.ToString();
						SQLiteDataReader reader = command.ExecuteReader();
						while (reader.Read())
						{
							list.Add(new MainInfo
							{
								ID = StringUtil.CheckSqlIntValue(reader["id"].ToString()),
								PersonName = reader["person_name"].ToString(),
								ShowImageName = reader["show_image_name"].ToString(),
								PLeft = StringUtil.CheckSqlIntValue(reader["p_left"].ToString()),
								PTop = StringUtil.CheckSqlIntValue(reader["p_top"].ToString()),
								PRight = StringUtil.CheckSqlIntValue(reader["p_right"].ToString()),
								PBottom = StringUtil.CheckSqlIntValue(reader["p_bottom"].ToString()),
								POrient = StringUtil.CheckSqlIntValue(reader["p_orient"].ToString()),
								PersonSerial = reader["person_serial"].ToString(),
								PValid = StringUtil.CheckSqlIntValue(reader["p_valid"].ToString()),
								AddTime = reader["add_time"].ToString(),
								UpdateTime = reader["update_time"].ToString(),
								Dept = reader["dept"].ToString()
							});
						}
						reader.Close();
						command.Dispose();
						conn.Close();
					}
				}
				catch (Exception e)
				{
					LogHelper.LogError(base.GetType(), e);
					Console.WriteLine(e);
				}
				finally
				{
					DBHelper.closeConn(conn);
				}
			}
			return list;
		}

		public int Update(MainInfo personInfo)
		{
			int result;
			using (SQLiteConnection conn = new SQLiteConnection(this.connectionString))
			{
				conn.SetPassword(ComputerUtil.GetDBPasswordStr());
				conn.Open();
				try
				{
					string sql = string.Format("update {0} set person_name='{1}',show_image_name='{2}',p_left={3},p_top={4},p_right={5},p_bottom={6},p_orient={7},person_serial='{8}',p_valid={9},add_time='{10}',update_time='{11}',dept='{13}' where id={12}", new object[]
					{
						"MainInfo",
						personInfo.PersonName,
						personInfo.ShowImageName,
						personInfo.PLeft,
						personInfo.PTop,
						personInfo.PRight,
						personInfo.PBottom,
						personInfo.POrient,
						personInfo.PersonSerial,
						personInfo.PValid,
						personInfo.AddTime,
						personInfo.UpdateTime,
						personInfo.ID,
						personInfo.Dept
					});
					Console.WriteLine("sql:{0}", sql);
					object dBLocker = DBHelper.DBLocker;
					lock (dBLocker)
					{
						using (SQLiteCommand command = conn.CreateCommand())
						{
							command.CommandText = sql;
							int arg_113_0 = command.ExecuteNonQuery();
							command.Dispose();
							conn.Close();
							result = arg_113_0;
							return result;
						}
					}
				}
				catch (Exception e)
				{
					LogHelper.LogError(base.GetType(), e);
					Console.WriteLine(e);
				}
				finally
				{
					DBHelper.closeConn(conn);
				}
				result = 0;
			}
			return result;
		}

		public int Update(List<int> idList, int status)
		{
			if (idList == null || idList.Count <= 0)
			{
				return 0;
			}
			int result;
			using (SQLiteConnection conn = new SQLiteConnection(this.connectionString))
			{
				conn.SetPassword(ComputerUtil.GetDBPasswordStr());
				conn.Open();
				try
				{
					StringBuilder sbStr = new StringBuilder();
					for (int i = 0; i < idList.Count; i++)
					{
						if (i != 0)
						{
							sbStr.Append(",");
						}
						StringBuilder arg_55_0 = sbStr;
						result = idList[i];
						arg_55_0.Append(result.ToString());
					}
					string sql = string.Format("update {0} set p_valid={1},update_time='{3}' where id in({2})", new object[]
					{
						"MainInfo",
						status,
						sbStr.ToString(),
						DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")
					});
					Console.WriteLine("sql:{0}", sql);
					object dBLocker = DBHelper.DBLocker;
					lock (dBLocker)
					{
						using (SQLiteCommand command = conn.CreateCommand())
						{
							command.CommandText = sql;
							int arg_EB_0 = command.ExecuteNonQuery();
							command.Dispose();
							conn.Close();
							result = arg_EB_0;
							return result;
						}
					}
				}
				catch (Exception e)
				{
					LogHelper.LogError(base.GetType(), e);
					Console.WriteLine(e);
				}
				finally
				{
					DBHelper.closeConn(conn);
				}
				result = 0;
			}
			return result;
		}

		public int MaxId()
		{
			using (SQLiteConnection conn = new SQLiteConnection(this.connectionString))
			{
				try
				{
					conn.SetPassword(ComputerUtil.GetDBPasswordStr());
					conn.Open();
					StringBuilder sqlBuilder = new StringBuilder();
					sqlBuilder.Append(string.Format(" select max(id) as num from {0}  ", "MainInfo"));
					using (SQLiteCommand command = conn.CreateCommand())
					{
						command.CommandText = sqlBuilder.ToString();
						using (SQLiteDataReader reader = command.ExecuteReader())
						{
							int num = 0;
							if (reader.Read())
							{
								num = int.Parse(reader["num"].ToString()) + 1;
							}
							reader.Close();
							command.Dispose();
							conn.Close();
							return num;
						}
					}
				}
				catch (Exception e)
				{
					LogHelper.LogError(base.GetType(), e);
					Console.WriteLine(e);
				}
				finally
				{
					DBHelper.closeConn(conn);
				}
			}
			return 1;
		}
	}
}
