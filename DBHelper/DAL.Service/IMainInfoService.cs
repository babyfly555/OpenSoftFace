using Model.Entity;
using System;
using System.Collections.Generic;

namespace DAL.Service
{
	public interface IMainInfoService
	{
		int Insert(MainInfo personInfo);

		int Delete(int personInfoID);

		int Update(MainInfo personInfo);

		int Update(List<int> idList, int status);

		MainInfo Get(int personInfoID);

		MainInfo Get(string workNumber);

		List<MainInfo> QueryForList(string[] where = null, string groupBy = null, string having = null, string orderBy = null, int offset = -1, int size = -1);

		int Count(string[] where = null);

		int MaxId();
	}
}
