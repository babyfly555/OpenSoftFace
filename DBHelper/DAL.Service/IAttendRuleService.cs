using Model.Entity;
using System;
using System.Collections.Generic;

namespace DAL.Service
{
	public interface IAttendRuleService
	{
		int Insert(AttendRule attendRule);

		int Delete(int ruleId);

		int Delete();

		int DeleteOtherRecordById(int ID);

		int Update(AttendRule attendRule);

		AttendRule Get(int attendID);

		AttendRule Get(bool isNew);

		List<AttendRule> QueryForList(string[] where = null, string groupBy = null, string having = null, string orderBy = null, int offset = -1, int size = -1);

		int Count(string[] where = null);

		int MaxId();
	}
}
