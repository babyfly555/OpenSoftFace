﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OpenSoftFace.WebService
{
    public interface IHttpHandler
    {
        void ProcessRequest(HttpContext context);
    }
}
