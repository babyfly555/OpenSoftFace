﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OpenSoftFace.WebService
{
    public class HttpResponse
    {
        // 响应状态码
        public string StateCode { get; set; }
        // 响应状态描述
        public string StateDescription { get; set; }
        // 响应内容类型
        public string ContentType { get; set; }
        //响应报文的正文内容
        public byte[] Body { get; set; }
        public string ContentDisposition { get; set; }
        // 生成响应头信息
        public byte[] GetResponseHeader()
        {
            string strRequestHeader = string.Format(@"HTTP/1.1 {0} {1}
Content-Type: {2}
Accept-Ranges: bytes
Server: Microsoft-IIS/7.5
X-Powered-By: ASP.NET
Date: {3} 
Content-Length: {4}
Content-Disposition: {5}

", StateCode, StateDescription, ContentType, string.Format("{0:R}", DateTime.Now), Body.Length, ContentDisposition);
//Content-Disposition: attachment;fileName=aa.xls
            return Encoding.UTF8.GetBytes(strRequestHeader);
        }
    }
}
