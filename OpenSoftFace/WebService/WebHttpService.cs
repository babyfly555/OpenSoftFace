﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace OpenSoftFace.WebService
{
    public class WebHttpService
    {
        private static Socket socketWatch = null;
        private static Thread threadWatch = null;
        public static String severIPAddress = "127.0.0.1";
        public static String severPort = "80";
        public static void run()
        {
            try
            {
                List<string> ipv4_ips = GetLocalIpAddress("InterNetwork");//获取ipv4类型的ip
                // 创建Socket->绑定IP与端口->设置监听队列的长度->开启监听连接
                if (ipv4_ips.Count > 0) {
                    severIPAddress = ipv4_ips[0];
                }
                if(portInUse(Int32.Parse(severPort))) {
                    severPort = "8888";
                }
                socketWatch = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                socketWatch.Bind(new IPEndPoint(IPAddress.Parse(severIPAddress), int.Parse(severPort)));
                socketWatch.Listen(10);
                // 创建Thread->后台执行
                threadWatch = new Thread(ListenClientConnect);
                threadWatch.IsBackground = true;
                threadWatch.Start(socketWatch);
                //btnStart.Enabled = false;

            }
            catch (Exception err)
            {
                Console.WriteLine(err.ToString());
            }
        }

        private static void ListenClientConnect(object obj)
        {
            Socket socketListen = obj as Socket;

            while (true)
            {
                Socket proxSocket = null;
                try
                {
                    proxSocket = socketListen.Accept();
                    byte[] data = new byte[1024 * 1024 * 2];
                    int length = proxSocket.Receive(data, 0, data.Length, SocketFlags.None);
                    // Step1:接收HTTP请求
                    string requestText = Encoding.Default.GetString(data, 0, length);
                    HttpContext context = new HttpContext(requestText);
                    // Step2:处理HTTP请求
                    HttpApplication application = new HttpApplication();
                    application.ProcessRequest(context);
                    // Step3:响应HTTP请求
                    proxSocket.Send(context.Response.GetResponseHeader());
                    proxSocket.Send(context.Response.Body);
                    // Step4:即时关闭Socket连接
                    proxSocket.Shutdown(SocketShutdown.Both);
                    proxSocket.Close();
                }
                catch (Exception err)
                {
                    Console.WriteLine(err.ToString());
                    //proxSocket.Shutdown(SocketShutdown.Both);
                    if (proxSocket != null)
                        proxSocket.Close();
                }
                Thread.Sleep(10);
            }
        }


        /// <summary>
        /// 获取本机所有ip地址
        /// </summary>
        /// <param name="netType">"InterNetwork":ipv4地址，"InterNetworkV6":ipv6地址</param>
        /// <returns>ip地址集合</returns>
        public static List<string> GetLocalIpAddress(string netType)
        {
            string hostName = Dns.GetHostName();                    //获取主机名称  
            IPAddress[] addresses = Dns.GetHostAddresses(hostName); //解析主机IP地址  

            List<string> IPList = new List<string>();
            if (netType == string.Empty)
            {
                for (int i = 0; i < addresses.Length; i++)
                {
                    IPList.Add(addresses[i].ToString());
                }
            }
            else
            {
                //AddressFamily.InterNetwork表示此IP为IPv4,
                //AddressFamily.InterNetworkV6表示此地址为IPv6类型
                for (int i = 0; i < addresses.Length; i++)
                {
                    if (addresses[i].AddressFamily.ToString() == netType)
                    {
                        IPList.Add(addresses[i].ToString());
                    }
                }
            }
            return IPList;
        }

        #region 指定类型的端口是否已经被使用了
        /// <summary>
        /// 指定类型的端口是否已经被使用了
        /// </summary>
        /// <param name="port">端口号</param>
        /// <param name="type">端口类型</param>
        /// <returns></returns>
        public static bool portInUse(int port)
        {
            bool flag = false;
            IPGlobalProperties properties = IPGlobalProperties.GetIPGlobalProperties();
            IPEndPoint[] ipendpoints = null;

            ipendpoints = properties.GetActiveTcpListeners();

            foreach (IPEndPoint ipendpoint in ipendpoints)
            {
                if (ipendpoint.Port == port)
                {
                    flag = true;
                    break;
                }
            }
            ipendpoints = null;
            properties = null;
            return flag;
        }
        #endregion
    }
}
