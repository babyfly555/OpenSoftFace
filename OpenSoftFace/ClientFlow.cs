﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenSoftFace
{
    class ClientFlow
    {
        public static int inNum = 0;
        public static int outNum = 0;
        public static String totleInOut="";
        public static List<Rectangle> rectsBefore=new List<Rectangle>();
        public static List<Rectangle> rectsNow = new List<Rectangle>();
        public static Rectangle rect=new Rectangle();
        public static string INOUTArea = "";
        public static IniHelper iniHelper = new IniHelper("setting.ini", "setting");
        public static void setRect(int width, int height, List<Rectangle> rectsNowInput)
        {
            if (totleInOut == "")
            {
                inNum = int.Parse(iniHelper.Get("INNUM"));
                outNum = int.Parse(iniHelper.Get("OUTNUM"));
                string INOUTDate = iniHelper.Get("INOUTDate");
                if (INOUTDate != DateTime.Now.ToString("yyyy-MM-dd"))
                {
                    inNum = 0;
                    outNum = 0;
                    iniHelper.Set("INNUM", inNum.ToString());
                    iniHelper.Set("OUTNUM", outNum.ToString());
                    INOUTDate = DateTime.Now.ToString("yyyy-MM-dd");
                    iniHelper.Set("INOUTDate", INOUTDate);
                }
                totleInOut = INOUTDate;
            }
            else if (totleInOut != DateTime.Now.ToString("yyyy-MM-dd"))
            {
                inNum = 0;
                outNum = 0;
                iniHelper.Set("INNUM", inNum.ToString());
                iniHelper.Set("OUTNUM", outNum.ToString());
                totleInOut = DateTime.Now.ToString("yyyy-MM-dd");
            }
            if (INOUTArea == "") {
                INOUTArea = iniHelper.Get("INOUTArea");
                string[] INOUTAreas = INOUTArea.Split('-');
                int x=0, y=0, widthx=0, heightx=0;
                x=int.Parse((width * float.Parse(INOUTAreas[0])).ToString("#######"));
                y=int.Parse((height * float.Parse(INOUTAreas[1])).ToString("#######"));
                widthx=int.Parse((width * float.Parse(INOUTAreas[2])).ToString("#######"));
                heightx=int.Parse((height * float.Parse(INOUTAreas[3])).ToString("#######"));
                rect = new Rectangle(x, y, widthx, heightx);
            }
            run(rectsNowInput);
        }
        public static Rectangle getRect() {
            return rect;
        }
        public static void run(List<Rectangle> rectsNowInput)
        {
            try { 
                rectsNow = rectsNowInput;
                foreach (Rectangle itemNow in rectsNow)
                {
                    foreach (Rectangle itemBefore in rectsBefore)
                    {
                        bool with=itemNow.IntersectsWith(itemNow);
                        if (rect.Contains(itemNow) && !rect.Contains(itemBefore)) {
                                inNum = inNum + 1;
                                //IniHelper iniHelper = new IniHelper("setting.ini", "setting");
                                iniHelper.Set("INNUM", inNum.ToString());
                                if (iniHelper.Get("TotalServer") != null && iniHelper.Get("TotalServer")!="")
                                { 
                                    String url = iniHelper.Get("TotalServer") + "&status=0&timestamp=" + DateTime.Now.ToLocalTime().ToString("yyyy-MM-dd");
                                    HttpMethods.HttpGet(url);
                                }
                                break;
                        }
                        if (!rect.Contains(itemNow) && rect.Contains(itemBefore))
                        {
                                outNum = outNum + 1;
                                //IniHelper iniHelper = new IniHelper("setting.ini", "setting");
                                iniHelper.Set("OUTNUM", outNum.ToString());
                                if (iniHelper.Get("TotalServer") != null && iniHelper.Get("TotalServer") != "") { 
                                    String url = iniHelper.Get("TotalServer") + "&status=1&timestamp=" + DateTime.Now.ToLocalTime().ToString("yyyy-MM-dd");
                                    HttpMethods.HttpGet(url);
                                }
                                break;
                        }
                    }
                }
                rectsBefore = rectsNowInput;
            }
            catch (Exception err) { }
        }
    }
}
