﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using OpenSoftFace.SDKModels;
using OpenSoftFace.SDKUtil;
using OpenSoftFace.Utils;
using OpenSoftFace.Entity;
using System.IO;
using System.Configuration;
using System.Threading;
using AForge.Video.DirectShow;
using System.Linq;
using System.Globalization;
using System.Media;
using System.Threading.Tasks;
using System.Diagnostics;
using DAL.Service;
using DAL.Service.Impl;
using Model.Entity;
using System.Drawing.Drawing2D;
using System.ServiceModel.Web;
using System.Web;
using NVR;
namespace OpenSoftFace
{
    public partial class FaceForm : Form
    {
        #region 参数定义
        /// <summary>
        /// 引擎Handle
        /// </summary>
        private IntPtr pImageEngine = IntPtr.Zero;

        /// <summary>
        /// 保存右侧图片路径
        /// </summary>
        private string image1Path;

        /// <summary>
        /// 图片最大大小
        /// </summary>
        private long maxSize = 1024 * 1024 * 2;

        /// <summary>
        /// 右侧图片人脸特征
        /// </summary>
        private IntPtr image1Feature;

        /// <summary>
        /// 保存对比图片的列表
        /// </summary>
        private List<string> imagePathList = new List<string>();

        /// <summary>
        /// 左侧图库人脸特征列表
        /// </summary>
        private List<IntPtr> imagesFeatureList = new List<IntPtr>();

        /// <summary>
        /// 相似度
        /// </summary>
        private float threshold = 0.8f;

        /// <summary>
        /// 用于标记是否需要清除比对结果
        /// </summary>
        private bool isCompare = false;

        /// <summary>
        /// 是否是双目摄像
        /// </summary>
        private bool isDoubleShot = false;

        /// <summary>
        /// 允许误差范围
        /// </summary>
        private int allowAbleErrorRange = 40;

        /// <summary>
        /// RGB 摄像头索引
        /// </summary>
        private int rgbCameraIndex = 0;
        /// <summary>
        /// IR 摄像头索引
        /// </summary>
        private int irCameraIndex = 0;

        #region 视频模式下相关
        /// <summary>
        /// 视频引擎Handle
        /// </summary>
        private IntPtr pVideoEngine = IntPtr.Zero;
        /// <summary>
        /// RGB视频引擎 FR Handle 处理   FR和图片引擎分开，减少强占引擎的问题
        /// </summary>
        private IntPtr pVideoRGBImageEngine = IntPtr.Zero;
        /// <summary>
        /// IR视频引擎 FR Handle 处理   FR和图片引擎分开，减少强占引擎的问题
        /// </summary>
        private IntPtr pVideoIRImageEngine = IntPtr.Zero;
        /// <summary>
        /// 视频输入设备信息
        /// </summary>
        private FilterInfoCollection filterInfoCollection;
        /// <summary>
        /// RGB摄像头设备
        /// </summary>
        private VideoCaptureDevice rgbDeviceVideo;
        /// <summary>
        /// IR摄像头设备
        /// </summary>
        private VideoCaptureDevice irDeviceVideo;
        #endregion

        #endregion

        public SoundPlayer _successVoice;
        public SoundPlayer _errorVoice;

        private IMainInfoService personInfoService;

        private IAttendanceRecordService attendanceRecordService;
        private List<MainInfo> mainInfos;
        //private List<AttendanceRecord> queryForLists;

        private String staff_image="";
        private String staff_name="";
        private String staff_date = "";
        private String staff_dept = "";
        private Boolean staff_visible = false;
        private IniHelper iniHelper = new IniHelper();

        #region 初始化
        public FaceForm()
        {
            InitializeComponent();
            CheckForIllegalCrossThreadCalls = false;
            //初始化引擎
            InitEngines();
            //隐藏摄像头图像窗口
            //rgbVideoSource.Hide();
            //irVideoSource.Hide();
            //阈值控件不可用
            txtThreshold.Enabled = false;
            //pan_kq.Visible = false;
            ReloadFace();
            if (btnStartVideo.Enabled == true) { 
                //btnStartVideo_Click(null,null);
            }
            this.personInfoService = new MainInfoServiceImpl();
            this.attendanceRecordService = new AttendanceRecordServiceImpl();
            this.Opacity=0.98;
            if (iniHelper.Dict["style"] == "easy") { 
                //this.panel1.Visible=false;
                this.panel2.Visible=false;
                //this.panel1.Visible = false;
                //this.pic_max.Visible=false;
                //this.pictureBox_close.Visible = false;
                //this.lab_company_info.Visible = false;
            }


            GraphicsPath gp = new GraphicsPath();

            gp.AddEllipse(pic_face.ClientRectangle);

            Region region = new Region(gp);

            pic_face.Region = region;

            gp.Dispose();

            region.Dispose();

            /*GraphicsPath gp = new GraphicsPath();
            gp.AddEllipse(pic_face.ClientRectangle);
            Region region = new Region(gp);
            pic_face.Region = region;
            gp.Dispose();
            region.Dispose();*/
            mainInfos = this.personInfoService.QueryForList();
            this.BackgroundImage = (Image)Image.FromFile(Path.Combine("Resources", iniHelper.Dict["BackgroundImage"])).Clone();
            /*Task.Run(() =>
            {
                while (true)
                {
                    Thread.Sleep(60000);
                     try
                     {
                        mainInfos = this.personInfoService.QueryForList();
                        //queryForLists = this.attendanceRecordService.QueryForList();
                     }
                     catch (Exception err) { }
                }
            });*/

            /*Task.Run(() =>
            {
                while (true)
                {
                    try
                    {
                        Thread.Sleep(600000);
                        this.Invoke(new Action<Image>(p =>
                        {
                            try
                            {
                                Random rd = new Random();
                                int sjs = rd.Next(1, 9);
                                this.BackgroundImage = (Image)Image.FromFile(Path.Combine("Resources", "bg" + sjs + ".jpg")).Clone();
                            }
                            catch (Exception err) { }
                        }), this.BackgroundImage);
                             
                    }
                    catch (Exception err) { }
                }
            });*/

            Task.Run(() =>
            {
                while (true)
                {
                    Thread.Sleep(10);
                    if (!staff_visible) {
                        try
                        {
                            pan_kq.Invoke(new Action<Boolean>(p =>
                            {
                                pan_kq.Visible = false;
                            }), pan_kq.Visible);
                            Thread.Sleep(100);
                            continue;
                        }
                        catch (Exception err) { }
                    }
                    try
                    {
                        pan_kq.Location = new System.Drawing.Point(this.Width - pan_kq.Width - 2, Convert.ToInt32(this.Height * 0.5) - 99);
                        pan_kq.Invoke(new Action<Boolean>(p =>
                        {
                            pan_kq.Visible = true;
                        }), pan_kq.Visible);

                        pic_face.Invoke(new Action<String>(p =>
                        {
                            pic_face.ImageLocation = staff_image;
                        }), pic_face.ImageLocation);

                        pic_facex.Invoke(new Action<String>(p =>
                        {
                            pic_facex.ImageLocation = staff_image;
                        }), pic_facex.ImageLocation);

                        lab_dept.Invoke(new Action<String>(p =>
                        {
                            lab_dept.Text = staff_dept;
                        }), lab_dept.Text);


                        lab_name.Invoke(new Action<String>(p =>
                        {
                            lab_name.Text = staff_name;
                        }), lab_name.Text);

                        lab_date.Invoke(new Action<String>(p =>
                        {
                            lab_date.Text = staff_date;
                        }), lab_date.Text);
                        staff_visible = false;
                        Thread.Sleep(2000);
                    }
                    catch (Exception err) { }
                }
            });
        }

        //Environment.GetEnvironmentVariable("ProgramData")
        public string Repository = Application.StartupPath + "\\OpenSoftFaceFile\\show\\";
        public List<string> imagePathListTemp = new List<string>();
        List<string> imagePathListTempName = new List<string>();
        public void ReloadFace()
        {
            //IniHelper iniHelper = new IniHelper();
            if (iniHelper.Dict["COMPANY_INFO"] != null && iniHelper.Dict["COMPANY_INFO"] != "") {
                lab_company_info.Text = HttpUtility.UrlDecode(iniHelper.Dict["COMPANY_INFO"]);
            }
            pic_logo.ImageLocation = HttpUtility.UrlDecode(iniHelper.Dict["IMAGE_LOGO"]);

            if(pic_logo.ImageLocation==""){
                pic_logo.ImageLocation = Path.Combine("Resources", "logo.jpg");
            }
            //IniHelper iniHelper = new IniHelper();
            //iniHelper.Get("path");
            var files = Directory.GetFiles(Repository).Where(w => w.EndsWith("jpg"));
            imagePathListTemp = new List<string>();
            imagePathListTempName = new List<string>();
            var numStart = imagePathList.Count;
            int isGoodImage = 0;
            imageList.Refresh();
            foreach (var file in files)
            {
                var fi = new FileInfo(file);
                var name = fi.DirectoryName+"\\"+fi.Name;
                //图片格式判断
                if (checkImage(name)) { 
                    imagePathListTemp.Add(name);
                    imagePathListTempName.Add(fi.Name.Substring(0, fi.Name.Length-4));
                }
            }

            //人脸检测以及提取人脸特征
            ThreadPool.QueueUserWorkItem(new WaitCallback(delegate
            {
                try
                {
                    //禁止点击按钮
                    Invoke(new Action(delegate
                    {
                        chooseMultiImgBtn.Enabled = false;
                        matchBtn.Enabled = false;
                        btnClearFaceList.Enabled = false;
                        chooseImgBtn.Enabled = false;
                        btnStartVideo.Enabled = false;
                    }));
                }
                catch (Exception err) { }
                //人脸检测和剪裁
                for (int i = 0; i < imagePathListTemp.Count; i++)
                {
                    Image image = ImageUtil.readFromFile(imagePathListTemp[i]);
                    if(image == null)
                    {
                        continue;
                    }
                    if (image.Width > 1536 || image.Height > 1536)
                    {
                        image = ImageUtil.ScaleImage(image, 1536, 1536);
                    }
                    if (image == null)
                    {
                        continue;
                    }
                    if (image.Width % 4 != 0)
                    {
                        image = ImageUtil.ScaleImage(image, image.Width - (image.Width % 4), image.Height);
                    }
                    //人脸检测
                    ASF_MultiFaceInfo multiFaceInfo = FaceUtil.DetectFace(pImageEngine, image);
                    //判断检测结果
                    if (multiFaceInfo.faceNum > 0)
                    {
                        imagePathList.Add(imagePathListTemp[i]);
                        MRECT rect = MemoryUtil.PtrToStructure<MRECT>(multiFaceInfo.faceRects);
                        image = ImageUtil.CutImage(image, rect.left, rect.top, rect.right, rect.bottom);
                    }
                    else
                    {
                        if(image != null)
                        {
                            image.Dispose();
                        }
                        continue;
                    }
                    //显示人脸
                    this.Invoke(new Action(delegate
                    {
                        if (image == null)
                        {
                            image = ImageUtil.readFromFile(imagePathListTemp[i]);

                            if (image.Width > 1536 || image.Height > 1536)
                            {
                                image = ImageUtil.ScaleImage(image, 1536, 1536);
                            }
                        }
                        imageLists.Images.Add(imagePathListTemp[i], image);
                        imageList.Items.Add((numStart + isGoodImage + imagePathListTempName[i]) + "号", imagePathListTemp[i]);
                        imageList.Refresh();
                        isGoodImage += 1;
                        if (image != null)
                        {
                            image.Dispose();
                        }
                    }));
                }

                //提取人脸特征
                for (int i = numStart; i < imagePathList.Count; i++)
                {
                    ASF_SingleFaceInfo singleFaceInfo = new ASF_SingleFaceInfo();
                    Image image = ImageUtil.readFromFile(imagePathList[i]);
                    if (image == null)
                    {
                        continue;
                    }
                    IntPtr feature = FaceUtil.ExtractFeature(pImageEngine, image, out singleFaceInfo);
                    this.Invoke(new Action(delegate
                    {
                        if (singleFaceInfo.faceRect.left == 0 && singleFaceInfo.faceRect.right == 0)
                        {
                            AppendText(string.Format("{0}号未检测到人脸\r\n", i));
                        }
                        else
                        {
                            AppendText(string.Format("已提取{0}号人脸特征值，[left:{1},right:{2},top:{3},bottom:{4},orient:{5}]\r\n", i, singleFaceInfo.faceRect.left, singleFaceInfo.faceRect.right, singleFaceInfo.faceRect.top, singleFaceInfo.faceRect.bottom, singleFaceInfo.faceOrient));
                            imagesFeatureList.Add(feature);
                        }
                    }));
                    if (image != null)
                    {
                        image.Dispose();
                    }
                }
                //允许点击按钮
                Invoke(new Action(delegate
                {
                    chooseMultiImgBtn.Enabled = true;
                    btnClearFaceList.Enabled = true;
                    btnStartVideo.Enabled = true;

                    if (btnStartVideo.Text == "启用摄像头")
                    {
                        chooseImgBtn.Enabled = true;
                        matchBtn.Enabled = true;
                        btnStartVideo_Click(null, null);
                    }
                    else
                    {
                        chooseImgBtn.Enabled = false;
                        matchBtn.Enabled = false;
                    }
                }));

                var successFile = Path.Combine("Resources", "success.wav");
                if (File.Exists(successFile))
                {
                    _successVoice = new SoundPlayer(successFile);
                    //Log.InfoFormat("加载success音效文件成功:{0}", successFile);
                }

                var errorFile = Path.Combine("Resources", "error.wav");
                if (File.Exists(errorFile))
                {
                    _errorVoice = new SoundPlayer(errorFile);
                    //Log.InfoFormat("加载error音效文件成功:{0}", errorFile);
                }

            }));
        }

        /// <summary>
        /// 初始化引擎
        /// </summary>
        private void InitEngines()
        {
            //IniHelper iniHelper = new IniHelper();
            //读取配置文件
            AppSettingsReader reader = new AppSettingsReader();
            //string appId = (string)reader.GetValue("APP_ID", typeof(string));
            //string sdkKey64 = (string)reader.GetValue("SDKKEY64", typeof(string));
            //string sdkKey32 = (string)reader.GetValue("SDKKEY32", typeof(string));
            //string appId ="";
            //string sdkKey64 ="";
            //string sdkKey32 = "";
            string appId = iniHelper.Get("APPID");
            string sdkKey64 = iniHelper.Get("SDKKEY");
            string sdkKey32 = iniHelper.Get("SDKKEY");
            rgbCameraIndex = (int)reader.GetValue("RGB_CAMERA_INDEX", typeof(int));
            irCameraIndex = (int)reader.GetValue("IR_CAMERA_INDEX", typeof(int));

            threshold = float.Parse(iniHelper.Get("thresh"));
            //判断CPU位数
            var is64CPU = Environment.Is64BitProcess;
            if (string.IsNullOrWhiteSpace(appId) || string.IsNullOrWhiteSpace(is64CPU?sdkKey64: sdkKey32))
            {
                //禁用相关功能按钮
                ControlsEnable(false, chooseMultiImgBtn, matchBtn, btnClearFaceList, chooseImgBtn);
                MessageBox.Show(string.Format("请在setting.ini配置文件中先配置APP_ID和SDKKEY{0}!", is64CPU ? "64" : "32"));
                return;
            }

            //在线激活引擎    如出现错误，1.请先确认从官网下载的sdk库已放到对应的bin中，2.当前选择的CPU为x86或者x64
            int retCode = 0;
            try
            {
                retCode = ASFFunctions.ASFActivation(appId, is64CPU ? sdkKey64 : sdkKey32);
                bool flag3 = retCode == 0 || retCode == 90114;
                for (int i = 1; i <= 20; i++) {
                    if (!flag3) {
                        string num = "";
                        if (i < 10)
                        {
                            num = "0" + i;
                        }
                        else {
                            num = i.ToString();
                        }
                        appId = iniHelper.Get("APPID_" + num);
                        sdkKey64 = iniHelper.Get("SDKKEY_" + num);
                        retCode = ASFFunctions.ASFActivation(appId, sdkKey64);
                        flag3 = retCode == 0 || retCode == 90114;
                        if (flag3) {
                            iniHelper.Set("APPID", appId);
                            iniHelper.Set("SDKKEY", sdkKey64);
                            break;
                        }
                    }
                }
                if (!flag3)
                {
                    MessageBox.Show("激活引擎失败!请在setting.ini配置APP_ID和SDKKEY。提供免费APP_ID和SDKKEY。");
                }
                else
                {
                    //MessageBox.Show("激活引擎失败!请在setting.ini配置APP_ID和SDKKEY。提供免费APP_ID和SDKKEY。");
                }
            }
            catch (Exception ex)
            {
                //禁用相关功能按钮
                ControlsEnable(false, chooseMultiImgBtn, matchBtn, btnClearFaceList, chooseImgBtn);
                if (ex.Message.Contains("无法加载 DLL"))
                {
                    MessageBox.Show("请将sdk相关DLL放入bin对应的x86或x64下的文件夹中!");
                }
                else
                {
                    MessageBox.Show("激活引擎失败!");
                }
                return;
            }
            Console.WriteLine("Activate Result:" + retCode);

            //初始化引擎
            uint detectMode = DetectionMode.ASF_DETECT_MODE_IMAGE;
            //Video模式下检测脸部的角度优先值
            int videoDetectFaceOrientPriority = ASF_OrientPriority.ASF_OP_0_HIGHER_EXT;
            //Image模式下检测脸部的角度优先值
            int imageDetectFaceOrientPriority = ASF_OrientPriority.ASF_OP_0_ONLY;
            //人脸在图片中所占比例，如果需要调整检测人脸尺寸请修改此值，有效数值为2-32
            int detectFaceScaleVal = 16;
            //最大需要检测的人脸个数
            int detectFaceMaxNum = 5;
            //引擎初始化时需要初始化的检测功能组合
            int combinedMask = FaceEngineMask.ASF_FACE_DETECT | FaceEngineMask.ASF_FACERECOGNITION | FaceEngineMask.ASF_AGE | FaceEngineMask.ASF_GENDER | FaceEngineMask.ASF_FACE3DANGLE;
            //初始化引擎，正常值为0，其他返回值请参考http://ai.arcsoft.com.cn/bbs/forum.php?mod=viewthread&tid=19&_dsign=dbad527e
            retCode = ASFFunctions.ASFInitEngine(detectMode, imageDetectFaceOrientPriority, detectFaceScaleVal, detectFaceMaxNum, combinedMask, ref pImageEngine);
            Console.WriteLine("InitEngine Result:" + retCode);
            AppendText((retCode == 0) ? "引擎初始化成功!\n" : string.Format("引擎初始化失败!错误码为:{0}\n", retCode));
            if (retCode != 0)
            {
                //禁用相关功能按钮
                ControlsEnable(false, chooseMultiImgBtn, matchBtn, btnClearFaceList, chooseImgBtn);
            }

            //初始化视频模式下人脸检测引擎
            uint detectModeVideo = DetectionMode.ASF_DETECT_MODE_VIDEO;
            int combinedMaskVideo = FaceEngineMask.ASF_FACE_DETECT | FaceEngineMask.ASF_FACERECOGNITION;
            retCode = ASFFunctions.ASFInitEngine(detectModeVideo, videoDetectFaceOrientPriority, detectFaceScaleVal, detectFaceMaxNum, combinedMaskVideo, ref pVideoEngine);
            //RGB视频专用FR引擎
            detectFaceMaxNum = 1;
            combinedMask = FaceEngineMask.ASF_FACE_DETECT | FaceEngineMask.ASF_FACERECOGNITION | FaceEngineMask.ASF_LIVENESS;
            retCode = ASFFunctions.ASFInitEngine(detectMode, imageDetectFaceOrientPriority, detectFaceScaleVal, detectFaceMaxNum, combinedMask, ref pVideoRGBImageEngine);

            //IR视频专用FR引擎
            combinedMask = FaceEngineMask.ASF_FACE_DETECT  | FaceEngineMask.ASF_FACERECOGNITION | FaceEngineMask.ASF_IR_LIVENESS;
            retCode = ASFFunctions.ASFInitEngine(detectMode, imageDetectFaceOrientPriority, detectFaceScaleVal, detectFaceMaxNum, combinedMask, ref pVideoIRImageEngine);

            Console.WriteLine("InitVideoEngine Result:" + retCode);


            initVideo();
        }

        /// <summary>
        /// 摄像头初始化
        /// </summary>
        private void initVideo()
        {
            filterInfoCollection = new FilterInfoCollection(FilterCategory.VideoInputDevice);
            //如果没有可用摄像头，“启用摄像头”按钮禁用，否则使可用
            if (filterInfoCollection.Count == 0)
            {
                btnStartVideo.Enabled = false;
            }
            else
            {
                btnStartVideo.Enabled = true;
            }
        }

        #endregion

        #region 注册人脸按钮事件

        private object locker = new object();
        /// <summary>
        /// 人脸库图片选择按钮事件
        /// </summary>
        private void ChooseMultiImg(object sender, EventArgs e)
        {
            lock (locker)
            {
                string fileName = System.Environment.CurrentDirectory+"/setting/OpenSoftFace.Setting.exe";
                if (File.Exists(fileName))
                    Process.Start(fileName);
                else
                    MessageBox.Show("not exist");
                return;
                //OpenSoftFace.Setting.Forms.LoginForm loginForm = new OpenSoftFace.Setting.Forms.LoginForm();
                //loginForm.Show();
                Task.Run(() =>
                {
                    Thread.Sleep(10000);
                    this.imageList.Visible = false;
                });

                imageList.Visible = true;
                OpenFileDialog openFileDialog = new OpenFileDialog();
                openFileDialog.Title = "选择图片";
                openFileDialog.Filter = "图片文件|*.bmp;*.jpg;*.jpeg;*.png";
                openFileDialog.Multiselect = true;
                openFileDialog.FileName = string.Empty;
                imageList.Refresh();
                if (openFileDialog.ShowDialog() == DialogResult.OK)
                {

                    List<string> imagePathListTemp = new List<string>();
                    //imagePathListTempName = new List<string>();
                    var numStart = imagePathList.Count;
                    int isGoodImage = 0;

                    //保存图片路径并显示
                    string[] fileNames = openFileDialog.FileNames;
                    for (int i = 0; i < fileNames.Length; i++)
                    {
                        //图片格式判断
                        if (checkImage(fileNames[i])) { 
                            imagePathListTemp.Add(fileNames[i]);
                            imagePathListTempName.Add(fileNames[i]);
                        }
                    }

                    //人脸检测以及提取人脸特征
                    ThreadPool.QueueUserWorkItem(new WaitCallback(delegate
                    {
                        //禁止点击按钮
                        Invoke(new Action(delegate
                        {
                            chooseMultiImgBtn.Enabled = false;
                            matchBtn.Enabled = false;
                            btnClearFaceList.Enabled = false;
                            chooseImgBtn.Enabled = false;
                            btnStartVideo.Enabled = false;
                        }));

                        //人脸检测和剪裁
                        for (int i = 0; i < imagePathListTemp.Count; i++)
                        {
                            Image image = ImageUtil.readFromFile(imagePathListTemp[i]);
                            if(image == null)
                            {
                                continue;
                            }
                            if (image.Width > 1536 || image.Height > 1536)
                            {
                                image = ImageUtil.ScaleImage(image, 1536, 1536);
                            }
                            if (image == null)
                            {
                                continue;
                            }
                            if (image.Width % 4 != 0)
                            {
                                image = ImageUtil.ScaleImage(image, image.Width - (image.Width % 4), image.Height);
                            }
                            //人脸检测
                            ASF_MultiFaceInfo multiFaceInfo = FaceUtil.DetectFace(pImageEngine, image);
                            //判断检测结果
                            if (multiFaceInfo.faceNum > 0)
                            {
                                imagePathList.Add(imagePathListTemp[i]);
                                MRECT rect = MemoryUtil.PtrToStructure<MRECT>(multiFaceInfo.faceRects);
                                image = ImageUtil.CutImage(image, rect.left, rect.top, rect.right, rect.bottom);
                            }
                            else
                            {
                                if(image != null)
                                {
                                    image.Dispose();
                                }
                                continue;
                            }
                            //显示人脸
                            this.Invoke(new Action(delegate
                            {
                                if (image == null)
                                {
                                    image = ImageUtil.readFromFile(imagePathListTemp[i]);

                                    if (image.Width > 1536 || image.Height > 1536)
                                    {
                                        image = ImageUtil.ScaleImage(image, 1536, 1536);
                                    }
                                }
                                imageLists.Images.Add(imagePathListTemp[i], image);
                                string fileNameTemp = System.IO.Path.GetFileName(imagePathListTemp[i]);
                                imageList.Items.Add((numStart + isGoodImage + fileNameTemp.Substring(0, fileNameTemp.Length - 4)) + "号", imagePathListTemp[i]);
                                image.Save("./facedb/" + fileNameTemp);
                                imageList.Refresh();
                                isGoodImage += 1;
                                if (image != null)
                                {
                                    image.Dispose();
                                }
                            }));
                        }

                        //提取人脸特征
                        for (int i = numStart; i < imagePathList.Count; i++)
                        {
                            ASF_SingleFaceInfo singleFaceInfo = new ASF_SingleFaceInfo();
                            Image image = ImageUtil.readFromFile(imagePathList[i]);
                            if (image == null)
                            {
                                continue;
                            }
                            IntPtr feature = FaceUtil.ExtractFeature(pImageEngine, image, out singleFaceInfo);
                            this.Invoke(new Action(delegate
                            {
                                if (singleFaceInfo.faceRect.left == 0 && singleFaceInfo.faceRect.right == 0)
                                {
                                    AppendText(string.Format("{0}号未检测到人脸\r\n", i));
                                }
                                else
                                {
                                    AppendText(string.Format("已提取{0}号人脸特征值，[left:{1},right:{2},top:{3},bottom:{4},orient:{5}]\r\n", i, singleFaceInfo.faceRect.left, singleFaceInfo.faceRect.right, singleFaceInfo.faceRect.top, singleFaceInfo.faceRect.bottom, singleFaceInfo.faceOrient));
                                    imagesFeatureList.Add(feature);
                                }
                            }));
                            if (image != null)
                            {
                                image.Dispose();
                            }
                        }
                        //允许点击按钮
                        Invoke(new Action(delegate
                        {
                            chooseMultiImgBtn.Enabled = true;
                            btnClearFaceList.Enabled = true;
                            btnStartVideo.Enabled = true;

                            if (btnStartVideo.Text == "启用摄像头")
                            {
                                chooseImgBtn.Enabled = true;
                                matchBtn.Enabled = true;
                            }
                            else
                            {
                                chooseImgBtn.Enabled = false;
                                matchBtn.Enabled = false;
                            }
                        }));
                    }));

                }
            }
        }
        #endregion

        #region 清空人脸库按钮事件
        /// <summary>
        /// 清除人脸库事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void btnClearFaceList_Click(object sender, EventArgs e)
        {
            //清除数据
            imageLists.Images.Clear();
            imageList.Items.Clear();
            foreach (IntPtr intptr in imagesFeatureList)
            {
                MemoryUtil.Free(intptr);
            }
            imagesFeatureList.Clear();
            imagePathList.Clear();
        }
        #endregion

        #region 选择识别图按钮事件
        /// <summary>
        /// “选择识别图片”按钮事件
        /// </summary>
        private void ChooseImg(object sender, EventArgs e)
        {
            lblCompareInfo.Text = "";
            //判断引擎是否初始化成功
            if (pImageEngine == IntPtr.Zero)
            {
                //禁用相关功能按钮
                ControlsEnable(false, chooseMultiImgBtn, matchBtn, btnClearFaceList, chooseImgBtn);
                MessageBox.Show("请先初始化引擎!");
                return;
            }
            nvr = false;
            //选择图片
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Title = "选择图片";
            openFileDialog.Filter = "图片文件|*.bmp;*.jpg;*.jpeg;*.png";
            openFileDialog.Multiselect = false;
            openFileDialog.FileName = string.Empty;
            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {

                image1Path = openFileDialog.FileName;
                //检测图片格式
                if (!checkImage(image1Path))
                {
                    return;
                }
                DateTime detectStartTime = DateTime.Now;
                AppendText(string.Format("------------------------------开始检测，时间:{0}------------------------------\n", detectStartTime.ToString("yyyy-MM-dd HH:mm:ss:ms")));

                //获取文件，拒绝过大的图片
                FileInfo fileInfo = new FileInfo(image1Path);
                if (fileInfo.Length > maxSize)
                {
                    MessageBox.Show("图像文件最大为2MB，请压缩后再导入!");
                    AppendText(string.Format("------------------------------检测结束，时间:{0}------------------------------\n", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss:ms")));
                    AppendText("\n");
                    return;
                }

                Image srcImage = ImageUtil.readFromFile(image1Path);
                if (srcImage == null)
                {
                    MessageBox.Show("图像数据获取失败，请稍后重试!");
                    AppendText(string.Format("------------------------------检测结束，时间:{0}------------------------------\n", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss:ms")));
                    AppendText("\n");
                    return;
                }
                if (srcImage.Width > 1536 || srcImage.Height > 1536)
                {
                    srcImage = ImageUtil.ScaleImage(srcImage, 1536, 1536);
                }
                if (srcImage == null)
                {
                    MessageBox.Show("图像数据获取失败，请稍后重试!");
                    AppendText(string.Format("------------------------------检测结束，时间:{0}------------------------------\n", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss:ms")));
                    AppendText("\n");
                    return;
                }
                //调整图像宽度，需要宽度为4的倍数
                if (srcImage.Width % 4 != 0)
                {
                    srcImage = ImageUtil.ScaleImage(srcImage, srcImage.Width - (srcImage.Width % 4), srcImage.Height);
                }
                //调整图片数据，非常重要
                ImageInfo imageInfo = ImageUtil.ReadBMP(srcImage);
                if (imageInfo == null)
                {
                    MessageBox.Show("图像数据获取失败，请稍后重试!");
                    AppendText(string.Format("------------------------------检测结束，时间:{0}------------------------------\n", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss:ms")));
                    AppendText("\n");
                    return;
                }
                //人脸检测
                ASF_MultiFaceInfo multiFaceInfo = FaceUtil.DetectFace(pImageEngine, imageInfo);
                //年龄检测
                int retCode_Age = -1;
                ASF_AgeInfo ageInfo = FaceUtil.AgeEstimation(pImageEngine, imageInfo, multiFaceInfo, out retCode_Age);
                //性别检测
                int retCode_Gender = -1;
                ASF_GenderInfo genderInfo = FaceUtil.GenderEstimation(pImageEngine, imageInfo, multiFaceInfo, out retCode_Gender);

                //3DAngle检测
                int retCode_3DAngle = -1;
                ASF_Face3DAngle face3DAngleInfo = FaceUtil.Face3DAngleDetection(pImageEngine, imageInfo, multiFaceInfo, out retCode_3DAngle);

                MemoryUtil.Free(imageInfo.imgData);

                if (multiFaceInfo.faceNum < 1)
                {
                    srcImage = ImageUtil.ScaleImage(srcImage, picImageCompare.Width, picImageCompare.Height);
                    image1Feature = IntPtr.Zero;
                    picImageCompare.Image = srcImage;
                    AppendText(string.Format("{0} - 未检测出人脸!\n\n", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")));
                    AppendText(string.Format("------------------------------检测结束，时间:{0}------------------------------\n", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss:ms")));
                    AppendText("\n");
                    return;
                }

                MRECT temp = new MRECT();
                int ageTemp = 0;
                int genderTemp = 0;
                int rectTemp = 0;

                //标记出检测到的人脸
                for (int i = 0; i < multiFaceInfo.faceNum; i++)
                {
                    MRECT rect = MemoryUtil.PtrToStructure<MRECT>(multiFaceInfo.faceRects + MemoryUtil.SizeOf<MRECT>() * i);
                    int orient = MemoryUtil.PtrToStructure<int>(multiFaceInfo.faceOrients + MemoryUtil.SizeOf<int>() * i);
                    int age = 0;

                    if (retCode_Age != 0)
                    {
                        AppendText(string.Format("年龄检测失败，返回{0}!\n\n", retCode_Age));
                    }
                    else
                    {
                        age = MemoryUtil.PtrToStructure<int>(ageInfo.ageArray + MemoryUtil.SizeOf<int>() * i);
                    }

                    int gender = -1;
                    if (retCode_Gender != 0)
                    {
                        AppendText(string.Format("性别检测失败，返回{0}!\n\n", retCode_Gender));
                    }
                    else
                    {
                        gender = MemoryUtil.PtrToStructure<int>(genderInfo.genderArray + MemoryUtil.SizeOf<int>() * i);
                    }

                    int face3DStatus = -1;
                    float roll = 0f;
                    float pitch = 0f;
                    float yaw = 0f;
                    if (retCode_3DAngle != 0)
                    {
                        AppendText(string.Format("3DAngle检测失败，返回{0}!\n\n", retCode_3DAngle));
                    }
                    else
                    {
                        //角度状态 非0表示人脸不可信
                        face3DStatus = MemoryUtil.PtrToStructure<int>(face3DAngleInfo.status + MemoryUtil.SizeOf<int>() * i);
                        //roll为侧倾角，pitch为俯仰角，yaw为偏航角
                        roll = MemoryUtil.PtrToStructure<float>(face3DAngleInfo.roll + MemoryUtil.SizeOf<float>() * i);
                        pitch = MemoryUtil.PtrToStructure<float>(face3DAngleInfo.pitch + MemoryUtil.SizeOf<float>() * i);
                        yaw = MemoryUtil.PtrToStructure<float>(face3DAngleInfo.yaw + MemoryUtil.SizeOf<float>() * i);
                    }

                    int rectWidth = rect.right - rect.left;
                    int rectHeight = rect.bottom - rect.top;

                    //查找最大人脸
                    if (rectWidth * rectHeight > rectTemp)
                    {
                        rectTemp = rectWidth * rectHeight;
                        temp = rect;
                        ageTemp = age;
                        genderTemp = gender;
                    }
                    else {
                        Graphics g = Graphics.FromImage(srcImage);
                        Brush brush = new SolidBrush(Color.Red);
                        Pen pen = new Pen(brush, 4);
                        pen.DashStyle = System.Drawing.Drawing2D.DashStyle.Dash;
                        g.DrawRectangle(pen, new Rectangle(rect.left, rect.top, rect.right - rect.left, rect.bottom - rect.top));
                        g.Dispose();
                    }
                    AppendText(string.Format("{0} - 人脸坐标:[left:{1},top:{2},right:{3},bottom:{4},orient:{5},roll:{6},pitch:{7},yaw:{8},status:{11}] Age:{9} Gender:{10}\n", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"), rect.left, rect.top, rect.right, rect.bottom, orient, roll, pitch, yaw, age, (gender >= 0 ? gender.ToString() : ""), face3DStatus));
                }

                AppendText(string.Format("{0} - 人脸数量:{1}\n\n", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"), multiFaceInfo.faceNum));
                
                DateTime detectEndTime = DateTime.Now;
                AppendText(string.Format("------------------------------检测结束，时间:{0}------------------------------\n", detectEndTime.ToString("yyyy-MM-dd HH:mm:ss:ms")));
                AppendText("\n");
                ASF_SingleFaceInfo singleFaceInfo = new ASF_SingleFaceInfo();
                //提取人脸特征
                image1Feature = FaceUtil.ExtractFeature(pImageEngine, srcImage, out singleFaceInfo);

                //清空上次的匹配结果
                for (int i = 0; i < imagesFeatureList.Count; i++)
                {
                    imageList.Items[i].Text = string.Format("{0}号", i);
                }
                //获取缩放比例
                float scaleRate = ImageUtil.getWidthAndHeight(srcImage.Width, srcImage.Height, picImageCompare.Width, picImageCompare.Height);
                //缩放图片
                srcImage = ImageUtil.ScaleImage(srcImage, picImageCompare.Width, picImageCompare.Height);
                //添加标记
                srcImage = ImageUtil.MarkRectAndString(srcImage, (int)(temp.left * scaleRate), (int)(temp.top * scaleRate), (int)(temp.right * scaleRate) - (int)(temp.left * scaleRate), (int)(temp.bottom * scaleRate) - (int)(temp.top * scaleRate), ageTemp, genderTemp, picImageCompare.Width);

                //显示标记后的图像
                picImageCompare.Image = srcImage;
            }
        }
        #endregion

        #region 开始匹配按钮事件
        /// <summary>
        /// 匹配事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void matchBtn_Click(object sender, EventArgs e)
        {
            if (imagesFeatureList.Count == 0)
            {
                MessageBox.Show("请注册人脸!", "提示", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if (image1Feature == IntPtr.Zero)
            {
                if (picImageCompare.Image == null)
                {
                    MessageBox.Show("请选择识别图!", "提示", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {
                    MessageBox.Show("比对失败，识别图未提取到特征值!", "提示", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                return;
            }
            //标记已经做了匹配比对，在开启视频的时候要清除比对结果
            isCompare = true;
            float compareSimilarity = 0f;
            int compareNum = 0;
            AppendText(string.Format("------------------------------开始比对，时间:{0}------------------------------\r\n", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss:ms")));
            for (int i = 0; i < imagesFeatureList.Count; i++)
            {
                IntPtr feature = imagesFeatureList[i];
                float similarity = 0f;
                int ret = ASFFunctions.ASFFaceFeatureCompare(pImageEngine, image1Feature, feature, ref similarity);
                //增加异常值处理
                if(similarity.ToString().IndexOf("E") > -1)
                {
                    similarity = 0f;
                }
                AppendText(string.Format("与{0}号比对结果:{1}\r\n", i, similarity));
                imageList.Items[i].Text = string.Format("{0}号({1})", i, similarity);
                if (similarity > compareSimilarity)
                {
                    compareSimilarity = similarity;
                    compareNum = i;
                }
            }
            if (compareSimilarity > 0)
            {
                lblCompareInfo.Text = " " + compareNum + "号," + compareSimilarity;
            }
            AppendText(string.Format("------------------------------比对结束，时间:{0}------------------------------\r\n", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss:ms")));
        }
        #endregion

        #region 视频检测相关(<摄像头按钮点击事件、摄像头Paint事件、特征比对、摄像头播放完成事件>)

        public static Boolean nvr = false;
        /// <summary>
        /// 摄像头按钮点击事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnStartVideo_Click(object sender, EventArgs e)
        {
            imageList.Visible = false;
            //在点击开始的时候再坐下初始化检测，防止程序启动时有摄像头，在点击摄像头按钮之前将摄像头拔掉的情况
            initVideo();
            
            //必须保证有可用摄像头
            if (filterInfoCollection.Count == 0)
            {
                //MessageBox.Show("未检测到摄像头，请确保已安装摄像头或驱动!");
                picImageCompare.Visible = true;
                picImageCompare.Dock = System.Windows.Forms.DockStyle.Fill;
                Task.Run(() =>
                 {
                     Boolean run = true;
                     nvr = true;
                     while (true)
                     {
                         Thread.Sleep(10);
                         if (run) {
                            run = false;
                            videoSource_Paint(null,null);
                            run = true;
                         }
                     }
                 });
                chooseImgBtn.Enabled = true;
                matchBtn.Enabled = true;
                txtThreshold.Enabled = false;
                return;
            }
            if (rgbVideoSource.IsRunning || irVideoSource.IsRunning)
            {
                nvr = false;
                btnStartVideo.Text = "启用摄像头";
                //关闭摄像头
                if (irVideoSource.IsRunning)
                {
                    irVideoSource.SignalToStop();
                    irVideoSource.Hide();
                }
                if (rgbVideoSource.IsRunning)
                {
                    rgbVideoSource.SignalToStop();
                    rgbVideoSource.Hide();
                }
                //“选择识别图”、“开始匹配”按钮可用，阈值控件禁用
                chooseImgBtn.Enabled = true;
                matchBtn.Enabled = true;
                txtThreshold.Enabled = false;
            }
            else
            {
                if (isCompare)
                {
                    //比对结果清除
                    for (int i = 0; i < imagesFeatureList.Count; i++)
                    {
                        imageList.Items[i].Text = string.Format("{0}号", i);
                    }
                    lblCompareInfo.Text = "";
                    isCompare = false;
                }
                //“选择识别图”、“开始匹配”按钮禁用，阈值控件可用，显示摄像头控件
                txtThreshold.Enabled = true;
                rgbVideoSource.Show();
                irVideoSource.Show();
                chooseImgBtn.Enabled = false;
                matchBtn.Enabled = false;
                btnStartVideo.Text = "关闭摄像头";
                //获取filterInfoCollection的总数
                int maxCameraCount = filterInfoCollection.Count;
                //如果配置了两个不同的摄像头索引
                if(rgbCameraIndex != irCameraIndex && maxCameraCount>=2)
                {
                    //RGB摄像头加载
                    rgbDeviceVideo = new VideoCaptureDevice(filterInfoCollection[rgbCameraIndex < maxCameraCount ? rgbCameraIndex : 0].MonikerString);
                    rgbDeviceVideo.VideoResolution = rgbDeviceVideo.VideoCapabilities[0];
                    rgbVideoSource.VideoSource = rgbDeviceVideo;
                    rgbVideoSource.Start();

                    //IR摄像头
                    irDeviceVideo = new VideoCaptureDevice(filterInfoCollection[irCameraIndex< maxCameraCount? irCameraIndex:0].MonikerString);
                    irDeviceVideo.VideoResolution = irDeviceVideo.VideoCapabilities[0];
                    irVideoSource.VideoSource = irDeviceVideo;
                    irVideoSource.Start();
                    //双摄标志设为true
                    isDoubleShot = true;
                }
                else
                {
                    //仅打开RGB摄像头，IR摄像头控件隐藏
                    rgbDeviceVideo = new VideoCaptureDevice(filterInfoCollection[rgbCameraIndex <= maxCameraCount ? rgbCameraIndex : 0].MonikerString);
                    rgbDeviceVideo.VideoResolution = rgbDeviceVideo.VideoCapabilities[0];
                    rgbVideoSource.VideoSource = rgbDeviceVideo;
                    rgbVideoSource.Start();
                    irVideoSource.Hide();
                }
            }
        }
        
        private FaceTrackUnit trackRGBUnit = new FaceTrackUnit();
        private FaceTrackUnit trackIRUnit = new FaceTrackUnit();
        private Font font = new Font(FontFamily.GenericSerif, 10f, FontStyle.Bold);
        private SolidBrush yellowBrush = new SolidBrush(Color.Yellow);
        private SolidBrush blueBrush = new SolidBrush(Color.Blue);
        private bool isRGBLock = false;
        private bool isIRLock = false;
        private MRECT allRect = new MRECT();
        private object rectLock = new object();


         Int32 m_lUserID = -1;
         int lChannel = Int16.Parse("1"); //通道号 Channel number
         CHCNetSDK.NET_DVR_JPEGPARA lpJpegPara = new CHCNetSDK.NET_DVR_JPEGPARA();
         int psersonPort = 0;
         String psersonServer = "false";
        /// <summary>
        /// RGB摄像头Paint事件，图像显示到窗体上，得到每一帧图像，并进行处理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void videoSource_Paint(object sender, PaintEventArgs e)
        {
            try
            {
                //Console.Write("videoSource_Paint-start" + DateTime.Now.ToString("hh:mm:sss"));
                //if (nvr || rgbVideoSource.IsRunning || !rgbVideoSource.IsRunning)
                if (nvr ||filterInfoCollection.Count>0)
                {
                    //Thread.Sleep(50);
                    //得到当前RGB摄像头下的图片
                    Bitmap bitmap;
                    //|| rgbVideoSource.IsRunning
                    string key = ".\\image\\test" + DateTime.Now.ToString("fff") + ".jpg";
                    if (filterInfoCollection.Count > 0)
                    {
                        bitmap = rgbVideoSource.GetCurrentVideoFrame();
                    }
                    else {
                       
                        if (m_lUserID < 0) {
                            //IniHelper iniHelper = new IniHelper();
                            string DVRIPAddress = iniHelper.Get("DVRIPAddress"); //设备IP地址或者域名
                            Int16 DVRPortNumber = Int16.Parse(iniHelper.Get("DVRPortNumber"));//设备服务端口号
                            string DVRUserName = iniHelper.Get("DVRUserName");//设备登录用户名
                            string DVRPassword = iniHelper.Get("DVRPassword");//设备登录密码
                            lpJpegPara.wPicQuality = ushort.Parse("2"); //图像质量 Image quality 0-最好 1-较好 2-一般
                            lpJpegPara.wPicSize = ushort.Parse("0"); //抓图分辨率 Picture size: 2- 4CIF，0xff- Auto(使用当前码流分辨率)，抓图分辨率需要设备支持，更多取值请参考SDK文档
                            bool m_bInitSDK = CHCNetSDK.NET_DVR_Init();
                            if (m_bInitSDK == false)
                            {
                                MessageBox.Show("NET_DVR_Init error!");
                                return;
                            }
                            CHCNetSDK.NET_DVR_DEVICEINFO_V30 DeviceInfo = new CHCNetSDK.NET_DVR_DEVICEINFO_V30();
                            //登录设备 Login the device
                            m_lUserID = CHCNetSDK.NET_DVR_Login_V30(DVRIPAddress, DVRPortNumber, DVRUserName, DVRPassword, ref DeviceInfo);
                        }
                     
                        bool result = CHCNetSDK.NET_DVR_CaptureJPEGPicture(m_lUserID, lChannel, ref lpJpegPara, key);
                        bitmap = new Bitmap(key);
                        /*picImageCompare.Invoke(new Action<Bitmap>(p =>
                        {
                            picImageCompare.Image = bitmap;
                        }), picImageCompare.Image);*/
                    }

                    if (bitmap == null)
                    {
                        return;
                    }
                    if (psersonPort == 0)
                    {
                        psersonPort = int.Parse(iniHelper.Get("PsersonPort")); //设备IP地址或者域名
                        psersonServer = iniHelper.Get("PsersonServer");
                    }
                    if (psersonServer == "true")
                    {
                        //string resPesron = null;
                        string resPesron = HttpMethods.HttpGet("http://127.0.0.1:" + psersonPort + "/?" + Application.StartupPath + "\\" + key.Replace("/", "\\"));
                        Graphics gPersion = Graphics.FromImage(bitmap);
                        if (resPesron != null && resPesron != "")
                        {
                            if (resPesron.Length >= 1)
                            {
                                resPesron = resPesron.Substring(0, resPesron.Length - 1);
                            }
                            string[] res = resPesron.Split(';');
                            List<Rectangle> rectsNowPerson = new List<Rectangle>();
                            foreach (string item in res)
                            {
                                var itemsplit = item.Split('_');
                                Rectangle itemr = new Rectangle(int.Parse(itemsplit[0]), int.Parse(itemsplit[1]), int.Parse(itemsplit[2]), int.Parse(itemsplit[3]));
                                rectsNowPerson.Add(itemr);
                                gPersion.DrawRectangle(Pens.Gold, itemr);
                            }
                            ClientFlow.setRect(bitmap.Width, bitmap.Height, rectsNowPerson);
                        }
                        if (nvr && this.panel2.Visible)
                        {
                            gPersion.DrawRectangle(Pens.Green, ClientFlow.getRect().X, ClientFlow.getRect().Y, ClientFlow.getRect().Width, ClientFlow.getRect().Height);
                        }
                        key = key.Replace("test", "ai");
                        bitmap.Save(key);
                        picImageCompare.Invoke(new Action<String>(p =>
                        {
                            picImageCompare.ImageLocation = key;
                        }), picImageCompare.ImageLocation);
                        return;
                    }
                    

                    //检测人脸，得到Rect框
                    ASF_MultiFaceInfo multiFaceInfo = FaceUtil.DetectFace(pVideoEngine, bitmap);
                    //得到最大人脸
                    ASF_SingleFaceInfo maxFace = FaceUtil.GetMaxFace(multiFaceInfo);
                    //得到Rect
                    MRECT rect = maxFace.faceRect;
                    int maxRectIndex = 0;
                    //检测RGB摄像头下最大人脸
                    Graphics g;
                    if (e != null)
                    {
                        g = e.Graphics;
                    }
                    else {
                        g = Graphics.FromImage(bitmap);
                    }
                    float offsetX = rgbVideoSource.Width * 1f / bitmap.Width;
                    float offsetY = rgbVideoSource.Height * 1f / bitmap.Height;
                    if (nvr) {
                        offsetX = 1f;
                        offsetY = 1f;
                    }
                    float x = rect.left * offsetX;
                    float width = rect.right * offsetX - x;
                    float y = rect.top * offsetY;
                    float height = rect.bottom * offsetY - y;
                    //根据Rect进行画框
                    g.DrawRectangle(Pens.Gold, x, y, width, height);
                    g.DrawRectangle(Pens.Green, ClientFlow.getRect().X, ClientFlow.getRect().Y, ClientFlow.getRect().Width, ClientFlow.getRect().Height);
                    List<Rectangle> rectsNow = new List<Rectangle>();
                    for (int i = 0; i < multiFaceInfo.faceNum; i++)
                    {
                        try
                        {
                            
                            MRECT rectx = MemoryUtil.PtrToStructure<MRECT>(multiFaceInfo.faceRects + MemoryUtil.SizeOf<MRECT>() * i);
                            if (rectx.left == rect.left && rectx.right == rect.right) {
                                maxRectIndex = i;
                                //continue; 
                            }
                            g.DrawRectangle(Pens.Gold, new Rectangle(rect.left, rect.top, rect.right - rect.left, rect.bottom - rect.top));
                            rectsNow.Add( new Rectangle(rect.left, rect.top, rect.right - rect.left, rect.bottom - rect.top));
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine(ex.Message);
                        }
                    }
                    ClientFlow.setRect(bitmap.Width, bitmap.Height, rectsNow);
                    if (this.panel2.Visible)
                    {
                        g.DrawRectangle(Pens.Green,  ClientFlow.getRect().X,  ClientFlow.getRect().Y,  ClientFlow.getRect().Width,  ClientFlow.getRect().Height);
                    }
                    if (trackRGBUnit.message != "" && x > 0 && y > 0)
                    {
                        //将上一帧检测结果显示到页面上
                        g.DrawString(trackRGBUnit.message, font, trackRGBUnit.message.Contains("活体") ? blueBrush : yellowBrush, x, y - 15);
                    }

                    //保证只检测一帧，防止页面卡顿以及出现其他内存被占用情况
                    if (isRGBLock == false)
                    {
                        isRGBLock = true;
                        //异步处理提取特征值和比对，不然页面会比较卡
                        ThreadPool.QueueUserWorkItem(new WaitCallback(delegate
                        {
                            if (rect.left != 0 && rect.right != 0 && rect.top != 0 && rect.bottom != 0)
                            {
                                try
                                {
                                    lock (rectLock)
                                    {
                                        allRect.left = (int)(rect.left * offsetX);
                                        allRect.top = (int)(rect.top * offsetY);
                                        allRect.right = (int)(rect.right * offsetX);
                                        allRect.bottom = (int)(rect.bottom * offsetY);
                                    }

                                    bool isLiveness = false;

                                    //调整图片数据，非常重要
                                    ImageInfo imageInfo = ImageUtil.ReadBMP(bitmap);
                                    if (imageInfo == null)
                                    {
                                        return;
                                    }
                                    int retCode_Liveness = -1;
                                    //RGB活体检测
                                    ASF_LivenessInfo liveInfo = FaceUtil.LivenessInfo_RGB(pVideoRGBImageEngine, imageInfo, multiFaceInfo, out retCode_Liveness);
                                    //判断检测结果
                                    if (retCode_Liveness == 0 && liveInfo.num > 0)
                                    {
                                        int isLive = MemoryUtil.PtrToStructure<int>(liveInfo.isLive);
                                        isLiveness = (isLive == 1) ? true : false;
                                    }
                                    if (!isLiveness && multiFaceInfo.faceNum > 0) { 
                                        isLiveness = true; 
                                    }
                                    if (isLiveness)
                                    {
                                        //提取人脸特征
                                        IntPtr feature = FaceUtil.ExtractFeature(pVideoRGBImageEngine, bitmap, maxFace);
                                        float similarity = 0f;
                                        //得到比对结果
                                        int result = compareFeature(feature, out similarity);
                                        MemoryUtil.Free(feature);
                                        if (result > -1)
                                        {
                                            //将比对结果放到显示消息中，用于最新显示
                                            //年龄检测
                                            int retCode_Age = -1;
                                            ASF_AgeInfo ageInfo = FaceUtil.AgeEstimation(pImageEngine, imageInfo, multiFaceInfo, out retCode_Age);
                                            //性别检测
                                            int retCode_Gender = -1;
                                            ASF_GenderInfo genderInfo = FaceUtil.GenderEstimation(pImageEngine, imageInfo, multiFaceInfo, out retCode_Gender);
                                            if (genderInfo.num > 0)
                                            {
                                                var age = MemoryUtil.PtrToStructure<int>(ageInfo.ageArray + MemoryUtil.SizeOf<int>() * maxRectIndex);
                                                var gender = MemoryUtil.PtrToStructure<int>(genderInfo.genderArray + MemoryUtil.SizeOf<int>() * maxRectIndex);
                                                String genderStr = "";
                                                if (gender == 0) { genderStr = "男"; } else { genderStr = "女"; }
                                                MainInfo mainInfo = null;
                                                mainInfos = this.personInfoService.QueryForList();
                                                foreach (MainInfo item in mainInfos)
                                                {
                                                    if (item.ShowImageName.Contains(imagePathListTempName[result]))
                                                    {
                                                        mainInfo = item;
                                                    }
                                                }
                                                string[] where = new string[] { "date=date('now') and person_id= '" + mainInfo.ID + "'" };
                                                List<AttendanceRecord> recordQueryForList = attendanceRecordService.QueryForList(where, "", "", "", 0, 1);
                                                if (recordQueryForList.Count >= 1)
                                                {

                                                    recordQueryForList[0].EndTime = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                                                    if (recordQueryForList[0].EndTime != DateTime.Now.ToString("yyyy-MM-dd HH:mm"))
                                                    {
                                                        String path = ".//OpenSoftFaceFile/check/" + DateTime.Now.ToString("yyyy-MM-dd");
                                                        if (!Directory.Exists(path))
                                                        {
                                                            Directory.CreateDirectory(path);
                                                        }
                                                        String bitmapName = path + "/" + mainInfo.PersonSerial + "_" + DateTime.Now.ToString("yyyy-MM-dd_HH_mm_ss") + ".jpg";
                                                        bitmap.Save(bitmapName);

                                                        recordQueryForList[0].EndAttendUrl = bitmapName;
                                                    }
                                                    attendanceRecordService.Update(recordQueryForList[0]);
                                                }
                                                else
                                                {
                                                    String path = ".//OpenSoftFaceFile/check/" + DateTime.Now.ToString("yyyy-MM-dd");
                                                    if (!Directory.Exists(path))
                                                    {
                                                        Directory.CreateDirectory(path);
                                                    }
                                                    String bitmapName = path + "/" + mainInfo.PersonSerial + "_" + DateTime.Now.ToString("yyyy-MM-dd_HH_mm_ss") + ".jpg";
                                                    bitmap.Save(bitmapName);
                                                    AttendanceRecord record = new AttendanceRecord();
                                                    record.PersonId = mainInfo.ID;
                                                    record.PersonSerial = mainInfo.PersonSerial;
                                                    record.Date = DateTime.Now.ToString("yyyy-MM-dd");
                                                    record.StartTime = DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss");
                                                    record.EndTime = DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss");
                                                    record.StartAttendUrl = bitmapName;
                                                    record.EndAttendUrl = "";
                                                    record.AttendStatus = 1;
                                                    record.AddTime = DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss");
                                                    attendanceRecordService.Insert(record);
                                                }
                                                trackRGBUnit.message = string.Format(" {0}号-{1} {2}", mainInfo.PersonSerial, mainInfo.Dept + "-" + mainInfo.PersonName, "年龄:" + age + "性别:" + genderStr);

                                                staff_image = imagePathListTemp[result];
                                                staff_name = mainInfo.PersonName + "(" + genderStr + "-" + age + "岁)";
                                                staff_date = DateTime.Now.ToString("HH:mm:ss");
                                                staff_dept = mainInfo.Dept;
                                                staff_visible = true;
                                            }
                                            else
                                            {
                                                trackRGBUnit.message = string.Format(" {0}号-{1}", result, imagePathListTempName[result]);
                                                staff_visible = false;
                                            }
                                            if (_successVoice != null)
                                                _successVoice.Play();
                                            //trackRGBUnit.message = string.Format(" {0}号-{3} {1},{2}", result, similarity, string.Format("RGB{0}", isLiveness ? "活体" : "假体"), imagePathListTempName[result]);
                                        }
                                        else
                                        {
                                            //年龄检测
                                            int retCode_Age = -1;
                                            ASF_AgeInfo ageInfo = FaceUtil.AgeEstimation(pImageEngine, imageInfo, multiFaceInfo, out retCode_Age);
                                            //性别检测
                                            int retCode_Gender = -1;
                                            ASF_GenderInfo genderInfo = FaceUtil.GenderEstimation(pImageEngine, imageInfo, multiFaceInfo, out retCode_Gender);
                                            if (genderInfo.num > 0)
                                            {
                                                var age = MemoryUtil.PtrToStructure<int>(ageInfo.ageArray + MemoryUtil.SizeOf<int>() * 0);
                                                var gender = MemoryUtil.PtrToStructure<int>(genderInfo.genderArray + MemoryUtil.SizeOf<int>() * 0);
                                                String genderStr = "";
                                                if (gender == 0) { genderStr = "男"; } else { genderStr = "女"; }
                                                //显示消息
                                                trackRGBUnit.message = string.Format("{0} {1}", isLiveness ? "活体" : "假体", " 年龄:" + age + " 性别:" + genderStr);
                                            }
                                            else
                                            {
                                                //显示消息
                                                trackRGBUnit.message = string.Format("{0}", isLiveness ? "活体" : "假体");
                                            }
                                            staff_visible = false;
                                            //if (_errorVoice != null)
                                            //_errorVoice.Play();
                                        }

                                        if (imageInfo != null)
                                        {
                                            MemoryUtil.Free(imageInfo.imgData);
                                        }
                                    }
                                    else
                                    {


                                        //年龄检测
                                        int retCode_Age = -1;
                                        ASF_AgeInfo ageInfo = FaceUtil.AgeEstimation(pImageEngine, imageInfo, multiFaceInfo, out retCode_Age);
                                        //性别检测
                                        int retCode_Gender = -1;
                                        ASF_GenderInfo genderInfo = FaceUtil.GenderEstimation(pImageEngine, imageInfo, multiFaceInfo, out retCode_Gender);
                                        if (genderInfo.num > 0)
                                        {
                                            var age = MemoryUtil.PtrToStructure<int>(ageInfo.ageArray + MemoryUtil.SizeOf<int>() * 0);
                                            var gender = MemoryUtil.PtrToStructure<int>(genderInfo.genderArray + MemoryUtil.SizeOf<int>() * 0);
                                            String genderStr = "";
                                            if (gender == 0) { genderStr = "男"; } else { genderStr = "女"; }
                                            trackRGBUnit.message = string.Format("{0} {1}", isLiveness ? "活体" : "假体", " 年龄:" + age + " 性别:" + genderStr);
                                        }
                                        else
                                        {
                                            //显示消息
                                            trackRGBUnit.message = string.Format("{0}", isLiveness ? "活体" : "假体");
                                        }
                                        staff_visible = false;
                                    }
                                    key = key.Replace("test", "ai");
                                    bitmap.Save(key);
                                    if (nvr)
                                    {
                                        picImageCompare.Invoke(new Action<String>(p =>
                                        {
                                            picImageCompare.ImageLocation = key;
                                        }), picImageCompare.ImageLocation);
                                    }
                                }
                                catch (Exception ex)
                                {
                                    Console.WriteLine(ex.Message + ex.StackTrace);
                                }
                                finally
                                {
                                    if (bitmap != null)
                                    {
                                        bitmap.Dispose();
                                    }
                                    isRGBLock = false;
                                }
                            }
                            else
                            {
                                lock (rectLock)
                                {
                                    allRect.left = 0;
                                    allRect.top = 0;
                                    allRect.right = 0;
                                    allRect.bottom = 0;
                                }
                                if (nvr)
                                {
                                    picImageCompare.Invoke(new Action<String>(p =>
                                    {
                                        picImageCompare.ImageLocation = key;
                                    }), picImageCompare.ImageLocation);
                                }
                            }
                            isRGBLock = false;
                        }));
                    }
                }
            }
            catch (Exception err) {
                Console.WriteLine(err.Message + err.StackTrace);
            }
            //Console.Write("videoSource_Paint-end" + DateTime.Now.ToString("hh:mm:sss"));
        }

        /// <summary>
        /// RGB摄像头Paint事件,同步RGB人脸框，对比人脸框后进行IR活体检测
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void irVideoSource_Paint(object sender, PaintEventArgs e)
        {
            if (isDoubleShot && irVideoSource.IsRunning)
            {
                //如果双摄，且IR摄像头工作，获取IR摄像头图片
                Bitmap irBitmap = irVideoSource.GetCurrentVideoFrame();
                if (irBitmap == null)
                {
                    return;
                }
                //得到Rect
                MRECT rect = new MRECT();
                lock (rectLock)
                {
                    rect = allRect;
                }
                float irOffsetX = irVideoSource.Width * 1f / irBitmap.Width;
                float irOffsetY = irVideoSource.Height * 1f / irBitmap.Height;
                float offsetX = irVideoSource.Width * 1f / rgbVideoSource.Width;
                float offsetY = irVideoSource.Height * 1f / rgbVideoSource.Height;
                //检测IR摄像头下最大人脸
                Graphics g = e.Graphics;

                float x = rect.left * offsetX;
                float width = rect.right * offsetX - x;
                float y = rect.top * offsetY;
                float height = rect.bottom * offsetY - y;
                //根据Rect进行画框
                g.DrawRectangle(Pens.Red, x, y, width, height);
                if (trackIRUnit.message != "" && x > 0 && y > 0)
                {
                    //将上一帧检测结果显示到页面上
                    g.DrawString(trackIRUnit.message, font, trackIRUnit.message.Contains("活体") ? blueBrush : yellowBrush, x, y - 15);
                }

                //保证只检测一帧，防止页面卡顿以及出现其他内存被占用情况
                if (isIRLock == false)
                {
                    isIRLock = true;
                    //异步处理提取特征值和比对，不然页面会比较卡
                    ThreadPool.QueueUserWorkItem(new WaitCallback(delegate
                    {
                        if (rect.left != 0 && rect.right != 0 && rect.top != 0 && rect.bottom != 0)
                        {
                            bool isLiveness = false;
                            try
                            {
                                //得到当前摄像头下的图片
                                if (irBitmap != null)
                                {
                                    //检测人脸，得到Rect框
                                    ASF_MultiFaceInfo irMultiFaceInfo = FaceUtil.DetectFace(pVideoIRImageEngine, irBitmap);
                                    if (irMultiFaceInfo.faceNum <= 0)
                                    {
                                        return;
                                    }
                                    //得到最大人脸
                                    ASF_SingleFaceInfo irMaxFace = FaceUtil.GetMaxFace(irMultiFaceInfo);
                                    //得到Rect
                                    MRECT irRect = irMaxFace.faceRect;
                                    //判断RGB图片检测的人脸框与IR摄像头检测的人脸框偏移量是否在误差允许范围内
                                    if (isInAllowErrorRange(rect.left * offsetX / irOffsetX, irRect.left) && isInAllowErrorRange(rect.right * offsetX / irOffsetX, irRect.right)
                                        && isInAllowErrorRange(rect.top * offsetY / irOffsetY, irRect.top) && isInAllowErrorRange(rect.bottom * offsetY / irOffsetY, irRect.bottom))
                                    {
                                        int retCode_Liveness = -1;
                                        //将图片进行灰度转换，然后获取图片数据
                                        ImageInfo irImageInfo = ImageUtil.ReadBMP_IR(irBitmap);
                                        if (irImageInfo == null)
                                        {
                                            return;
                                        }
                                        //IR活体检测
                                        ASF_LivenessInfo liveInfo = FaceUtil.LivenessInfo_IR(pVideoIRImageEngine, irImageInfo, irMultiFaceInfo, out retCode_Liveness);
                                        //判断检测结果
                                        if (retCode_Liveness == 0 && liveInfo.num > 0)
                                        {
                                            int isLive = MemoryUtil.PtrToStructure<int>(liveInfo.isLive);
                                            isLiveness = (isLive == 1) ? true : false;
                                        }
                                        if (irImageInfo != null)
                                        {
                                            MemoryUtil.Free(irImageInfo.imgData);
                                        }
                                    }
                                }
                            }
                            catch (Exception ex)
                            {
                                Console.WriteLine(ex.Message);
                            }
                            finally
                            {
                                trackIRUnit.message = string.Format("IR{0}", isLiveness ? "活体" : "假体");
                                if (irBitmap != null)
                                {
                                    irBitmap.Dispose();
                                }
                                isIRLock = false;
                            }
                        }
                        else
                        {
                            trackIRUnit.message = string.Empty;
                        }
                        isIRLock = false;
                    }));
                }
            }
        }


        /// <summary>
        /// 得到feature比较结果
        /// </summary>
        /// <param name="feature"></param>
        /// <returns></returns>
        private int compareFeature(IntPtr feature, out float similarity)
        {
            int result = -1;
            similarity = 0f;
            float maxsimilarity = 0f;
            //如果人脸库不为空，则进行人脸匹配
            if (imagesFeatureList != null && imagesFeatureList.Count > 0)
            {
                for (int i = 0; i < imagesFeatureList.Count; i++)
                {
                    //调用人脸匹配方法，进行匹配
                    ASFFunctions.ASFFaceFeatureCompare(pVideoRGBImageEngine, feature, imagesFeatureList[i], ref similarity);
                    if (similarity >= threshold)
                    {
                        if (similarity > maxsimilarity) {
                            result = i;
                        }
                        //result = i;
                        //break;
                    }
                }
            }
            return result;
        }        

        /// <summary>
        /// 摄像头播放完成事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="reason"></param>
        private void videoSource_PlayingFinished(object sender, AForge.Video.ReasonToFinishPlaying reason)
        {
            try
            {
                Control.CheckForIllegalCrossThreadCalls = false;
                chooseImgBtn.Enabled = true;
                matchBtn.Enabled = true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        #endregion

        #region 阈值相关
        /// <summary>
        /// 阈值文本框键按下事件，检测输入内容是否正确，不正确不能输入
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtThreshold_KeyPress(object sender, KeyPressEventArgs e)
        {
            //阻止从键盘输入键
            e.Handled = true;
            //是数值键，回退键，.能输入，其他不能输入
            if (char.IsDigit(e.KeyChar) || e.KeyChar == 8 || e.KeyChar == '.')
            {
                //渠道当前文本框的内容
                string thresholdStr = txtThreshold.Text.Trim();
                int countStr = 0;
                int startIndex = 0;
                //如果当前输入的内容是否是“.”
                if (e.KeyChar == '.')
                {
                    countStr = 1;
                }
                //检测当前内容是否含有.的个数
                if (thresholdStr.IndexOf('.', startIndex) > -1)
                {
                    countStr += 1;
                }
                //如果输入的内容已经超过12个字符，
                if (e.KeyChar != 8 && (thresholdStr.Length > 12 || countStr > 1))
                {
                    return;
                }
                e.Handled = false;
            }
        }

        /// <summary>
        /// 阈值文本框键抬起事件，检测阈值是否正确，不正确改为0.8f
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtThreshold_KeyUp(object sender, KeyEventArgs e)
        {
            //如果输入的内容不正确改为默认值
            if (!float.TryParse(txtThreshold.Text.Trim(), out threshold))
            {
                threshold = 0.8f;
            }
        }
        #endregion

        #region 窗体关闭
        /// <summary>
        /// 窗体关闭事件
        /// </summary>
        private void Form_Closed(object sender, FormClosedEventArgs e)
        {
            try
            {
                if (rgbVideoSource.IsRunning)
                {
                    btnStartVideo_Click(sender, e); //关闭摄像头
                }

                //销毁引擎
                int retCode = ASFFunctions.ASFUninitEngine(pImageEngine);
                Console.WriteLine("UninitEngine pImageEngine Result:" + retCode);
                //销毁引擎
                retCode = ASFFunctions.ASFUninitEngine(pVideoEngine);
                Console.WriteLine("UninitEngine pVideoEngine Result:" + retCode);

                //销毁引擎
                retCode = ASFFunctions.ASFUninitEngine(pVideoRGBImageEngine);
                Console.WriteLine("UninitEngine pVideoImageEngine Result:" + retCode);

                //销毁引擎
                retCode = ASFFunctions.ASFUninitEngine(pVideoIRImageEngine);
                Console.WriteLine("UninitEngine pVideoIRImageEngine Result:" + retCode);
            }
            catch (Exception ex)
            {
                Console.WriteLine("UninitEngine pImageEngine Error:" + ex.Message);
            }
        }
        #endregion

        #region 公用方法
        /// <summary>
        /// 恢复使用/禁用控件列表控件
        /// </summary>
        /// <param name="isEnable"></param>
        /// <param name="controls">控件列表</param>
        private void ControlsEnable(bool isEnable,params Control[] controls)
        {
            if(controls == null || controls.Length <= 0)
            {
                return;
            }
            foreach(Control control in controls)
            {
                control.Enabled = isEnable;
            }
        }

        /// <summary>
        /// 校验图片
        /// </summary>
        /// <param name="imagePath"></param>
        /// <returns></returns>
        private bool checkImage(string imagePath)
        {
            if (imagePath == null)
            {
                AppendText("图片不存在，请确认后再导入\r\n");
                return false;
            }
            try
            {
                //判断图片是否正常，如将其他文件把后缀改为.jpg，这样就会报错
                Image image = ImageUtil.readFromFile(imagePath);
                if(image == null)
                {
                    throw new Exception();
                }else
                {
                    image.Dispose();
                }
            }
            catch
            {
                AppendText(string.Format("{0} 图片格式有问题，请确认后再导入\r\n", imagePath));
                return false;
            }
            FileInfo fileCheck = new FileInfo(imagePath);
            if (fileCheck.Exists == false)
            {
                AppendText(string.Format("{0} 不存在\r\n", fileCheck.Name));
                return false;
            }
            else if (fileCheck.Length > maxSize)
            {
                AppendText(string.Format("{0} 图片大小超过2M，请压缩后再导入\r\n", fileCheck.Name));
                return false;
            }
            else if (fileCheck.Length < 2)
            {
                AppendText(string.Format("{0} 图像质量太小，请重新选择\r\n", fileCheck.Name));
                return false;
            }
            return true;
        }

        /// <summary>
        /// 追加公用方法
        /// </summary>
        /// <param name="message"></param>
        private void AppendText(string message)
        {
            logBox.AppendText(message);
        }

        /// <summary>
        /// 判断参数0与参数1是否在误差允许范围内
        /// </summary>
        /// <param name="arg0">参数0</param>
        /// <param name="arg1">参数1</param>
        /// <returns></returns>
        private bool isInAllowErrorRange(float arg0, float arg1)
        {
            bool rel = false;
            if (arg0 > arg1 - allowAbleErrorRange && arg0 < arg1 + allowAbleErrorRange)
            {
                rel = true;
            }
            return rel;
        }
        #endregion

        private void lblCompareImage_Click(object sender, EventArgs e)
        {

        }

        private void pictureBox_close_Click(object sender, EventArgs e)
        {
            try
            {
                btnStartVideo_Click(sender,e);
                Thread.Sleep(2000);
                if (_successVoice != null)
                {
                    _successVoice.Dispose();
                }
                if (_errorVoice != null)
                {
                    _errorVoice.Dispose();
                }
                this.Close();
            }
            catch (Exception err) {
                this.Close();
            }
        }

        private void label5_Click(object sender, EventArgs e)
        {

        }

        private void FaceForm_Load(object sender, EventArgs e)
        {

        }

         /// <summary>
        /// 获取系统的星期
       /// </summary>
        /// <param name="dt">英文的星期</param>
        /// <returns>中文的星期几</returns>
        private static string GetWeek(string dt)
        {
            string week = "";
            //根据取得的英文单词返回汉字
            switch (dt)
            {
                case "Monday":
                    week = "星期一";
                    break;
                case "Tuesday":
                    week = "星期二";
                    break;
                case "Wednesday":
                    week = "星期三";
                    break;
                case "Thursday":
                    week = "星期四";
                    break;
                case "Friday":
                    week = "星期五";
                    break;
                case "Saturday":
                    week = "星期六";
                    break;
                case "Sunday":
                    week = "星期日";
                    break;
            }
            return week;
        }

        private string format = "yyyy年MM月dd日HH时mm分ss秒";
        private string message = "";
        private void updateTime_Tick(object sender, EventArgs e)
        {
            try
            {
                var now = DateTime.Now;
                format = "yyyy年MM月dd日";
                label3.Text = now.ToString(format, DateTimeFormatInfo.InvariantInfo) + " " + GetWeek(DateTime.Now.DayOfWeek.ToString()) ;
                if (ClientFlow.inNum > 0 && this.panel2.Visible)
                {
                    label3.Text = label3.Text + ";客流量进" + ClientFlow.inNum + "出" + ClientFlow.outNum;
                }
                //+ "(" + (_faceApi == null ? 0 : _faceApi.FaceCount()) + ")"
                label1.Text = message;
                lab_hhmm.Text = now.ToString("HH:mm:ss");
            }
            catch (Exception ex)
            {
                //Log.Error("更新提示异常", ex);
            }
        }

        private void FaceForm_DoubleClick(object sender, EventArgs e)
        {

        }

        private void FaceForm_Click(object sender, EventArgs e)
        {
            this.Invoke(new Action<Image>(p =>
            {
                Random rd = new Random();
                int sjs = rd.Next(1, 10);
                this.BackgroundImage = (Image)Image.FromFile(Path.Combine("Resources", "bg" + sjs + ".jpg")).Clone();
            }), this.BackgroundImage);
        }

        private void rgbVideoSource_Click(object sender, EventArgs e)
        {
            if (this.WindowState != FormWindowState.Maximized)
            {
                this.WindowState = FormWindowState.Maximized;
                //this.FormBorderStyle = FormBorderStyle.None;
            }
            else
            {
                //this.FormBorderStyle = FormBorderStyle.FixedSingle;
                this.WindowState = FormWindowState.Normal;
            }
        }

        private void imageList_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void imageList_DoubleClick(object sender, EventArgs e)
        {
            imageList.Visible = false;
        }

        private void imageList_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            imageList.Visible = false;
        }

        private void label2_Click(object sender, EventArgs e)
        {
            if (this.FormBorderStyle != FormBorderStyle.None)
            {
                this.FormBorderStyle = FormBorderStyle.None;
            }
            else
            {
                this.FormBorderStyle = FormBorderStyle.FixedSingle;
            }
        }

        private void pic_logo_Click(object sender, EventArgs e)
        {
            //http://www.logofree.cn/
            if (this.FormBorderStyle != FormBorderStyle.None)
            {
                this.FormBorderStyle = FormBorderStyle.None;
            }
            else
            {
                this.FormBorderStyle = FormBorderStyle.Sizable;
            }
        }

        public static OpenSoftFace.Setting.Forms.SettingForms.BaseForm baseForm = new OpenSoftFace.Setting.Forms.SettingForms.BaseForm();
        private void pic_set_Click(object sender, EventArgs e)
        {
            //OpenSoftFace.Setting.Forms.LoginForm login = new OpenSoftFace.Setting.Forms.LoginForm();
            //login.Show();
            //OpenSoftFace.Setting.Forms.SettingForms.BaseForm baseForm = new OpenSoftFace.Setting.Forms.SettingForms.BaseForm();
            try
            {
                if (baseForm != null)
                    baseForm.Show();
            }
            catch (Exception err) {
                baseForm = new OpenSoftFace.Setting.Forms.SettingForms.BaseForm();
                baseForm.Show();
            }
            /*string fileName = System.Environment.CurrentDirectory + "/setting/OpenSoftFace.Setting.exe";
            if (File.Exists(fileName))
                Process.Start(fileName);
            else
                MessageBox.Show("not exist");
            return;*/
        }

        private void pic_max_Click(object sender, EventArgs e)
        {
            if (this.WindowState != FormWindowState.Maximized)
            {
                this.WindowState = FormWindowState.Maximized;
                //this.FormBorderStyle = FormBorderStyle.None;
            }
            else
            {
                //this.FormBorderStyle = FormBorderStyle.FixedSingle;
                this.WindowState = FormWindowState.Normal;
            }
        }

        private void panel1_MouseDown(object sender, MouseEventArgs e)
        {
            Win32.ReleaseCapture();
            Win32.SendMessage(base.Handle, 274, 61458, 0);
        }

        private void label2_MouseDown(object sender, MouseEventArgs e)
        {
            Win32.ReleaseCapture();
            Win32.SendMessage(base.Handle, 274, 61458, 0);
        }

        private void panel3_MouseDown(object sender, MouseEventArgs e)
        {
            Win32.ReleaseCapture();
            Win32.SendMessage(base.Handle, 274, 61458, 0);
        }

        private void pic_logo_MouseDown(object sender, MouseEventArgs e)
        {
            Win32.ReleaseCapture();
            Win32.SendMessage(base.Handle, 274, 61458, 0);

            if (this.FormBorderStyle != FormBorderStyle.None)
            {
                this.FormBorderStyle = FormBorderStyle.None;
            }
            else
            {
                this.FormBorderStyle = FormBorderStyle.Sizable;
            }
        }

        private void label3_Click(object sender, EventArgs e)
        {
            if (!this.logBox.Visible)
            {
                this.logBox.Visible = true;
                this.btnStartVideo.Visible = true;
                this.btnClearFaceList.Visible = true;
                this.matchBtn.Visible = true;
                this.chooseImgBtn.Visible = true;
                this.irVideoSource.Visible = true;
                this.picImageCompare.Visible = true;
                this.imageList.Visible = true;
                this.btnStartVideo.Enabled = true;
            }
            else
            {
                this.logBox.Visible = false;
                this.btnStartVideo.Visible = false;
                this.btnClearFaceList.Visible = false;
                this.matchBtn.Visible = false;
                this.chooseImgBtn.Visible = false;
                this.irVideoSource.Visible = false;
                if (!nvr) { 
                    this.picImageCompare.Visible = false;
                }
                this.imageList.Visible = false;
            }
        }

        private void panel2_Paint(object sender, PaintEventArgs e)
        {
        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void lab_hhmm_Click(object sender, EventArgs e)
        {
            staff_visible = true;
        }

        private void picImageCompare_Click(object sender, EventArgs e)
        {
            if (this.WindowState != FormWindowState.Maximized)
            {
                this.WindowState = FormWindowState.Maximized;
                //this.FormBorderStyle = FormBorderStyle.None;
            }
            else
            {
                //this.FormBorderStyle = FormBorderStyle.FixedSingle;
                this.WindowState = FormWindowState.Normal;
            }
        }

        private void FaceForm_KeyDown(object sender, KeyEventArgs e)
        {
            /*try
            {
                btnStartVideo_Click(sender, e);
                Thread.Sleep(2000);
                if (_successVoice != null)
                {
                    _successVoice.Dispose();
                }
                if (_errorVoice != null)
                {
                    _errorVoice.Dispose();
                }
                this.Close();
            }
            catch (Exception err)
            {
                this.Close();
            }*/
        }
    }
}
