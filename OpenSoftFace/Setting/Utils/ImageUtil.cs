using OpenSoftFace.Setting.Entity;
using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;

namespace OpenSoftFace.Setting.Utils
{
	public class ImageUtil
	{
        public static byte[] CompressionImage(Stream fileStream, long quality, bool isRotate)
        {
            byte[] result;
            using (Image image = Image.FromStream(fileStream))
            {
                ImageCodecInfo encoder = ImageUtil.GetEncoder(ImageFormat.Jpeg);
                if (isRotate)
                {
                    image.RotateFlip(RotateFlipType.Rotate90FlipNone);
                }
                using (Bitmap bitmap = new Bitmap(image))
                {
                    Encoder arg_30_0 = Encoder.Quality;
                    EncoderParameters encoderParameters = new EncoderParameters(1);
                    EncoderParameter encoderParameter = new EncoderParameter(arg_30_0, quality);
                    encoderParameters.Param[0] = encoderParameter;
                    using (MemoryStream memoryStream = new MemoryStream())
                    {
                        try
                        {
                            bitmap.Save(memoryStream, encoder, encoderParameters);
                            encoderParameters.Dispose();
                            encoderParameter.Dispose();
                            fileStream.Dispose();
                            fileStream.Close();
                            bitmap.Dispose();
                            image.Dispose();
                        }
                        catch (Exception se)
                        {
                            LogUtil.LogHelper.LogError(typeof(ImageUtil), se);
                        }
                        result = memoryStream.ToArray();
                    }
                }
            }
            return result;
        }

        public static ImageCodecInfo GetEncoder(ImageFormat format)
        {
            ImageCodecInfo result;
            try
            {
                ImageCodecInfo[] imageDecoders = ImageCodecInfo.GetImageDecoders();
                for (int i = 0; i < imageDecoders.Length; i++)
                {
                    ImageCodecInfo imageCodecInfo = imageDecoders[i];
                    if (imageCodecInfo.FormatID == format.Guid)
                    {
                        result = imageCodecInfo;
                        return result;
                    }
                }
                result = null;
            }
            catch (Exception se)
            {
                LogUtil.LogHelper.LogError(typeof(ImageUtil), se);
                result = null;
            }
            return result;
        }

		public static Image readFromFile(string imageUrl)
		{
			Image result = null;
			FileStream fileStream = null;
			try
			{
				fileStream = new FileStream(imageUrl, FileMode.Open, FileAccess.Read);
				result = Image.FromStream(fileStream);
			}
			finally
			{
				fileStream.Close();
			}
			return result;
		}

		public static ImageInfo ReadBMP(Image image)
		{
			ImageInfo imageInfo = new ImageInfo();
			Bitmap bitmap = new Bitmap(image);
			BitmapData bitmapData = bitmap.LockBits(new Rectangle(0, 0, bitmap.Width, bitmap.Height), ImageLockMode.ReadOnly, PixelFormat.Format24bppRgb);
			ImageInfo result;
			try
			{
				IntPtr scan = bitmapData.Scan0;
				int num = bitmapData.Height * Math.Abs(bitmapData.Stride);
				byte[] array = new byte[num];
				MemoryUtil.Copy(scan, array, 0, num);
				imageInfo.width = bitmapData.Width;
				imageInfo.height = bitmapData.Height;
				imageInfo.format = 513;
				int num2 = imageInfo.width * 3;
				int num3 = Math.Abs(bitmapData.Stride);
				int num4 = num2 * imageInfo.height;
				byte[] array2 = new byte[num4];
				for (int i = 0; i < imageInfo.height; i++)
				{
					Array.Copy(array, i * num3, array2, i * num2, num2);
				}
				imageInfo.imgData = MemoryUtil.Malloc(array2.Length);
				MemoryUtil.Copy(array2, 0, imageInfo.imgData, array2.Length);
				result = imageInfo;
				return result;
			}
			catch (Exception ex)
			{
				Console.WriteLine(ex.Message);
			}
			finally
			{
				bitmap.UnlockBits(bitmapData);
			}
			result = null;
			return result;
		}

		public static Image MarkRect(Image image, int startX, int startY, int width, int height)
		{
			Image image2 = (Image)image.Clone();
			Graphics graphics = Graphics.FromImage(image2);
			Image result;
			try
			{
				Brush brush = new SolidBrush(Color.Red);
				graphics.DrawRectangle(new Pen(brush, 2f)
				{
					DashStyle = DashStyle.Dash
				}, new Rectangle(startX, startY, width, height));
				result = image2;
				return result;
			}
			catch (Exception ex)
			{
				Console.WriteLine(ex.Message);
			}
			finally
			{
				graphics.Dispose();
			}
			result = null;
			return result;
		}

		public static Image MarkRectAndString(Image image, int startX, int startY, int width, int height, int age, int gender, int showWidth)
		{
			Image image2 = (Image)image.Clone();
			Graphics graphics = Graphics.FromImage(image2);
			Image result;
			try
			{
				Brush brush = new SolidBrush(Color.Red);
				int num = image.Width / showWidth;
				graphics.DrawRectangle(new Pen(brush, (float)((num > 1) ? (2 * num) : 2))
				{
					DashStyle = DashStyle.Dash
				}, new Rectangle((startX < 1) ? 0 : startX, (startY < 1) ? 0 : startY, width, height));
				string arg = "";
				bool flag = gender >= 0;
				if (flag)
				{
					bool flag2 = gender == 0;
					if (flag2)
					{
						arg = "男";
					}
					else
					{
						bool flag3 = gender == 1;
						if (flag3)
						{
							arg = "女";
						}
					}
				}
				int num2 = image.Width / showWidth;
				bool flag4 = num2 > 1;
				if (flag4)
				{
					int num3 = 12;
					for (int i = 0; i < num2; i++)
					{
						num3 += 6;
					}
					num2 = num3;
				}
				else
				{
					bool flag5 = num2 == 1;
					if (flag5)
					{
						num2 = 14;
					}
					else
					{
						num2 = 12;
					}
				}
				graphics.DrawString(string.Format("Age:{0}   Gender:{1}", age, arg), new Font(FontFamily.GenericSerif, (float)num2), brush, (float)((startX < 1) ? 0 : startX), (float)((startY - 20 < 1) ? 0 : (startY - 20)));
				result = image2;
				return result;
			}
			catch (Exception ex)
			{
				Console.WriteLine(ex.Message);
			}
			finally
			{
				graphics.Dispose();
			}
			result = null;
			return result;
		}

		public static Image ScaleImage(Image image, int dstWidth, int dstHeight)
		{
			Graphics graphics = null;
			Image result;
			try
			{
				float widthAndHeight = ImageUtil.getWidthAndHeight(image.Width, image.Height, dstWidth, dstHeight);
				int num = (int)((float)image.Width * widthAndHeight);
				int num2 = (int)((float)image.Height * widthAndHeight);
				bool flag = num % 4 != 0;
				if (flag)
				{
					num -= num % 4;
				}
				Bitmap bitmap = new Bitmap(num, num2);
				graphics = Graphics.FromImage(bitmap);
				graphics.Clear(Color.Transparent);
				graphics.CompositingQuality = CompositingQuality.HighQuality;
				graphics.SmoothingMode = SmoothingMode.HighQuality;
				graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
				graphics.DrawImage(image, new Rectangle((num - num) / 2, (num2 - num2) / 2, num, num2), 0, 0, image.Width, image.Height, GraphicsUnit.Pixel);
				result = bitmap;
				return result;
			}
			catch (Exception ex)
			{
				Console.WriteLine(ex.Message);
			}
			finally
			{
				bool flag2 = graphics != null;
				if (flag2)
				{
					graphics.Dispose();
				}
			}
			result = null;
			return result;
		}

		public static float getWidthAndHeight(int oldWidth, int oldHeigt, int newWidth, int newHeight)
		{
			bool flag = oldWidth >= newWidth && oldHeigt >= newHeight;
			float result;
			if (flag)
			{
				int num = oldWidth - newWidth;
				int num2 = oldHeigt - newHeight;
				bool flag2 = num > num2;
				if (flag2)
				{
					result = (float)newWidth * 1f / (float)oldWidth;
				}
				else
				{
					result = (float)newHeight * 1f / (float)oldHeigt;
				}
			}
			else
			{
				bool flag3 = oldWidth >= newWidth && oldHeigt < newHeight;
				if (flag3)
				{
					result = (float)newWidth * 1f / (float)oldWidth;
				}
				else
				{
					bool flag4 = oldWidth < newWidth && oldHeigt >= newHeight;
					if (flag4)
					{
						result = (float)newHeight * 1f / (float)oldHeigt;
					}
					else
					{
						int num3 = newWidth - oldWidth;
						int num4 = newHeight - oldHeigt;
						bool flag5 = num3 > num4;
						if (flag5)
						{
							result = (float)newHeight * 1f / (float)oldHeigt;
						}
						else
						{
							result = (float)newWidth * 1f / (float)oldWidth;
						}
					}
				}
			}
			return result;
		}

		public static Image CutImage(Image src, int left, int top, int right, int bottom, ref int tempLeft, ref int tempRight, ref int tempTop, ref int tempBottom)
		{
			Image result;
			try
			{
				Bitmap image = new Bitmap(src);
				int num = right - left;
				int num2 = bottom - top;
				int num3 = num / 2;
				int num4 = num2 / 2;
				bool flag = left < num3;
				if (flag)
				{
					left = 0;
				}
				else
				{
					left -= num3;
					tempLeft = num3;
					tempRight = tempLeft + num;
				}
				bool flag2 = top < num4;
				if (flag2)
				{
					top = 0;
				}
				else
				{
					top -= num4;
					tempTop = num4;
					tempBottom = tempTop + num2;
				}
				right = ((right + num3 > src.Width) ? src.Width : (right + num3));
				bottom = ((bottom + num4 > src.Height) ? src.Height : (bottom + num4));
				num = right - left;
				num2 = bottom - top;
				Bitmap bitmap = new Bitmap(num, num2);
				using (Graphics graphics = Graphics.FromImage(bitmap))
				{
					graphics.Clear(Color.Transparent);
					graphics.CompositingQuality = CompositingQuality.HighQuality;
					graphics.SmoothingMode = SmoothingMode.HighQuality;
					graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
					graphics.DrawImage(image, new Rectangle(0, 0, num, num2), left, top, num, num2, GraphicsUnit.Pixel);
				}
				result = bitmap;
				return result;
			}
			catch (Exception value)
			{
				Console.WriteLine(value);
			}
			result = null;
			return result;
		}
	}
}
