using System;
using System.Runtime.InteropServices;

namespace OpenSoftFace.Setting.Utils
{
	public class MemoryUtil
	{
		public static IntPtr Malloc(int len)
		{
			return Marshal.AllocHGlobal(len);
		}

		public static void Free(IntPtr ptr)
		{
			Marshal.FreeHGlobal(ptr);
		}

		public static void Copy(byte[] source, int startIndex, IntPtr destination, int length)
		{
			Marshal.Copy(source, startIndex, destination, length);
		}

		public static void Copy(IntPtr source, byte[] destination, int startIndex, int length)
		{
			Marshal.Copy(source, destination, startIndex, length);
		}

		public static T PtrToStructure<T>(IntPtr ptr)
		{
			return Marshal.PtrToStructure<T>(ptr);
		}

		public static void StructureToPtr<T>(T t, IntPtr ptr)
		{
			Marshal.StructureToPtr<T>(t, ptr, false);
		}

		public static int SizeOf<T>()
		{
			return Marshal.SizeOf<T>();
		}
	}
}
