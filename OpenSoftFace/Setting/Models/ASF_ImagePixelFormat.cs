using System;
using System.Runtime.InteropServices;

namespace OpenSoftFace.Setting.Models
{
	[StructLayout(LayoutKind.Sequential, Size = 1)]
	public struct ASF_ImagePixelFormat
	{
		public const int ASVL_PAF_RGB24_B8G8R8 = 513;

		public const int ASVL_PAF_GRAY = 1793;
	}
}
