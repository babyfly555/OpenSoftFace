using System;

namespace OpenSoftFace.Setting.Models
{
	public struct ASF_MultiFaceInfo
	{
		public IntPtr faceRects;

		public IntPtr faceOrients;

		public int faceNum;

		public IntPtr faceID;
	}
}
