using System;

namespace OpenSoftFace.Setting.Models
{
	public enum ASF_OrientCode
	{
		ASF_OC_0 = 1,
		ASF_OC_90,
		ASF_OC_270,
		ASF_OC_180,
		ASF_OC_30,
		ASF_OC_60,
		ASF_OC_120,
		ASF_OC_150,
		ASF_OC_210,
		ASF_OC_240,
		ASF_OC_300,
		ASF_OC_330
	}
}
