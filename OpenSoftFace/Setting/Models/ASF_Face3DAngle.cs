using System;

namespace OpenSoftFace.Setting.Models
{
	public struct ASF_Face3DAngle
	{
		public IntPtr roll;

		public IntPtr yaw;

		public IntPtr pitch;

		public IntPtr status;

		public int num;
	}
}
