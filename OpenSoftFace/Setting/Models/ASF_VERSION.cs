using System;

namespace OpenSoftFace.Setting.Models
{
	public struct ASF_VERSION
	{
		public IntPtr Version;

		public IntPtr BuildDate;

		public IntPtr CopyRight;
	}
}
