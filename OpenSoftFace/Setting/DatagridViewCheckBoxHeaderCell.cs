using System;
using System.Diagnostics;
using System.Drawing;
using System.Runtime.CompilerServices;
using System.Windows.Forms;
using System.Windows.Forms.VisualStyles;

namespace OpenSoftFace.Setting
{
	public class DatagridViewCheckBoxHeaderCell : DataGridViewColumnHeaderCell
	{
		private Point checkBoxLocation;

		private Size checkBoxSize;

		private bool _checked = false;

		private Point _cellLocation = default(Point);

		private CheckBoxState _cbState = CheckBoxState.UncheckedNormal;

		[method: CompilerGenerated]
		//[DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
		public event CheckBoxClickedHandler OnCheckBoxClicked;

		public bool IsChecked
		{
			get
			{
				return this._checked;
			}
		}

		public DatagridViewCheckBoxHeaderCell(bool checkedValue = false)
		{
			this._checked = checkedValue;
		}

		public void SetCheck(bool bChecked)
		{
			this._checked = bChecked;
			base.DataGridView.InvalidateCell(this);
		}

		protected override void Paint(Graphics graphics, Rectangle clipBounds, Rectangle cellBounds, int rowIndex, DataGridViewElementStates dataGridViewElementState, object value, object formattedValue, string errorText, DataGridViewCellStyle cellStyle, DataGridViewAdvancedBorderStyle advancedBorderStyle, DataGridViewPaintParts paintParts)
		{
			base.Paint(graphics, clipBounds, cellBounds, rowIndex, dataGridViewElementState, value, formattedValue, errorText, cellStyle, advancedBorderStyle, paintParts);
			Point point = default(Point);
			Size glyphSize = CheckBoxRenderer.GetGlyphSize(graphics, CheckBoxState.UncheckedNormal);
			point.X = cellBounds.Location.X + 12;
			point.Y = cellBounds.Location.Y + cellBounds.Height / 2 - glyphSize.Height / 2;
			this._cellLocation = cellBounds.Location;
			this.checkBoxLocation = point;
			this.checkBoxSize = glyphSize;
			bool @checked = this._checked;
			if (@checked)
			{
				this._cbState = CheckBoxState.CheckedNormal;
			}
			else
			{
				this._cbState = CheckBoxState.UncheckedNormal;
			}
			CheckBoxRenderer.DrawCheckBox(graphics, this.checkBoxLocation, this._cbState);
		}

		protected override void OnMouseClick(DataGridViewCellMouseEventArgs e)
		{
			Point point = new Point(e.X + this._cellLocation.X, e.Y + this._cellLocation.Y);
			bool flag = point.X >= this.checkBoxLocation.X && point.X <= this.checkBoxLocation.X + this.checkBoxSize.Width && point.Y >= this.checkBoxLocation.Y && point.Y <= this.checkBoxLocation.Y + this.checkBoxSize.Height;
			if (flag)
			{
				this._checked = !this._checked;
				bool flag2 = this.OnCheckBoxClicked != null;
				if (flag2)
				{
					this.OnCheckBoxClicked(this._checked);
					base.DataGridView.InvalidateCell(this);
				}
			}
			base.OnMouseClick(e);
		}
	}
}
