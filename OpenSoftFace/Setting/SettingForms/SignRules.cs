using OpenSoftFace.Setting.Common;
using OpenSoftFace.Setting.Controls;
using DAL.Service;
using DAL.Service.Impl;
using LitJson2;
using LogUtil;
using Model.Entity;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Globalization;
using System.Runtime.CompilerServices;
using System.Windows.Forms;

namespace OpenSoftFace.Setting.Forms.SettingForms
{
	public class SignRules : Form
	{
		private IniHelper iniHelper = new IniHelper("setting.ini", "setting");

		private ToolTip btnToolTip = new ToolTip();

		private IAttendRuleService attendRuleService = new AttendRuleServiceImpl();

		private AttendRule attendRule = new AttendRule();

		private BaseForm baseForm;

		private IContainer components = null;

		private MyButton btn_update;

		private RadioButton rab_recog_mode1;

		private RadioButton rab_recog_mode2;

		private Panel panel_liveness;

		private CheckBox cbx_monday;

		private Label label1;

		private DateTimePicker datetime_start;

		private DateTimePicker datetime_end;

		private Label label2;

		private Label label3;

		private MyTextBox txt_work_hour;

		private Label label4;

		private Label label5;

		private Label label7;

		private CheckBox cbx_tuesday;

		private CheckBox cbx_wednesday;

		private CheckBox cbx_thursday;

		private CheckBox cbx_friday;

		private CheckBox cbx_saturday;

		private CheckBox cbx_sunday;

		private TableLayoutPanel tableLayoutPanel2;

		private Label lbl_attend_effect;

		private TableLayoutPanel panel_warning;

		private PictureBox pic_warning;

		private Label lbl_warning;

		private TableLayoutPanel panel_current_rule;

		private MyButton btn_add;

		private Label label6;

		private Panel panel_effective_mode;

		private RadioButton rab_effective_mode_2;

		private RadioButton rab_effective_mode_1;
        private MyTextBox txt_work_delay_min;
        private Label label8;
        private Label label9;

		private Label lbl_recog_retry_tip;

		[method: CompilerGenerated]
		//[DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
		public event Action showLayer;

		[method: CompilerGenerated]
		//[DebuggerBrowsable(DebuggerBrowsableState.Never), CompilerGenerated]
		public event Action hideLayer;

		public SignRules()
		{
			this.InitializeComponent();
			base.TopMost = true;
		}

		public SignRules(BaseForm baseForm)
		{
			this.InitializeComponent();
			base.TopMost = true;
			this.baseForm = baseForm;
		}

		private void BasicParamForm_Shown(object sender, EventArgs e)
		{
			try
			{
				this.panel_current_rule.Visible = false;
				this.attendRuleService.Delete();
				this.attendRule = this.attendRuleService.Get(true);
				bool flag = this.attendRule != null && !string.IsNullOrEmpty(this.attendRule.Json);
				if (flag)
				{
					AttendRuleParam attendRuleParam = new AttendRuleParam();
					attendRuleParam = JsonMapper.ToObject<AttendRuleParam>(this.attendRule.Json);
					bool flag2 = this.attendRuleService.Count(null) > 1;
					if (flag2)
					{
						DateTime now = DateTime.Now;
						bool flag3 = DateTime.TryParse(this.attendRule.StartDate, out now);
						if (flag3)
						{
							bool flag4 = DateTime.Compare(DateTime.Now, now) < 0;
							if (flag4)
							{
								this.panel_warning.Visible = true;
								this.lbl_warning.Text = string.Format("新规则将于{0}开始生效", now.ToString("yyyy.MM.dd", DateTimeFormatInfo.InvariantInfo));
								this.panel_current_rule.Visible = true;
							}
						}
					}
					else
					{
						bool flag5 = 1.Equals(attendRuleParam.effective_mode);
						if (flag5)
						{
							attendRuleParam.effective_mode = 0;
							this.attendRule.Json = JsonMapper.ToJson(attendRuleParam);
							this.attendRuleService.Update(this.attendRule);
						}
					}
					this.fillInfoControl(attendRuleParam);
				}
			}
			catch (Exception se)
			{
				LogHelper.LogError(base.GetType(), se);
			}
		}

		public void fillInfoControl(AttendRuleParam ruleParam)
		{
			try
			{
				bool flag = true;
				bool flag2 = ruleParam != null && "1".Equals(ruleParam.attend_rule_mode);
				if (flag2)
				{
					flag = false;
				}
				bool flag3 = flag;
				if (flag3)
				{
					this.rab_recog_mode1.Checked = true;
					this.txt_work_hour.Enabled = false;
				}
				else
				{
					this.rab_recog_mode2.Checked = true;
					this.datetime_start.Enabled = false;
					this.datetime_end.Enabled = false;
				}
				string text = "09:00";
				bool flag4 = ruleParam != null && !string.IsNullOrEmpty(ruleParam.attend_start_time);
				if (flag4)
				{
					text = ruleParam.attend_start_time;
				}
				this.datetime_start.Text = text;
				string text2 = "18:00";
				bool flag5 = ruleParam != null && !string.IsNullOrEmpty(ruleParam.attend_end_time);
				if (flag5)
				{
					text2 = ruleParam.attend_end_time;
				}
				this.datetime_end.Text = text2;
				string text3 = "8";
				bool flag6 = ruleParam != null && !string.IsNullOrEmpty(ruleParam.work_hours_limit);
				if (flag6)
				{
					text3 = ruleParam.work_hours_limit;
				}
				this.txt_work_hour.Text = text3;
				this.lbl_attend_effect.Text = "00:00:00 - 23:59:59";
				string source = string.Empty;
				bool flag7 = ruleParam != null && !string.IsNullOrEmpty(ruleParam.rest_day);
				if (flag7)
				{
					source = ruleParam.rest_day;
				}
				this.SetCbxCheckState(this.cbx_monday, source, "1");
				this.SetCbxCheckState(this.cbx_tuesday, source, "2");
				this.SetCbxCheckState(this.cbx_wednesday, source, "3");
				this.SetCbxCheckState(this.cbx_thursday, source, "4");
				this.SetCbxCheckState(this.cbx_friday, source, "5");
				this.SetCbxCheckState(this.cbx_saturday, source, "6");
				this.SetCbxCheckState(this.cbx_sunday, source, "7");
				int obj = 1;
				bool flag8 = ruleParam != null && ruleParam.effective_mode >= 0;
				if (flag8)
				{
					bool flag9 = 0.Equals(ruleParam.effective_mode);
					if (flag9)
					{
						obj = 0;
					}
				}
				this.rab_effective_mode_1.Checked = 1.Equals(obj);
				this.rab_effective_mode_2.Checked = 0.Equals(obj);
                this.txt_work_delay_min.Text=this.iniHelper.Get("WORK_DELAY_MIN");
			}
			catch (Exception se)
			{
				LogHelper.LogError(base.GetType(), se);
			}
		}

		private void SetCbxCheckState(CheckBox cbx, string source, string key)
		{
			cbx.Checked = (!string.IsNullOrWhiteSpace(source) && source.Contains(key));
		}

		private void updateBtn_Click(object sender, EventArgs e)
		{
			try
			{
				bool flag = DateTime.Compare(this.datetime_start.Value, this.datetime_end.Value) >= 0;
				if (flag)
				{
					ErrorDialogForm.ShowErrorMsg("下班时间不能早于或等于上班时间!", "", false, null, false);
				}
				else
				{
                    this.iniHelper.Set("WORK_DELAY_MIN", this.txt_work_delay_min.Text);
					string text = this.txt_work_hour.Text;
					bool flag2 = string.IsNullOrWhiteSpace(text);
					if (flag2)
					{
						ErrorDialogForm.ShowErrorMsg("每日工时为1~24的整数!", "", false, null, false);
					}
					else
					{
						int num = 8;
						try
						{
							num = (int)Convert.ToInt16(text.Trim());
						}
						catch
						{
							num = 8;
						}
						bool flag3 = num <= 0 || num > 24;
						if (flag3)
						{
							ErrorDialogForm.ShowErrorMsg("每日工时为1~24的整数!", "", false, null, false);
						}
						else
						{
							text = num.ToString();
							string attend_rule_mode = "0";
							bool @checked = this.rab_recog_mode2.Checked;
							if (@checked)
							{
								attend_rule_mode = "1";
							}
							List<string> list = new List<string>();
							this.checkCbxState(ref list, this.cbx_monday, "1");
							this.checkCbxState(ref list, this.cbx_tuesday, "2");
							this.checkCbxState(ref list, this.cbx_wednesday, "3");
							this.checkCbxState(ref list, this.cbx_thursday, "4");
							this.checkCbxState(ref list, this.cbx_friday, "5");
							this.checkCbxState(ref list, this.cbx_saturday, "6");
							this.checkCbxState(ref list, this.cbx_sunday, "7");
							string rest_day = string.Empty;
							bool flag4 = list != null && list.Count > 0;
							if (flag4)
							{
								rest_day = string.Join("-", list);
							}
							int num2 = 1;
							bool checked2 = this.rab_effective_mode_2.Checked;
							if (checked2)
							{
								num2 = 0;
							}
							AttendRuleParam attendRuleParam = new AttendRuleParam();
							attendRuleParam.attend_rule_mode = attend_rule_mode;
							attendRuleParam.attend_start_time = this.datetime_start.Text;
							attendRuleParam.attend_end_time = this.datetime_end.Text;
							attendRuleParam.effective_time_start = "00:00:00";
							attendRuleParam.effective_time_end = "23:59:59";
							attendRuleParam.work_hours_limit = text;
							attendRuleParam.rest_day = rest_day;
							attendRuleParam.effective_mode = num2;
							int num3 = this.attendRuleService.Count(null);
							int num4 = 0;
							this.attendRule = this.attendRuleService.Get(true);
							DateTime dateTime = DateTime.Now.AddMonths(1);
							DateTime dateTime2 = dateTime.AddDays((double)(1 - dateTime.Day));
							bool flag5 = false;
							bool flag6 = 1.Equals(num2);
							if (flag6)
							{
								flag5 = true;
								string startDate = dateTime2.ToString("yyyy-MM-dd");
								bool flag7 = num3 > 1 && this.attendRule != null && this.attendRule.ID > 0;
								if (flag7)
								{
									this.attendRule.Json = JsonMapper.ToJson(attendRuleParam);
									this.attendRule.StartDate = startDate;
									num4 = this.attendRuleService.Update(this.attendRule);
								}
								else
								{
									AttendRule attendRule = new AttendRule();
									int num5 = this.attendRuleService.MaxId();
									attendRule.ID = num5;
									attendRule.StartDate = startDate;
									attendRule.Json = JsonMapper.ToJson(attendRuleParam);
									num4 = this.attendRuleService.Insert(attendRule);
									bool flag8 = num4 > 0;
									if (flag8)
									{
										this.attendRule.EndDate = dateTime2.AddDays(-1.0).ToString("yyyy-MM-dd");
										bool flag9 = this.attendRuleService.Update(this.attendRule) <= 0;
										if (flag9)
										{
											num4 = -1;
											this.attendRuleService.Delete(num5);
										}
									}
								}
							}
							else
							{
								bool flag10 = this.attendRule != null && this.attendRule.ID > 0;
								if (flag10)
								{
									bool flag11 = num3 > 1;
									if (flag11)
									{
										this.attendRuleService.DeleteOtherRecordById(this.attendRule.ID);
									}
									this.attendRule.Json = JsonMapper.ToJson(attendRuleParam);
									this.attendRule.StartDate = DateTime.Now.ToString("yyyy-MM-dd");
									num4 = this.attendRuleService.Update(this.attendRule);
								}
							}
							bool flag12 = num4 > 0;
							if (flag12)
							{
								AttendRule attendRule2 = this.attendRuleService.Get(false);
								this.panel_current_rule.Visible = (attendRule2 != null && !string.IsNullOrEmpty(attendRule2.Json));
								UpdateSuccessDialogForm.showMsg(!flag5);
								this.panel_warning.Visible = flag5;
								bool flag13 = flag5;
								if (flag13)
								{
									this.lbl_warning.Text = string.Format("新规则将于{0}开始生效", dateTime2.ToString("yyyy.MM.dd", DateTimeFormatInfo.InvariantInfo));
								}
							}
							else
							{
								ErrorDialogForm.ShowErrorMsg("设置失败，请稍后重试!", "", true, this, false);
							}
						}
					}
				}
			}
			catch (Exception se)
			{
				ErrorDialogForm.ShowErrorMsg("设置失败，请稍后重试!", "", true, this, false);
				LogHelper.LogError(base.GetType(), se);
			}
		}

		private void checkCbxState(ref List<string> list, CheckBox cbx, string val)
		{
			bool @checked = cbx.Checked;
			if (@checked)
			{
				list.Add(val);
			}
		}

		private void enabledFormAndChangeShow(bool isEnabled)
		{
			base.Enabled = isEnabled;
			if (isEnabled)
			{
				Action expr_15 = this.hideLayer;
				if (expr_15 != null)
				{
					expr_15();
				}
			}
			else
			{
				Action expr_2B = this.showLayer;
				if (expr_2B != null)
				{
					expr_2B();
				}
			}
		}

		private void rab_recog_mode2_CheckedChanged(object sender, EventArgs e)
		{
			this.txt_work_hour.Enabled = this.rab_recog_mode2.Checked;
		}

		private void rab_recog_mode1_CheckedChanged(object sender, EventArgs e)
		{
			this.datetime_start.Enabled = this.rab_recog_mode1.Checked;
			this.datetime_end.Enabled = this.rab_recog_mode1.Checked;
		}

		private void txt_work_hour_KeyPress(object sender, KeyPressEventArgs e)
		{
			try
			{
				bool flag = e.KeyChar != '\b' && !char.IsDigit(e.KeyChar);
				if (flag)
				{
					e.Handled = true;
				}
			}
			catch (Exception se)
			{
				LogHelper.LogError(base.GetType(), se);
			}
		}

		private void btn_add_Click(object sender, EventArgs e)
		{
			try
			{
				AttendRule attendRule = this.attendRuleService.Get(false);
				bool flag = attendRule != null && !string.IsNullOrEmpty(attendRule.Json);
				if (flag)
				{
					AttendRuleParam ruleParam = new AttendRuleParam();
					ruleParam = JsonMapper.ToObject<AttendRuleParam>(attendRule.Json);
					this.baseForm.ShowOpaqueLayer();
					bool flag2 = DialogResult.OK.Equals(CurrentRule.showCrrentRule(ruleParam));
					if (flag2)
					{
						this.baseForm.HideOpaqueLayer();
					}
				}
			}
			catch (Exception se)
			{
				LogHelper.LogError(base.GetType(), se);
			}
		}

		protected override void Dispose(bool disposing)
		{
			bool flag = disposing && this.components != null;
			if (flag)
			{
				this.components.Dispose();
			}
			base.Dispose(disposing);
		}

		private void InitializeComponent()
		{
            this.rab_recog_mode1 = new System.Windows.Forms.RadioButton();
            this.rab_recog_mode2 = new System.Windows.Forms.RadioButton();
            this.cbx_monday = new System.Windows.Forms.CheckBox();
            this.panel_liveness = new System.Windows.Forms.Panel();
            this.txt_work_hour = new OpenSoftFace.Setting.Controls.MyTextBox();
            this.datetime_end = new System.Windows.Forms.DateTimePicker();
            this.datetime_start = new System.Windows.Forms.DateTimePicker();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.cbx_tuesday = new System.Windows.Forms.CheckBox();
            this.cbx_wednesday = new System.Windows.Forms.CheckBox();
            this.cbx_thursday = new System.Windows.Forms.CheckBox();
            this.cbx_friday = new System.Windows.Forms.CheckBox();
            this.cbx_saturday = new System.Windows.Forms.CheckBox();
            this.cbx_sunday = new System.Windows.Forms.CheckBox();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.btn_update = new OpenSoftFace.Setting.Controls.MyButton();
            this.lbl_attend_effect = new System.Windows.Forms.Label();
            this.panel_warning = new System.Windows.Forms.TableLayoutPanel();
            this.pic_warning = new System.Windows.Forms.PictureBox();
            this.lbl_warning = new System.Windows.Forms.Label();
            this.panel_current_rule = new System.Windows.Forms.TableLayoutPanel();
            this.btn_add = new OpenSoftFace.Setting.Controls.MyButton();
            this.label6 = new System.Windows.Forms.Label();
            this.panel_effective_mode = new System.Windows.Forms.Panel();
            this.rab_effective_mode_2 = new System.Windows.Forms.RadioButton();
            this.rab_effective_mode_1 = new System.Windows.Forms.RadioButton();
            this.lbl_recog_retry_tip = new System.Windows.Forms.Label();
            this.txt_work_delay_min = new OpenSoftFace.Setting.Controls.MyTextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.tableLayoutPanel2.SuspendLayout();
            this.panel_warning.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pic_warning)).BeginInit();
            this.panel_current_rule.SuspendLayout();
            this.panel_effective_mode.SuspendLayout();
            this.SuspendLayout();
            // 
            // rab_recog_mode1
            // 
            this.rab_recog_mode1.AutoSize = true;
            this.rab_recog_mode1.Checked = true;
            this.rab_recog_mode1.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.rab_recog_mode1.Location = new System.Drawing.Point(25, 59);
            this.rab_recog_mode1.Name = "rab_recog_mode1";
            this.rab_recog_mode1.Size = new System.Drawing.Size(50, 21);
            this.rab_recog_mode1.TabIndex = 1;
            this.rab_recog_mode1.TabStop = true;
            this.rab_recog_mode1.Tag = "1";
            this.rab_recog_mode1.Text = "固定";
            this.rab_recog_mode1.UseVisualStyleBackColor = true;
            this.rab_recog_mode1.CheckedChanged += new System.EventHandler(this.rab_recog_mode1_CheckedChanged);
            // 
            // rab_recog_mode2
            // 
            this.rab_recog_mode2.AutoSize = true;
            this.rab_recog_mode2.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.rab_recog_mode2.Location = new System.Drawing.Point(28, 216);
            this.rab_recog_mode2.Name = "rab_recog_mode2";
            this.rab_recog_mode2.Size = new System.Drawing.Size(50, 21);
            this.rab_recog_mode2.TabIndex = 4;
            this.rab_recog_mode2.TabStop = true;
            this.rab_recog_mode2.Tag = "2";
            this.rab_recog_mode2.Text = "弹性";
            this.rab_recog_mode2.UseVisualStyleBackColor = true;
            this.rab_recog_mode2.Visible = false;
            this.rab_recog_mode2.CheckedChanged += new System.EventHandler(this.rab_recog_mode2_CheckedChanged);
            // 
            // cbx_monday
            // 
            this.cbx_monday.AutoSize = true;
            this.cbx_monday.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.cbx_monday.Location = new System.Drawing.Point(138, 23);
            this.cbx_monday.Name = "cbx_monday";
            this.cbx_monday.Size = new System.Drawing.Size(51, 21);
            this.cbx_monday.TabIndex = 8;
            this.cbx_monday.Text = "周一";
            this.cbx_monday.UseVisualStyleBackColor = true;
            // 
            // panel_liveness
            // 
            this.panel_liveness.Location = new System.Drawing.Point(498, 294);
            this.panel_liveness.Name = "panel_liveness";
            this.panel_liveness.Size = new System.Drawing.Size(155, 51);
            this.panel_liveness.TabIndex = 54;
            // 
            // txt_work_hour
            // 
            this.txt_work_hour.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(178)))), ((int)(((byte)(178)))), ((int)(((byte)(178)))));
            this.txt_work_hour.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_work_hour.EmptyTextTip = null;
            this.txt_work_hour.EmptyTextTipColor = System.Drawing.Color.DarkGray;
            this.txt_work_hour.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.txt_work_hour.Location = new System.Drawing.Point(198, 216);
            this.txt_work_hour.MaxLength = 4;
            this.txt_work_hour.Name = "txt_work_hour";
            this.txt_work_hour.Size = new System.Drawing.Size(53, 23);
            this.txt_work_hour.TabIndex = 5;
            this.txt_work_hour.Visible = false;
            this.txt_work_hour.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txt_work_hour_KeyPress);
            // 
            // datetime_end
            // 
            this.datetime_end.CustomFormat = "HH:mm";
            this.datetime_end.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.datetime_end.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.datetime_end.Location = new System.Drawing.Point(326, 60);
            this.datetime_end.Name = "datetime_end";
            this.datetime_end.ShowUpDown = true;
            this.datetime_end.Size = new System.Drawing.Size(53, 23);
            this.datetime_end.TabIndex = 3;
            this.datetime_end.Value = new System.DateTime(2020, 3, 3, 18, 0, 0, 0);
            // 
            // datetime_start
            // 
            this.datetime_start.CalendarFont = new System.Drawing.Font("微软雅黑", 9F);
            this.datetime_start.CustomFormat = "HH:mm";
            this.datetime_start.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.datetime_start.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.datetime_start.Location = new System.Drawing.Point(194, 60);
            this.datetime_start.Name = "datetime_start";
            this.datetime_start.ShowUpDown = true;
            this.datetime_start.Size = new System.Drawing.Size(53, 23);
            this.datetime_start.TabIndex = 2;
            this.datetime_start.Value = new System.DateTime(2020, 3, 3, 9, 0, 0, 0);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.label3.Location = new System.Drawing.Point(139, 218);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(56, 17);
            this.label3.TabIndex = 52;
            this.label3.Text = "每日工时";
            this.label3.Visible = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.label2.Location = new System.Drawing.Point(267, 63);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(56, 17);
            this.label2.TabIndex = 52;
            this.label2.Text = "下班时间";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label1.Location = new System.Drawing.Point(135, 63);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(56, 17);
            this.label1.TabIndex = 52;
            this.label1.Text = "上班时间";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.label4.Location = new System.Drawing.Point(253, 218);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(32, 17);
            this.label4.TabIndex = 52;
            this.label4.Text = "小时";
            this.label4.Visible = false;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.label5.Location = new System.Drawing.Point(29, 306);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(56, 17);
            this.label5.TabIndex = 52;
            this.label5.Text = "考勤时间";
            this.label5.Visible = false;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.label7.Location = new System.Drawing.Point(22, 24);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(44, 17);
            this.label7.TabIndex = 52;
            this.label7.Text = "休息日";
            // 
            // cbx_tuesday
            // 
            this.cbx_tuesday.AutoSize = true;
            this.cbx_tuesday.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.cbx_tuesday.Location = new System.Drawing.Point(200, 23);
            this.cbx_tuesday.Name = "cbx_tuesday";
            this.cbx_tuesday.Size = new System.Drawing.Size(51, 21);
            this.cbx_tuesday.TabIndex = 9;
            this.cbx_tuesday.Text = "周二";
            this.cbx_tuesday.UseVisualStyleBackColor = true;
            // 
            // cbx_wednesday
            // 
            this.cbx_wednesday.AutoSize = true;
            this.cbx_wednesday.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.cbx_wednesday.Location = new System.Drawing.Point(266, 23);
            this.cbx_wednesday.Name = "cbx_wednesday";
            this.cbx_wednesday.Size = new System.Drawing.Size(51, 21);
            this.cbx_wednesday.TabIndex = 10;
            this.cbx_wednesday.Text = "周三";
            this.cbx_wednesday.UseVisualStyleBackColor = true;
            // 
            // cbx_thursday
            // 
            this.cbx_thursday.AutoSize = true;
            this.cbx_thursday.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.cbx_thursday.Location = new System.Drawing.Point(330, 23);
            this.cbx_thursday.Name = "cbx_thursday";
            this.cbx_thursday.Size = new System.Drawing.Size(51, 21);
            this.cbx_thursday.TabIndex = 11;
            this.cbx_thursday.Text = "周四";
            this.cbx_thursday.UseVisualStyleBackColor = true;
            // 
            // cbx_friday
            // 
            this.cbx_friday.AutoSize = true;
            this.cbx_friday.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.cbx_friday.Location = new System.Drawing.Point(393, 23);
            this.cbx_friday.Name = "cbx_friday";
            this.cbx_friday.Size = new System.Drawing.Size(51, 21);
            this.cbx_friday.TabIndex = 12;
            this.cbx_friday.Text = "周五";
            this.cbx_friday.UseVisualStyleBackColor = true;
            // 
            // cbx_saturday
            // 
            this.cbx_saturday.AutoSize = true;
            this.cbx_saturday.Checked = true;
            this.cbx_saturday.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbx_saturday.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.cbx_saturday.Location = new System.Drawing.Point(455, 23);
            this.cbx_saturday.Name = "cbx_saturday";
            this.cbx_saturday.Size = new System.Drawing.Size(51, 21);
            this.cbx_saturday.TabIndex = 13;
            this.cbx_saturday.Text = "周六";
            this.cbx_saturday.UseVisualStyleBackColor = true;
            // 
            // cbx_sunday
            // 
            this.cbx_sunday.AutoSize = true;
            this.cbx_sunday.Checked = true;
            this.cbx_sunday.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbx_sunday.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.cbx_sunday.Location = new System.Drawing.Point(521, 23);
            this.cbx_sunday.Name = "cbx_sunday";
            this.cbx_sunday.Size = new System.Drawing.Size(51, 21);
            this.cbx_sunday.TabIndex = 14;
            this.cbx_sunday.Text = "周日";
            this.cbx_sunday.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 1;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Controls.Add(this.btn_update, 0, 0);
            this.tableLayoutPanel2.Location = new System.Drawing.Point(291, 372);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 1;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(86, 31);
            this.tableLayoutPanel2.TabIndex = 58;
            // 
            // btn_update
            // 
            this.btn_update.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(1)))), ((int)(((byte)(119)))), ((int)(((byte)(213)))));
            this.btn_update.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btn_update.EnterBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(93)))), ((int)(((byte)(163)))), ((int)(((byte)(239)))));
            this.btn_update.EnterForeColor = System.Drawing.Color.White;
            this.btn_update.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(1)))), ((int)(((byte)(119)))), ((int)(((byte)(213)))));
            this.btn_update.FlatAppearance.BorderSize = 0;
            this.btn_update.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_update.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btn_update.ForeColor = System.Drawing.Color.White;
            this.btn_update.LeaveBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(1)))), ((int)(((byte)(119)))), ((int)(((byte)(213)))));
            this.btn_update.LeaveForeColor = System.Drawing.Color.White;
            this.btn_update.Location = new System.Drawing.Point(3, 3);
            this.btn_update.MaximumSize = new System.Drawing.Size(86, 31);
            this.btn_update.Name = "btn_update";
            this.btn_update.Size = new System.Drawing.Size(80, 25);
            this.btn_update.TabIndex = 16;
            this.btn_update.Text = "确认";
            this.btn_update.UseVisualStyleBackColor = false;
            this.btn_update.Click += new System.EventHandler(this.updateBtn_Click);
            // 
            // lbl_attend_effect
            // 
            this.lbl_attend_effect.AutoSize = true;
            this.lbl_attend_effect.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.lbl_attend_effect.Location = new System.Drawing.Point(143, 306);
            this.lbl_attend_effect.Name = "lbl_attend_effect";
            this.lbl_attend_effect.Size = new System.Drawing.Size(117, 17);
            this.lbl_attend_effect.TabIndex = 59;
            this.lbl_attend_effect.Text = "00:00:00 - 23:59:59";
            this.lbl_attend_effect.Visible = false;
            // 
            // panel_warning
            // 
            this.panel_warning.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(252)))), ((int)(((byte)(231)))));
            this.panel_warning.ColumnCount = 2;
            this.panel_warning.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 34F));
            this.panel_warning.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.panel_warning.Controls.Add(this.pic_warning, 0, 0);
            this.panel_warning.Controls.Add(this.lbl_warning, 1, 0);
            this.panel_warning.Location = new System.Drawing.Point(23, 433);
            this.panel_warning.Margin = new System.Windows.Forms.Padding(0);
            this.panel_warning.Name = "panel_warning";
            this.panel_warning.RowCount = 1;
            this.panel_warning.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.panel_warning.Size = new System.Drawing.Size(630, 30);
            this.panel_warning.TabIndex = 60;
            this.panel_warning.Visible = false;
            // 
            // pic_warning
            // 
            this.pic_warning.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(249)))), ((int)(((byte)(208)))));
            this.pic_warning.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pic_warning.Location = new System.Drawing.Point(9, 7);
            this.pic_warning.Margin = new System.Windows.Forms.Padding(9, 7, 5, 7);
            this.pic_warning.Name = "pic_warning";
            this.pic_warning.Size = new System.Drawing.Size(20, 16);
            this.pic_warning.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pic_warning.TabIndex = 0;
            this.pic_warning.TabStop = false;
            // 
            // lbl_warning
            // 
            this.lbl_warning.AutoSize = true;
            this.lbl_warning.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbl_warning.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.lbl_warning.Location = new System.Drawing.Point(37, 0);
            this.lbl_warning.Name = "lbl_warning";
            this.lbl_warning.Size = new System.Drawing.Size(590, 30);
            this.lbl_warning.TabIndex = 52;
            this.lbl_warning.Text = "新规则将于下月开始生效";
            this.lbl_warning.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lbl_warning.Visible = false;
            // 
            // panel_current_rule
            // 
            this.panel_current_rule.ColumnCount = 1;
            this.panel_current_rule.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.panel_current_rule.Controls.Add(this.btn_add, 0, 0);
            this.panel_current_rule.Location = new System.Drawing.Point(542, 257);
            this.panel_current_rule.MaximumSize = new System.Drawing.Size(86, 31);
            this.panel_current_rule.Name = "panel_current_rule";
            this.panel_current_rule.RowCount = 1;
            this.panel_current_rule.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.panel_current_rule.Size = new System.Drawing.Size(86, 31);
            this.panel_current_rule.TabIndex = 61;
            // 
            // btn_add
            // 
            this.btn_add.BackColor = System.Drawing.Color.White;
            this.btn_add.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btn_add.EnterBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(93)))), ((int)(((byte)(163)))), ((int)(((byte)(239)))));
            this.btn_add.EnterForeColor = System.Drawing.Color.White;
            this.btn_add.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(1)))), ((int)(((byte)(119)))), ((int)(((byte)(213)))));
            this.btn_add.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_add.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btn_add.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(1)))), ((int)(((byte)(119)))), ((int)(((byte)(213)))));
            this.btn_add.LeaveBackColor = System.Drawing.Color.White;
            this.btn_add.LeaveForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(1)))), ((int)(((byte)(119)))), ((int)(((byte)(213)))));
            this.btn_add.Location = new System.Drawing.Point(3, 3);
            this.btn_add.Name = "btn_add";
            this.btn_add.Size = new System.Drawing.Size(80, 25);
            this.btn_add.TabIndex = 1;
            this.btn_add.Text = "当前规则";
            this.btn_add.UseVisualStyleBackColor = false;
            this.btn_add.Click += new System.EventHandler(this.btn_add_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.label6.Location = new System.Drawing.Point(22, 251);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(56, 17);
            this.label6.TabIndex = 52;
            this.label6.Text = "规则生效";
            this.label6.Visible = false;
            // 
            // panel_effective_mode
            // 
            this.panel_effective_mode.Controls.Add(this.rab_effective_mode_2);
            this.panel_effective_mode.Controls.Add(this.rab_effective_mode_1);
            this.panel_effective_mode.Location = new System.Drawing.Point(133, 248);
            this.panel_effective_mode.Name = "panel_effective_mode";
            this.panel_effective_mode.Size = new System.Drawing.Size(200, 27);
            this.panel_effective_mode.TabIndex = 62;
            this.panel_effective_mode.Visible = false;
            // 
            // rab_effective_mode_2
            // 
            this.rab_effective_mode_2.AutoSize = true;
            this.rab_effective_mode_2.Location = new System.Drawing.Point(95, 4);
            this.rab_effective_mode_2.Name = "rab_effective_mode_2";
            this.rab_effective_mode_2.Size = new System.Drawing.Size(71, 16);
            this.rab_effective_mode_2.TabIndex = 10;
            this.rab_effective_mode_2.TabStop = true;
            this.rab_effective_mode_2.Tag = "2";
            this.rab_effective_mode_2.Text = "立即生效";
            this.rab_effective_mode_2.UseVisualStyleBackColor = true;
            // 
            // rab_effective_mode_1
            // 
            this.rab_effective_mode_1.AutoSize = true;
            this.rab_effective_mode_1.Checked = true;
            this.rab_effective_mode_1.Location = new System.Drawing.Point(4, 4);
            this.rab_effective_mode_1.Name = "rab_effective_mode_1";
            this.rab_effective_mode_1.Size = new System.Drawing.Size(71, 16);
            this.rab_effective_mode_1.TabIndex = 9;
            this.rab_effective_mode_1.TabStop = true;
            this.rab_effective_mode_1.Tag = "1";
            this.rab_effective_mode_1.Text = "次月生效";
            this.rab_effective_mode_1.UseVisualStyleBackColor = true;
            // 
            // lbl_recog_retry_tip
            // 
            this.lbl_recog_retry_tip.AutoSize = true;
            this.lbl_recog_retry_tip.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.lbl_recog_retry_tip.ForeColor = System.Drawing.Color.Gray;
            this.lbl_recog_retry_tip.Location = new System.Drawing.Point(134, 275);
            this.lbl_recog_retry_tip.Name = "lbl_recog_retry_tip";
            this.lbl_recog_retry_tip.Size = new System.Drawing.Size(404, 17);
            this.lbl_recog_retry_tip.TabIndex = 63;
            this.lbl_recog_retry_tip.Text = "修改规则后立即生效，将可能导致不同员工的计算规则不一致，请谨慎操作";
            this.lbl_recog_retry_tip.Visible = false;
            // 
            // txt_work_delay_min
            // 
            this.txt_work_delay_min.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(178)))), ((int)(((byte)(178)))), ((int)(((byte)(178)))));
            this.txt_work_delay_min.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_work_delay_min.EmptyTextTip = null;
            this.txt_work_delay_min.EmptyTextTipColor = System.Drawing.Color.DarkGray;
            this.txt_work_delay_min.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.txt_work_delay_min.Location = new System.Drawing.Point(137, 94);
            this.txt_work_delay_min.MaxLength = 4;
            this.txt_work_delay_min.Name = "txt_work_delay_min";
            this.txt_work_delay_min.Size = new System.Drawing.Size(53, 23);
            this.txt_work_delay_min.TabIndex = 64;
            this.txt_work_delay_min.Text = "0";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.label8.Location = new System.Drawing.Point(24, 99);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(80, 17);
            this.label8.TabIndex = 65;
            this.label8.Text = "早上延迟时间";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.label9.Location = new System.Drawing.Point(193, 96);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(32, 17);
            this.label9.TabIndex = 66;
            this.label9.Text = "分钟";
            // 
            // SignRules
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(669, 490);
            this.Controls.Add(this.txt_work_delay_min);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.txt_work_hour);
            this.Controls.Add(this.lbl_recog_retry_tip);
            this.Controls.Add(this.datetime_end);
            this.Controls.Add(this.panel_effective_mode);
            this.Controls.Add(this.datetime_start);
            this.Controls.Add(this.panel_current_rule);
            this.Controls.Add(this.rab_recog_mode2);
            this.Controls.Add(this.panel_warning);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.lbl_attend_effect);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.tableLayoutPanel2);
            this.Controls.Add(this.rab_recog_mode1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cbx_sunday);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.cbx_saturday);
            this.Controls.Add(this.cbx_friday);
            this.Controls.Add(this.cbx_thursday);
            this.Controls.Add(this.cbx_wednesday);
            this.Controls.Add(this.cbx_tuesday);
            this.Controls.Add(this.cbx_monday);
            this.Controls.Add(this.panel_liveness);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label5);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "SignRules";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.TopMost = true;
            this.Shown += new System.EventHandler(this.BasicParamForm_Shown);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.panel_warning.ResumeLayout(false);
            this.panel_warning.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pic_warning)).EndInit();
            this.panel_current_rule.ResumeLayout(false);
            this.panel_effective_mode.ResumeLayout(false);
            this.panel_effective_mode.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

		}
	}
}
