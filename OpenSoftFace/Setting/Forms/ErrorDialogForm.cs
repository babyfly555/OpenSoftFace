using OpenSoftFace.Setting.Controls;
using LogUtil;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using Utils;

namespace OpenSoftFace.Setting.Forms
{
	public class ErrorDialogForm : DragBaseForm
	{
		public delegate DialogResult InvokeDelegate(Form parent, Form subForm);

		private static ErrorDialogForm errorForm;

		public static int labelWidth = 10;

		private IContainer components = null;

		private PictureBox pic_error;

		private Label lbl_errorMsg;

		private MyButton btn_ok;

		private ErrorDialogForm()
		{
			this.InitializeComponent();
		}

		public static void ShowErrorMsg(string msg, string title = null, bool type = false, Form parent = null, bool isCenterWindows = false)
		{
			ErrorDialogForm.errorForm = new ErrorDialogForm();
			ErrorDialogForm.errorForm.TopMost = true;
			ErrorDialogForm.errorForm.lbl_errorMsg.Text = msg;
			bool flag = !string.IsNullOrEmpty(msg);
			if (flag)
			{
				try
				{
					int width = TextRenderer.MeasureText(msg, ErrorDialogForm.errorForm.lbl_errorMsg.Font).Width;
					int num = StringUtil.SearchCharCount(msg, "\r");
					int num2 = width / 180 + num;
					int num3 = num2 * 25;
					ErrorDialogForm.errorForm.lbl_errorMsg.AutoSize = false;
					ErrorDialogForm.errorForm.Height += num3;
					ErrorDialogForm.errorForm.splitContainer.Size = new Size(ErrorDialogForm.errorForm.splitContainer.Width, ErrorDialogForm.errorForm.splitContainer.Height + num3);
					ErrorDialogForm.errorForm.splitContainer.Panel2.Size = new Size(ErrorDialogForm.errorForm.splitContainer.Panel2.Width, ErrorDialogForm.errorForm.splitContainer.Panel2.Height + num3);
					ErrorDialogForm.errorForm.btn_ok.Location = new Point(ErrorDialogForm.errorForm.btn_ok.Location.X, ErrorDialogForm.errorForm.btn_ok.Location.Y + num3);
					ErrorDialogForm.errorForm.lbl_errorMsg.Size = new Size(200, ErrorDialogForm.errorForm.lbl_errorMsg.Height + num3);
					ErrorDialogForm.errorForm.lbl_errorMsg.Text = msg;
				}
				catch (Exception se)
				{
					LogHelper.LogError(typeof(ErrorDialogForm), se);
				}
			}
			bool flag2 = !string.IsNullOrEmpty(title);
			if (flag2)
			{
				ErrorDialogForm.errorForm.lbl_title.Text = title;
			}
			if (type)
			{
				//ErrorDialogForm.errorForm.pic_error.Image = Resources.成功1;
			}
			if (isCenterWindows)
			{
				ErrorDialogForm.errorForm.StartPosition = FormStartPosition.CenterScreen;
			}
			bool flag3 = parent != null;
			if (flag3)
			{
				ErrorDialogForm.xshowdialog(parent, ErrorDialogForm.errorForm);
			}
			else
			{
				ErrorDialogForm.errorForm.ShowDialog();
			}
		}

		private void ok_Click(object sender, EventArgs e)
		{
			bool flag = ErrorDialogForm.errorForm != null;
			if (flag)
			{
				ErrorDialogForm.errorForm.Close();
			}
		}

		public static DialogResult xshowdialog(Form parent, Form subForm)
		{
			bool invokeRequired = parent.InvokeRequired;
			DialogResult result;
			if (invokeRequired)
			{
				ErrorDialogForm.InvokeDelegate method = new ErrorDialogForm.InvokeDelegate(ErrorDialogForm.xshowdialog);
				parent.Invoke(method, new object[]
				{
					parent,
					subForm
				});
				result = subForm.DialogResult;
			}
			else
			{
				result = subForm.ShowDialog(parent);
			}
			return result;
		}

		protected override void Dispose(bool disposing)
		{
			bool flag = disposing && this.components != null;
			if (flag)
			{
				this.components.Dispose();
			}
			base.Dispose(disposing);
		}

		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ErrorDialogForm));
            this.lbl_errorMsg = new System.Windows.Forms.Label();
            this.btn_ok = new OpenSoftFace.Setting.Controls.MyButton();
            this.pic_error = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer)).BeginInit();
            this.splitContainer.Panel1.SuspendLayout();
            this.splitContainer.Panel2.SuspendLayout();
            this.splitContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btn_close)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn_hide)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_error)).BeginInit();
            this.SuspendLayout();
            // 
            // splitContainer
            // 
            this.splitContainer.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splitContainer.IsSplitterFixed = true;
            // 
            // splitContainer.Panel2
            // 
            this.splitContainer.Panel2.Controls.Add(this.lbl_errorMsg);
            this.splitContainer.Panel2.Controls.Add(this.pic_error);
            this.splitContainer.Panel2.Controls.Add(this.btn_ok);
            this.splitContainer.Size = new System.Drawing.Size(375, 188);
            this.splitContainer.SplitterDistance = 52;
            this.splitContainer.TabStop = false;
            // 
            // btn_close
            // 
            this.btn_close.Image = ((System.Drawing.Image)(resources.GetObject("btn_close.Image")));
            this.btn_close.Location = new System.Drawing.Point(350, 10);
            this.btn_close.Size = new System.Drawing.Size(16, 16);
            // 
            // lbl_title
            // 
            this.lbl_title.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(1)))), ((int)(((byte)(119)))), ((int)(((byte)(213)))));
            this.lbl_title.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.lbl_title.Size = new System.Drawing.Size(375, 52);
            this.lbl_title.Text = "系统提示";
            // 
            // lbl_errorMsg
            // 
            this.lbl_errorMsg.AutoEllipsis = true;
            this.lbl_errorMsg.Font = new System.Drawing.Font("微软雅黑", 13F);
            this.lbl_errorMsg.Location = new System.Drawing.Point(126, 31);
            this.lbl_errorMsg.Name = "lbl_errorMsg";
            this.lbl_errorMsg.Size = new System.Drawing.Size(127, 24);
            this.lbl_errorMsg.TabIndex = 6;
            this.lbl_errorMsg.Text = "确定要删除吗?";
            // 
            // btn_ok
            // 
            this.btn_ok.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(1)))), ((int)(((byte)(119)))), ((int)(((byte)(213)))));
            this.btn_ok.EnterBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(93)))), ((int)(((byte)(163)))), ((int)(((byte)(239)))));
            this.btn_ok.EnterForeColor = System.Drawing.Color.White;
            this.btn_ok.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(1)))), ((int)(((byte)(119)))), ((int)(((byte)(213)))));
            this.btn_ok.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_ok.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btn_ok.ForeColor = System.Drawing.Color.White;
            this.btn_ok.LeaveBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(1)))), ((int)(((byte)(119)))), ((int)(((byte)(213)))));
            this.btn_ok.LeaveForeColor = System.Drawing.Color.White;
            this.btn_ok.Location = new System.Drawing.Point(141, 88);
            this.btn_ok.Name = "btn_ok";
            this.btn_ok.Size = new System.Drawing.Size(92, 32);
            this.btn_ok.TabIndex = 4;
            this.btn_ok.Text = "确认";
            this.btn_ok.UseVisualStyleBackColor = false;
            this.btn_ok.Click += new System.EventHandler(this.ok_Click);
            // 
            // pic_error
            // 
            this.pic_error.Location = new System.Drawing.Point(79, 28);
            this.pic_error.Name = "pic_error";
            this.pic_error.Size = new System.Drawing.Size(30, 30);
            this.pic_error.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pic_error.TabIndex = 7;
            this.pic_error.TabStop = false;
            // 
            // ErrorDialogForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(375, 188);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "ErrorDialogForm";
            this.ShowInTaskbar = false;
            this.splitContainer.Panel1.ResumeLayout(false);
            this.splitContainer.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer)).EndInit();
            this.splitContainer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.btn_close)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn_hide)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_error)).EndInit();
            this.ResumeLayout(false);

		}
	}
}
