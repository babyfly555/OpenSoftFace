using OpenSoftFace.Setting.Controls;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace OpenSoftFace.Setting.Forms
{
	public class UpdateSuccessDialogForm : DragBaseForm
	{
		public delegate DialogResult InvokeDelegate(Form parent, Form subForm);

		private static UpdateSuccessDialogForm updateForm;

		private static DialogResult dialog;

		public static int labelWidth = 10;

		private IContainer components = null;

		private MyButton btn_ok;

		private Label label1;

		private Label lbl_errorMsg;

		private PictureBox pic_error;

		public UpdateSuccessDialogForm()
		{
			this.InitializeComponent();
		}

		public static DialogResult showMsg(bool isNow)
		{
			UpdateSuccessDialogForm.updateForm = new UpdateSuccessDialogForm();
			if (isNow)
			{
				UpdateSuccessDialogForm.updateForm.label1.Text = "新的考勤规则即将生效";
				UpdateSuccessDialogForm.updateForm.label1.Location = new Point(120, 94);
			}
			UpdateSuccessDialogForm.updateForm.TopMost = true;
			UpdateSuccessDialogForm.updateForm.ShowDialog();
			return UpdateSuccessDialogForm.dialog;
		}

		private void ok_Click(object sender, EventArgs e)
		{
			UpdateSuccessDialogForm.dialog = DialogResult.OK;
			UpdateSuccessDialogForm.updateForm.DialogResult = UpdateSuccessDialogForm.dialog;
			UpdateSuccessDialogForm.updateForm.Close();
		}

		private void cancel_Click(object sender, EventArgs e)
		{
			UpdateSuccessDialogForm.dialog = DialogResult.Cancel;
			UpdateSuccessDialogForm.updateForm.DialogResult = UpdateSuccessDialogForm.dialog;
			UpdateSuccessDialogForm.updateForm.Close();
		}

		public override void Close_btn_Click(object sender, EventArgs e)
		{
			UpdateSuccessDialogForm.dialog = DialogResult.Cancel;
			UpdateSuccessDialogForm.updateForm.DialogResult = UpdateSuccessDialogForm.dialog;
			UpdateSuccessDialogForm.updateForm.Close();
		}

		public static DialogResult XShowDialog(Form parent, Form subForm)
		{
			bool invokeRequired = parent.InvokeRequired;
			DialogResult result;
			if (invokeRequired)
			{
				UpdateSuccessDialogForm.InvokeDelegate method = new UpdateSuccessDialogForm.InvokeDelegate(UpdateSuccessDialogForm.XShowDialog);
				parent.Invoke(method, new object[]
				{
					parent,
					subForm
				});
				result = subForm.DialogResult;
			}
			else
			{
				DialogResult dialogResult = subForm.ShowDialog(parent);
				result = dialogResult;
			}
			return result;
		}

		protected override void Dispose(bool disposing)
		{
			bool flag = disposing && this.components != null;
			if (flag)
			{
				this.components.Dispose();
			}
			base.Dispose(disposing);
		}

		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UpdateSuccessDialogForm));
            this.btn_ok = new OpenSoftFace.Setting.Controls.MyButton();
            this.label1 = new System.Windows.Forms.Label();
            this.lbl_errorMsg = new System.Windows.Forms.Label();
            this.pic_error = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer)).BeginInit();
            this.splitContainer.Panel1.SuspendLayout();
            this.splitContainer.Panel2.SuspendLayout();
            this.splitContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btn_close)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn_hide)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_error)).BeginInit();
            this.SuspendLayout();
            // 
            // splitContainer
            // 
            this.splitContainer.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splitContainer.IsSplitterFixed = true;
            // 
            // splitContainer.Panel2
            // 
            this.splitContainer.Panel2.Controls.Add(this.label1);
            this.splitContainer.Panel2.Controls.Add(this.lbl_errorMsg);
            this.splitContainer.Panel2.Controls.Add(this.pic_error);
            this.splitContainer.Panel2.Controls.Add(this.btn_ok);
            this.splitContainer.Size = new System.Drawing.Size(375, 228);
            this.splitContainer.SplitterDistance = 52;
            this.splitContainer.TabStop = false;
            // 
            // btn_close
            // 
            this.btn_close.Image = ((System.Drawing.Image)(resources.GetObject("btn_close.Image")));
            this.btn_close.Location = new System.Drawing.Point(350, 9);
            this.btn_close.Size = new System.Drawing.Size(16, 16);
            // 
            // lbl_title
            // 
            this.lbl_title.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(1)))), ((int)(((byte)(119)))), ((int)(((byte)(213)))));
            this.lbl_title.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.lbl_title.Size = new System.Drawing.Size(375, 52);
            this.lbl_title.Text = "系统提示";
            // 
            // btn_ok
            // 
            this.btn_ok.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(1)))), ((int)(((byte)(119)))), ((int)(((byte)(213)))));
            this.btn_ok.EnterBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(93)))), ((int)(((byte)(163)))), ((int)(((byte)(239)))));
            this.btn_ok.EnterForeColor = System.Drawing.Color.White;
            this.btn_ok.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(1)))), ((int)(((byte)(119)))), ((int)(((byte)(213)))));
            this.btn_ok.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_ok.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btn_ok.ForeColor = System.Drawing.Color.White;
            this.btn_ok.LeaveBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(1)))), ((int)(((byte)(119)))), ((int)(((byte)(213)))));
            this.btn_ok.LeaveForeColor = System.Drawing.Color.White;
            this.btn_ok.Location = new System.Drawing.Point(141, 136);
            this.btn_ok.Name = "btn_ok";
            this.btn_ok.Size = new System.Drawing.Size(92, 32);
            this.btn_ok.TabIndex = 0;
            this.btn_ok.Text = "确认";
            this.btn_ok.UseVisualStyleBackColor = false;
            this.btn_ok.Click += new System.EventHandler(this.ok_Click);
            // 
            // label1
            // 
            this.label1.AutoEllipsis = true;
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.label1.Location = new System.Drawing.Point(111, 94);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(152, 17);
            this.label1.TabIndex = 8;
            this.label1.Text = "新的考勤规则将于次月生效";
            // 
            // lbl_errorMsg
            // 
            this.lbl_errorMsg.AutoEllipsis = true;
            this.lbl_errorMsg.AutoSize = true;
            this.lbl_errorMsg.Font = new System.Drawing.Font("微软雅黑", 13F);
            this.lbl_errorMsg.Location = new System.Drawing.Point(146, 15);
            this.lbl_errorMsg.Name = "lbl_errorMsg";
            this.lbl_errorMsg.Size = new System.Drawing.Size(82, 24);
            this.lbl_errorMsg.TabIndex = 9;
            this.lbl_errorMsg.Text = "操作成功";
            // 
            // pic_error
            // 
            this.pic_error.Location = new System.Drawing.Point(167, 47);
            this.pic_error.Name = "pic_error";
            this.pic_error.Size = new System.Drawing.Size(40, 40);
            this.pic_error.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pic_error.TabIndex = 10;
            this.pic_error.TabStop = false;
            // 
            // UpdateSuccessDialogForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(375, 228);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "UpdateSuccessDialogForm";
            this.ShowInTaskbar = false;
            this.splitContainer.Panel1.ResumeLayout(false);
            this.splitContainer.Panel2.ResumeLayout(false);
            this.splitContainer.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer)).EndInit();
            this.splitContainer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.btn_close)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn_hide)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_error)).EndInit();
            this.ResumeLayout(false);

		}
	}
}
