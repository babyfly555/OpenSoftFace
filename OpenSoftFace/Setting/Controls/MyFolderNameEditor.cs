using System;
using System.ComponentModel;
using System.Drawing.Design;
using System.Windows.Forms;

namespace OpenSoftFace.Setting.Controls
{
	public class MyFolderNameEditor : UITypeEditor
	{
		public override UITypeEditorEditStyle GetEditStyle(ITypeDescriptorContext context)
		{
			return UITypeEditorEditStyle.Modal;
		}

		public override object EditValue(ITypeDescriptorContext context, IServiceProvider provider, object value)
		{
			MyFolderBrowserDialog myFolderBrowserDialog = new MyFolderBrowserDialog();
			bool flag = value != null;
			if (flag)
			{
				myFolderBrowserDialog.DirectoryPath = string.Format("{0}", value);
			}
			bool flag2 = myFolderBrowserDialog.ShowDialog(null) == DialogResult.OK;
			object result;
			if (flag2)
			{
				result = myFolderBrowserDialog.DirectoryPath;
			}
			else
			{
				result = value;
			}
			return result;
		}
	}
}
