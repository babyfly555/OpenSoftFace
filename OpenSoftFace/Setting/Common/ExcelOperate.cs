using LogUtil;
using Model.Entity;
using NPOI.HSSF.UserModel;
using NPOI.HSSF.Util;
using NPOI.SS.UserModel;
using NPOI.SS.Util;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Text.RegularExpressions;
using Utils;

namespace OpenSoftFace.Setting.Common
{
	public class ExcelOperate
	{
		public static bool AttendRecordsToExcel(List<string> commonColumnsList, Dictionary<string, List<string>> dateDict, Dictionary<string, List<MainInfo>> tempExportDict, string fileName,string excelName="")
		{
			HSSFWorkbook hSSFWorkbook = new HSSFWorkbook();
			bool result;
			try
			{
				foreach (string current in dateDict.Keys)
				{
					ISheet sheet = hSSFWorkbook.CreateSheet(current);
					HSSFSheet hSSFSheet = (HSSFSheet)hSSFWorkbook.GetSheet(current);
					ICellStyle cellStyle = hSSFWorkbook.CreateCellStyle();
					ICellStyle cellStyle2 = hSSFWorkbook.CreateCellStyle();
					IFont font = hSSFWorkbook.CreateFont();
					font.Boldweight = 700;
                    //font.FontHeightInPoints = 16;
					//cellStyle.FillForegroundColor = 9;
					//cellStyle.FillPattern = FillPattern.SolidForeground;
					//cellStyle.BorderLeft = BorderStyle.Thick;
					//cellStyle.BorderRight = BorderStyle.Thin;
					//cellStyle.BorderTop = BorderStyle.Thin;
					//cellStyle.BorderBottom = BorderStyle.Thin;

                    //cellStyle.BorderBottom= BorderStyle.Thin;
                    //cellStyle.BorderLeft= BorderStyle.Thin;
                    //cellStyle.BorderRight= BorderStyle.Thin;
                    //cellStyle.BorderTop = BorderStyle.Thin;
                    //cellStyle.BottomBorderColor= HSSFColor.Green.Index;
					cellStyle.WrapText = true;
					cellStyle2.CloneStyleFrom(cellStyle);
					cellStyle.SetFont(font);
					int num = 0;
					hSSFSheet.CreateRow(num);
					HSSFRow hSSFRow = (HSSFRow)hSSFSheet.GetRow(num);

                    //CellRangeAddress region = new CellRangeAddress(0, 0, 0, 36); //设置合并的单元格;1.开始行 2.结束行 3.开始列 4.结束列;从0开始
                    //hSSFSheet.AddMergedRegion(region);

					hSSFRow.CreateCell(0).SetCellValue(excelName+"考勤表");


                    HSSFCellStyle fCellStyle = (HSSFCellStyle)hSSFWorkbook.CreateCellStyle();
                    fCellStyle.Alignment = HorizontalAlignment.Center;
                    fCellStyle.VerticalAlignment = VerticalAlignment.Center;
                    //style.VerticalAlignment = VerticalAlignment.Justify;//垂直居中 方法1 
                    //style.Alignment = HorizontalAlignment.CenterSelection;//设置居中 方法2
                    //style.Alignment = HorizontalAlignment.Center;//设置居中 方法3 
                    HSSFFont fontTitle = (HSSFFont)hSSFWorkbook.CreateFont();
                    fontTitle.Boldweight = 700;
                    //fontTitle.FontHeightInPoints = 12;
                   // fontTitle.setFontHeightInPoints((short)16);//设置字体大小
                    fontTitle.FontHeightInPoints = 16;
                    fCellStyle.SetFont(fontTitle);
                    hSSFRow.GetCell(0).CellStyle = fCellStyle;
                    hSSFRow.GetCell(0).Row.Height = 600;

                    HSSFCellStyle fCellStyleItem = (HSSFCellStyle)hSSFWorkbook.CreateCellStyle();
                    //IFont fontItem = hSSFWorkbook.CreateFont();
                    fCellStyleItem.Alignment = HorizontalAlignment.Center;
                    fCellStyleItem.VerticalAlignment = VerticalAlignment.Center;
                    HSSFFont fontItem = (HSSFFont)hSSFWorkbook.CreateFont();
                    fontTitle.FontHeightInPoints = 12;
                    //fCellStyleItem.BorderBottom = BorderStyle.Thin;
                    //fCellStyleItem.BorderLeft = BorderStyle.Thin;
                    //fCellStyleItem.BorderRight = BorderStyle.Thin;
                    //fCellStyleItem.BorderTop = BorderStyle.Thin;
                    fCellStyleItem.SetFont(fontTitle);

                    //hSSFRow.GetCell(0).CellStyle.Alignment = HorizontalAlignment.CenterSelection;

					hSSFSheet.CreateRow(++num);
					hSSFSheet.CreateRow(num + 1);
					hSSFRow = (HSSFRow)hSSFSheet.GetRow(num);

					HSSFRow hSSFRow2 = (HSSFRow)hSSFSheet.GetRow(num + 1);
					List<string> list = new List<string>();
					list.AddRange(dateDict[current]);
					HSSFCell[] array = new HSSFCell[list.Count + 1 + commonColumnsList.Count];
					int num2 = 0;
					foreach (string current2 in commonColumnsList)
					{
						array[num2] = (HSSFCell)hSSFRow.CreateCell(num2);
                        CellRangeAddress cellRangeAddress = new CellRangeAddress(num, num + 1, num2, num2);
                        hSSFSheet.AddMergedRegion(cellRangeAddress);
                        hSSFRow.GetCell(num2).CellStyle = fCellStyleItem;
                        //hSSFRow.GetCell(num2+1).CellStyle = fCellStyleItem;
						array[num2++].SetCellValue(current2);

                        /*for (int i = cellRangeAddress.FirstRow; i <= cellRangeAddress.LastRow; i++)
                        {
                            IRow row = HSSFCellUtil.GetRow(i, hSSFSheet);
                            for (int j = region.FirstColumn; j <= region.LastColumn; j++)
                            {
                                ICell singleCell = HSSFCellUtil.GetCell(row, (short)j);
                                singleCell.CellStyle = fCellStyleItem;
                            }
                        }*/

					}

                    //((HSSFRow)hSSFSheet.GetRow(1)).CreateCell(1).CellStyle.BorderBottom = BorderStyle.Thin;
                    //((HSSFRow)hSSFSheet.GetRow(1)).CreateCell(1).CellStyle.BorderLeft = BorderStyle.Thin;
                    //((HSSFRow)hSSFSheet.GetRow(1)).CreateCell(1).CellStyle.BorderRight = BorderStyle.Thin;
                    //((HSSFRow)hSSFSheet.GetRow(1)).CreateCell(1).CellStyle.BorderTop = BorderStyle.Thin;

                    //hSSFSheet.AddMergedRegion(new CellRangeAddress(1,2,3,3));
                    ((HSSFCell)hSSFRow.CreateCell(3)).SetCellValue("日期");
                    //hSSFRow.CreateCell(3).CellStyle.Alignment = HorizontalAlignment.CenterSelection;
					((HSSFCell)hSSFRow2.CreateCell(3)).SetCellValue("星期");
                    hSSFRow.GetCell(3).CellStyle = fCellStyleItem;
                    hSSFRow2.GetCell(3).CellStyle = fCellStyleItem;
					num2++;
					foreach (string current3 in list)
					{
						string[] expr_212 = new string[]
						{
							"-",
							"-"
						};
						string[] array2 = current3.Split(new char[]
						{
							','
						});
                        int temp = num2;
						((HSSFCell)hSSFRow.CreateCell(num2)).SetCellValue((array2 != null && array2.Length >= 1) ? array2[0] : "-");
						((HSSFCell)hSSFRow2.CreateCell(num2++)).SetCellValue((array2 != null && array2.Length >= 2) ? array2[1] : "-");
                        hSSFRow.GetCell(temp).CellStyle = fCellStyleItem;
                        hSSFRow2.GetCell(temp).CellStyle = fCellStyleItem;
					}
					hSSFSheet.AddMergedRegion(new CellRangeAddress(num, num + 1, num2, num2));
					((HSSFCell)hSSFRow.CreateCell(num2++)).SetCellValue("出勤");
					hSSFSheet.AddMergedRegion(new CellRangeAddress(num, num + 1, num2, num2));
					((HSSFCell)hSSFRow.CreateCell(num2++)).SetCellValue("正常");
					hSSFSheet.AddMergedRegion(new CellRangeAddress(num, num + 1, num2, num2));
					((HSSFCell)hSSFRow.CreateCell(num2++)).SetCellValue("缺勤");
                    hSSFRow.GetCell(num2 - 1).CellStyle = fCellStyleItem;
                    hSSFRow.GetCell(num2 - 2).CellStyle = fCellStyleItem;
                    hSSFRow.GetCell(num2 - 3).CellStyle = fCellStyleItem;
					num++;
					int num3 = 3;
					List<MainInfo> list2 = tempExportDict[current];
					for (int i = 0; i < list2.Count; i++)
					{
						int num4 = num;
						hSSFSheet.CreateRow(++num);
						hSSFSheet.CreateRow(++num);
						HSSFRow hSSFRow3 = (HSSFRow)hSSFSheet.GetRow(num4 + 1);
						HSSFRow hSSFRow4 = (HSSFRow)hSSFSheet.GetRow(num4 + 2);
						hSSFRow3.CreateCell(num3).SetCellValue("上班");
						hSSFRow4.CreateCell(num3).SetCellValue("下班");
                        hSSFRow3.GetCell(num3).CellStyle = fCellStyleItem;
                        hSSFRow4.GetCell(num3).CellStyle = fCellStyleItem;
						List<AttendanceRecord2> attendRecordList = list2[i].attendRecordList;
						bool flag = attendRecordList != null && attendRecordList.Count > 0;
						if (flag)
						{
							for (int j = 0; j < attendRecordList.Count; j++)
							{
								int column = num3 + j + 1;
                                if (attendRecordList[j].StartTime == null ||attendRecordList[j].StartTime =="") {
                                    attendRecordList[j].StartTime = "      ";
                                }
                                if (attendRecordList[j].EndTime == null || attendRecordList[j].EndTime == "")
                                {
                                    attendRecordList[j].EndTime = "      ";
                                }
								hSSFRow3.CreateCell(column).SetCellValue(StringUtil.CheckStringEmpty(attendRecordList[j].StartTime.Substring(0,5)));
                                hSSFRow4.CreateCell(column).SetCellValue(StringUtil.CheckStringEmpty(attendRecordList[j].EndTime.Substring(0, 5)));
                                hSSFRow3.GetCell(column).CellStyle = fCellStyleItem;
                                hSSFRow4.GetCell(column).CellStyle = fCellStyleItem;
							}
						}
                        CellRangeAddress cellRangeAddress0 = new CellRangeAddress(num4 + 1, num4 + 2, 0, 0);
                        CellRangeAddress cellRangeAddress1 = new CellRangeAddress(num4 + 1, num4 + 2, 1, 1);
                        CellRangeAddress cellRangeAddress2 = new CellRangeAddress(num4 + 1, num4 + 2, 2, 2);

                        CellRangeAddress cellRangeAddress4 = new CellRangeAddress(num4 + 1, num4 + 2, array.Length, array.Length);
                        CellRangeAddress cellRangeAddress5 = new CellRangeAddress(num4 + 1, num4 + 2, array.Length + 1, array.Length + 1);
                        CellRangeAddress cellRangeAddress6 = new CellRangeAddress(num4 + 1, num4 + 2, array.Length + 2, array.Length + 2);

						hSSFSheet.AddMergedRegion(new CellRangeAddress(num4 + 1, num4 + 2, 0, 0));
						hSSFRow3.CreateCell(0).SetCellValue((double)(i + 1));
						hSSFSheet.AddMergedRegion(new CellRangeAddress(num4 + 1, num4 + 2, 1, 1));
						hSSFRow3.CreateCell(1).SetCellValue(StringUtil.CheckStringEmpty(list2[i].PersonName));
						hSSFSheet.AddMergedRegion(new CellRangeAddress(num4 + 1, num4 + 2, 2, 2));
						hSSFRow3.CreateCell(2).SetCellValue(StringUtil.CheckStringEmpty(list2[i].PersonSerial));
						hSSFSheet.AddMergedRegion(new CellRangeAddress(num4 + 1, num4 + 2, array.Length, array.Length));
						hSSFRow3.CreateCell(array.Length).SetCellValue(StringUtil.CheckStringEmpty(list2[i].MonthTotal));
						hSSFSheet.AddMergedRegion(new CellRangeAddress(num4 + 1, num4 + 2, array.Length + 1, array.Length + 1));
						hSSFRow3.CreateCell(array.Length + 1).SetCellValue(list2[i].MonthAttendTotal);
						hSSFSheet.AddMergedRegion(new CellRangeAddress(num4 + 1, num4 + 2, array.Length + 2, array.Length + 2));
						hSSFRow3.CreateCell(array.Length + 2).SetCellValue(list2[i].MonthAbsenceTotal);

                        hSSFRow3.GetCell(0).CellStyle = fCellStyleItem;
                        hSSFRow3.GetCell(1).CellStyle = fCellStyleItem;
                        hSSFRow3.GetCell(3).CellStyle = fCellStyleItem;
                        hSSFRow3.GetCell(array.Length).CellStyle = fCellStyleItem;
                        hSSFRow3.GetCell(array.Length + 1).CellStyle = fCellStyleItem;
                        hSSFRow3.GetCell(array.Length + 2).CellStyle = fCellStyleItem;

                        /*for (int i0 = cellRangeAddress0.FirstRow; i0 <= cellRangeAddress0.LastRow; i0++)
                        {
                            IRow row = HSSFCellUtil.GetRow(i0, hSSFSheet);
                            for (int j = region.FirstColumn; j <= region.LastColumn; j++)
                            {
                                ICell singleCell = HSSFCellUtil.GetCell(row, (short)j);
                                singleCell.CellStyle = fCellStyleItem;
                            }
                        }*/
					}
				}



				FileStream fileStream = new FileStream(fileName, FileMode.Create);
				hSSFWorkbook.Write(fileStream);
				fileStream.Close();
				hSSFWorkbook.Close();
			}
			catch (Exception se)
			{
				LogHelper.LogError(typeof(ExcelOperate), se);
				result = false;
				return result;
			}
			finally
			{
				bool flag2 = hSSFWorkbook != null;
				if (flag2)
				{
					hSSFWorkbook.Close();
				}
			}
			result = true;
			return result;
		}

		private static string getCellValue(ICell cell)
		{
			string result;
			try
			{
				result = cell.ToString().Trim();
			}
			catch
			{
				result = null;
			}
			return result;
		}

		private static List<Process> GetExcelProcesses()
		{
			Process[] processes = Process.GetProcesses();
			List<Process> list = new List<Process>();
			for (int i = 0; i < processes.Length; i++)
			{
				bool flag = "EXCEL".Equals(processes[i].ProcessName.ToUpper()) || "NPOI".Equals(processes[i].ProcessName.ToUpper());
				if (flag)
				{
					list.Add(processes[i]);
				}
			}
			return list;
		}

		private static void KillAllExcel()
		{
			try
			{
				List<Process> excelProcesses = ExcelOperate.GetExcelProcesses();
				for (int i = 0; i < excelProcesses.Count; i++)
				{
					excelProcesses[i].Kill();
				}
			}
			catch (Exception se)
			{
				LogHelper.LogError(typeof(ExcelOperate), se);
			}
		}

		public static bool isOldExcel(string filePath)
		{
			Regex regex = new Regex("^.+\\.(?i)(xls)$");
			return regex.IsMatch(filePath);
		}
	}
}
