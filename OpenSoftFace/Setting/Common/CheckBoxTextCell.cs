using LogUtil;
using System;
using System.Drawing;
using System.Windows.Forms;
using System.Windows.Forms.VisualStyles;

namespace OpenSoftFace.Setting.Common
{
	public class CheckBoxTextCell : DataGridViewCheckBoxCell
	{
		private string text;

		private int deviation_X = 12;

		private int deviation_Y = 12;

		private Point center = new Point(0, 0);

		private Size size = new Size(0, 0);

		private CheckBoxState state = CheckBoxState.UncheckedNormal;

		private static Font defaultFont = new Font("微软雅黑", 9f, FontStyle.Regular, GraphicsUnit.Point, 134);

		public string Text
		{
			get
			{
				return this.text;
			}
			set
			{
				this.text = value;
			}
		}

		protected override void OnMouseDown(DataGridViewCellMouseEventArgs e)
		{
			base.OnMouseDown(e);
		}

		protected override void OnMouseUp(DataGridViewCellMouseEventArgs e)
		{
			bool flag = string.IsNullOrEmpty(this.text);
			if (flag)
			{
				bool flag2 = e.X >= base.ContentBounds.X && e.X <= base.ContentBounds.X + base.ContentBounds.Width && e.Y >= base.ContentBounds.Y && e.Y <= base.ContentBounds.Y + base.ContentBounds.Height;
				if (flag2)
				{
					bool flag3 = !Convert.ToBoolean(base.Value);
					if (flag3)
					{
						base.Value = true;
						this.state = CheckBoxState.CheckedNormal;
						DataGridViewCellEventArgs e2 = new DataGridViewCellEventArgs(e.ColumnIndex, e.RowIndex);
						base.RaiseCellValueChanged(e2);
					}
					else
					{
						base.Value = false;
						this.state = CheckBoxState.UncheckedNormal;
						DataGridViewCellEventArgs e3 = new DataGridViewCellEventArgs(e.ColumnIndex, e.RowIndex);
						base.RaiseCellValueChanged(e3);
					}
					base.OnMouseUp(e);
				}
			}
			else
			{
				Size size = base.Size;
				bool flag4 = e.X >= this.center.X && e.X <= this.center.X + this.size.Width && e.Y >= this.deviation_Y && e.Y <= this.size.Height + this.deviation_Y;
				if (flag4)
				{
					bool flag5 = !Convert.ToBoolean(base.Value);
					if (flag5)
					{
						base.Value = true;
						this.state = CheckBoxState.CheckedNormal;
						DataGridViewCellEventArgs e4 = new DataGridViewCellEventArgs(e.ColumnIndex, e.RowIndex);
						base.RaiseCellValueChanged(e4);
					}
					else
					{
						base.Value = false;
						this.state = CheckBoxState.UncheckedNormal;
						DataGridViewCellEventArgs e5 = new DataGridViewCellEventArgs(e.ColumnIndex, e.RowIndex);
						base.RaiseCellValueChanged(e5);
					}
					base.OnMouseUp(e);
				}
			}
		}

		protected override void Paint(Graphics graphics, Rectangle clipBounds, Rectangle cellBounds, int rowIndex, DataGridViewElementStates elementState, object value, object formattedValue, string errorText, DataGridViewCellStyle cellStyle, DataGridViewAdvancedBorderStyle advancedBorderStyle, DataGridViewPaintParts paintParts)
		{
			try
			{
				base.Paint(graphics, clipBounds, cellBounds, rowIndex, elementState, value, formattedValue, errorText, cellStyle, advancedBorderStyle, paintParts);
				Rectangle textBounds = default(Rectangle);
				bool flag = this.Text != null;
				if (flag)
				{
					bool flag2 = (paintParts & DataGridViewPaintParts.Background) == DataGridViewPaintParts.Background;
					if (flag2)
					{
						SolidBrush solidBrush = new SolidBrush(this.Selected ? cellStyle.SelectionBackColor : cellStyle.BackColor);
						graphics.FillRectangle(solidBrush, cellBounds);
						solidBrush.Dispose();
					}
					bool flag3 = (paintParts & DataGridViewPaintParts.Border) == DataGridViewPaintParts.Border;
					if (flag3)
					{
						this.PaintBorder(graphics, clipBounds, cellBounds, cellStyle, advancedBorderStyle);
					}
					this.size = CheckBoxRenderer.GetGlyphSize(graphics, this.state);
					this.center = new Point(cellBounds.X, cellBounds.Y);
					this.deviation_Y = (cellBounds.Height - this.size.Height) / 2;
					this.center.X = this.center.X + this.deviation_X;
					this.center.Y = this.center.Y + this.deviation_Y;
					textBounds.X = CheckBoxRenderer.GetGlyphSize(graphics, CheckBoxState.UncheckedNormal).Width + cellBounds.X + this.deviation_X;
					textBounds.Y = cellBounds.Y + 3;
					textBounds.Width = cellBounds.Width - CheckBoxRenderer.GetGlyphSize(graphics, CheckBoxState.UncheckedNormal).Width;
					textBounds.Height = cellBounds.Height - (cellBounds.Height - CheckBoxRenderer.GetGlyphSize(graphics, CheckBoxState.UncheckedNormal).Width) / 2 - 1;
					bool flag4 = Convert.ToBoolean(base.Value);
					if (flag4)
					{
						this.state = CheckBoxState.CheckedNormal;
					}
					else
					{
						this.state = CheckBoxState.UncheckedNormal;
					}
					CheckBoxTextCell.defaultFont = new Font("微软雅黑", SystemFonts.DefaultFont.Size);
					CheckBoxRenderer.DrawCheckBox(graphics, this.center, textBounds, this.text, CheckBoxTextCell.defaultFont, TextFormatFlags.Bottom, false, this.state);
				}
				else
				{
					base.Paint(graphics, clipBounds, cellBounds, rowIndex, elementState, value, formattedValue, errorText, cellStyle, advancedBorderStyle, paintParts);
				}
			}
			catch (Exception se)
			{
				LogHelper.LogError(typeof(CheckBoxTextCell), se);
			}
		}
	}
}
