
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows.Forms;

namespace OpenSoftFace
{
	public class IniHelper
	{
		private string FilePath = "setting.ini";

		private string Section = "setting";

		public Dictionary<string, string> Dict = new Dictionary<string, string>();

		[DllImport("kernel32")]
		private static extern long WritePrivateProfileString(string section, string key, string val, string filepath);

		[DllImport("kernel32")]
		private static extern int GetPrivateProfileString(string section, string key, string def, StringBuilder retval, int size, string filePath);

		[DllImport("kernel32", EntryPoint = "GetPrivateProfileString")]
		private static extern uint GetPrivateProfileStringA(string section, string key, string def, byte[] retVal, int size, string filePath);

		public IniHelper(string _filePath = "setting.ini", string _section = "setting")
		{
			bool flag = File.Exists(System.Environment.CurrentDirectory+"/setting/" + "setting.ini");
			if (flag)
			{
                _filePath = System.Environment.CurrentDirectory + "/setting/" +"setting.ini";
			}
			bool flag2 = _filePath.Contains("\\");
			if (flag2)
			{
				this.FilePath = _filePath;
			}
			else
			{
				this.FilePath = Application.StartupPath + "\\" + _filePath;
			}
			this.Section = _section;
			this.Reload();
		}

		public void Reload()
		{
			this.Dict = new Dictionary<string, string>();
			List<string> list = this._getKeyList();
			foreach (string current in list)
			{
				this.Dict.Add(current, this.Get(current, ""));
			}
		}

		public string[] GetKeyList()
		{
			return this.Dict.Keys.ToArray<string>();
		}

		private List<string> _getKeyList()
		{
			List<string> list = new List<string>();
			byte[] array = new byte[65536];
			uint privateProfileStringA = IniHelper.GetPrivateProfileStringA(this.Section, null, null, array, array.Length, this.FilePath);
			int num = 0;
			int num2 = 0;
			while ((long)num2 < (long)((ulong)privateProfileStringA))
			{
				bool flag = array[num2] == 0;
				if (flag)
				{
					list.Add(Encoding.UTF8.GetString(array, num, num2 - num));
					num = num2 + 1;
				}
				num2++;
			}
			return list;
		}

		public string Get(string key, string defaultVal = "")
		{
			bool flag = this.Dict.ContainsKey(key);
			string result;
			if (flag)
			{
				result = this.Dict[key];
			}
			else
			{
				byte[] array = new byte[1024];
				IniHelper.GetPrivateProfileStringA(this.Section, key, defaultVal, array, 1024, this.FilePath);
				string text = Encoding.UTF8.GetString(array);
				text = text.Replace("\0", string.Empty);
				result = text;
			}
			return result;
		}

		public void Set(string key, string val)
		{
			this.Dict[key] = val;
			IniHelper.WritePrivateProfileString(this.Section, key, val, this.FilePath);
		}

		public void Del(string key)
		{
			this.Dict.Remove(key);
			IniHelper.WritePrivateProfileString(this.Section, key, null, this.FilePath);
		}

		public int GetInt(string key, int defaultVal = 0)
		{
			string s = this.Get(key, defaultVal.ToString());
			int num = defaultVal;
			return int.TryParse(s, out num) ? num : defaultVal;
		}

		public float GetFloat(string key, float defaultVal = 0f)
		{
			string s = this.Get(key, defaultVal.ToString());
			float num = defaultVal;
			return float.TryParse(s, out num) ? num : defaultVal;
		}

		public bool GetBool(string key, bool defaultVal = false)
		{
			string a = this.Get(key, defaultVal ? "1" : "0");
			return a == "1";
		}

		public void SetInt(string key, int val)
		{
			this.Set(key, val.ToString());
		}

		public void SetFloat(string key, float val)
		{
			this.Set(key, val.ToString());
		}

		public void SetBool(string key, bool bo)
		{
			this.Set(key, bo ? "1" : "0");
		}
	}
}
